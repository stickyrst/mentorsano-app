var execSync = require('child_process').execSync;

module.exports = function() {
    execSync("mkdir -p ./www-sourcemaps");
    execSync("mv ./www/*.map ./www-sourcemaps");
    execSync("cp ./www/*.js ./www-sourcemaps");
};
