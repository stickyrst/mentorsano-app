#!/usr/bin/env bash

if [ "${PWD##*/}" == "scripts" ]; then
  echo "Please run this script from main directory"
  exit 1
fi

unsignedApkName=app-release-unsigned.apk
unsignedApkPath=./platforms/android/app/build/outputs/apk/release/$unsignedApkName

buildFolder=./build
signedApkName=MentorSano.apk

if [[ ! -f "$unsignedApkPath" ]]; then
  echo "unsigned apk file does not exists: '$unsignedApkPath'"
fi

mkdir -p $buildFolder

rm -f "$buildFolder/$signedApkName"
rm -f "$buildFolder/$unsignedApkName"

cp "$unsignedApkPath" "$buildFolder"
cd "$buildFolder" || exit 1

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore file:../scripts/android-key/mentorsano.keystore -storepass mentorsano $unsignedApkName mentorsano_release

zipalign -v 4 $unsignedApkName $signedApkName

rm -f "./$unsignedApkName"
