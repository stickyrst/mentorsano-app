#!/usr/bin/env bash

mkdir -p build
rm -rf ./build/app-debug.apk
cd build || exit
cp ../platforms/android/app/build/outputs/apk/debug/app-debug.apk .
echo "copied to ./build/app-debug.apk"
