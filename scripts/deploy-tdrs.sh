#!/usr/bin/env bash

pushd () {
    command pushd "$@" > /dev/null
}

popd () {
    command popd "$@" > /dev/null
}

echo ""
echo deploying ...
echo ""

copyKey=false
keyPath=

if $copyKey
then
    echo copying key
    mkdir -p /home/nm/.key
    keyPath=/home/nm/.key/priv-key
    cp /mnt/c/Users/NM/.key/#2/id_rsa $keyPath
    chmod 700 $keyPath
else
    keyPath=/home/nm/.key/hashtag_2_key
fi


echo archiving ...
pushd -n "$(pwd)"
cd www || exit
rm -f ../app-build.tgz
tar -czf ../app-build.tgz *
cd ..

echo uploading...
scp -i $keyPath app-build.tgz root@tdrs.ro:/var/www/tdrs.ro/tht/mentorsano
rm -f app-build.tgz

echo running deploy script...

ssh -i $keyPath root@tdrs.ro "/var/www/tdrs.ro/tht/mentorsano/scripts/deploy-app.sh"


echo done!
popd
