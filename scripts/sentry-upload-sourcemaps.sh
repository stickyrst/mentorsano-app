# Assumes you're in a git repository
export SENTRY_AUTH_TOKEN=edb46fb6bfbe4bb78c2346fc4a530ab9e582fe67e33a446f8dd1d3da31d376bb
export SENTRY_ORG=tdr
export SENTRY_PROJECT=mentorsano-app
export SENTRY_URL=https://sentry.a.tdrs.ro/
#VERSION=$(sentry-cli releases propose-version)
VERSION=0.5.6

echo "creating release"
# Create a release
sentry-cli releases new "$VERSION" || exit 1

echo "associating commits"
# Associate commits with the release
sentry-cli releases set-commits --auto "$VERSION" || exit 1

echo "uploading sourcemaps"
sentry-cli releases files "$VERSION" upload-sourcemaps -d tdr --ext js --ext map --wait --validate ./www-sourcemaps || exit 1
# --url-prefix http://localhost/

sentry-cli releases finalize "$VERSION" || exit 1
