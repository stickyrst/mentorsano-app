## MentorSano Mobile app


#### Using
* Angular 9
* Ionic 5
* ngx formly 5.5.15 



### Setup
  * Prepare
    * `npm i -g ionic cordova native-run`
    * `npm i`
    * `ionic cordova prepare`
##### For generating resources:
``npm i -g cordova res``


#### Useful stuff
``keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore -list -v``

* for build apk
    * ``cd ./platforms/android/app/build/outputs/apk/debug``
    * ``cd ./META-INF``
    * ``keytool -printcert -file CERT.RSA``
    * ``keytool -list -printcert -jarfile file``
* for keystore
    * `keytool -list -keystore mentorsano.keystore`
    * `keytool -exportcert -alias mentorsano_release -keystore mentorsano.keystore | openssl sha1 -binary | openssl base64`
    
### Sign for Google Play


### Google sign in
* updated vars in package.json, config.xml and social-media-login.component.ts


