export interface EnvironmentModel {
    production?: boolean;
    debug?: boolean;
}
