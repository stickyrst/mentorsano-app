import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import * as Sentry from '@sentry/angular';
import { Integrations } from '@sentry/tracing';

(window as any).skipLocalNotificationReady = true;

Sentry.init({
    dsn: 'https://5a7cd791ec3547cea37fe98f85a483f4@sentry.a.tdrs.ro/4',
    integrations: [
        new Integrations.BrowserTracing({
            // tracingOrigins: ['localhost:7100', 'http://localhost:7100', '192.168.1.101:7101', 'http://192.168.1.101:7101'],
            routingInstrumentation: Sentry.routingInstrumentation,
        }),
    ],
    tracesSampleRate: 1.0,
    release: '0.5.6',
    dist: 'tdr',
});

if (environment.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.log(err));

