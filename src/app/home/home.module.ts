import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { SharedModule } from '../shared/shared.module';
import { FormlyHelperModule } from '@feats/formly-helper/formly-helper.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        SharedModule,
        FormlyHelperModule,
    ],
    declarations: [
        HomePage,
    ],
    providers: [],
})
export class HomePageModule {
}
