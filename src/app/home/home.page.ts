import { Component } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { HomePageActivityStatusModel } from '@feats/user-activity/models/home-page-activity-status.model';
import { UserActivityService } from '@feats/user-activity/services/user-activity.service';
import { MsFormlyHelperService } from '@feats/formly-helper/ms-formly-helper.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage {
    public pModel: HomePageActivityStatusModel;
    private firstLoad: boolean;
    constructor(
        private userActivityService: UserActivityService,
        public trans: TranslationsService,
        public msFormlyHelperService: MsFormlyHelperService,
    ) {
        this.firstLoad = true;
    }

    public async ionViewWillEnter() {
        const model = await this.userActivityService.getHomePageActivity({uiSilent: !this.firstLoad, goBackOnError: false});
        if (this.pModel?.firstAvailableCheckQuestion) {
            this.msFormlyHelperService.patchForCheckForm([this.pModel.firstAvailableCheckQuestion]);
        }
        if (model) {
            this.pModel = model;
            this.firstLoad = false;
        }
    }
}
