import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
    selector: 'app-circular-progress',
    templateUrl: './circular-progress.component.html',
    styleUrls: ['./circular-progress.component.scss'],
})
export class CircularProgressComponent implements OnInit, OnChanges {

    constructor() {
    }

    @Input() public size: number;
    @Input() public gap: number;
    @Input() public progress = 0;
    @Input() public backgroundColor: string;
    @Input() public progressColor: string;
    @Input() public strokeWidth: number;

    @Input() public title: CircularProgressTextConfig;
    @Input() public titleUnit: CircularProgressTextConfig;
    @Input() public subtitle: CircularProgressTextConfig;
    @Input() public dotRadius: number;
    @Input() public dotColor: string;

    public arcRadius: number;
    public arcSize: number;
    public arcStart: number;
    public arcStrokeWidth: number;
    public positionX: number;
    public positionY: number;
    public strokeDashArray: number;
    private strokeDashOffSet: number;
    public fillValue: number;
    private range: number;
    public arcBackgroundColor: string;
    public arcProgressColor: string;

    public indicatorPositionX: string;
    public indicatorPositionY: string;

    private static polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        const angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians)),
        };
    }

    public ngOnInit() {
        this.initArc();
    }

    public ngOnChanges() {
        this.initArc();
    }

    private initArc() {
        this.arcBackgroundColor = this.backgroundColor ? this.backgroundColor : 'lightgrey';
        this.arcProgressColor = this.progressColor ? this.progressColor : 'blue';
        this.arcStrokeWidth = this.strokeWidth ? this.strokeWidth : 1;
        this.arcSize = this.size ? this.size : 300;
        this.arcRadius = (45 * this.arcSize) / 100;
        this.arcStart = this.gap ? 180 - this.gap : 90;
        this.positionX = this.arcSize / 2;
        this.positionY = this.arcSize / 2;
        this.range = (this.arcStart / 88);
        this.strokeDashArray = this.arcRadius * Math.PI * this.range;
        this.strokeDashOffSet = (this.arcRadius * Math.PI * this.range * -1);
        this.fillValue = ((this.progress * this.strokeDashArray) / 100) + this.strokeDashOffSet;

        const indicatorAngle = (2 * this.arcStart) / 100 * this.progress - this.arcStart;
        const indicatorPosition = CircularProgressComponent
            .polarToCartesian(this.positionX, this.positionY, this.arcRadius, indicatorAngle);
        this.indicatorPositionX = indicatorPosition.x;
        this.indicatorPositionY = indicatorPosition.y;
    }

    public drawnArc(x, y, radius, startAngle, endAngle) {
        endAngle = endAngle - 0.0001;
        /*to be able to draw a full circle*/
        const start = CircularProgressComponent.polarToCartesian(x, y, radius, endAngle);
        const end = CircularProgressComponent.polarToCartesian(x, y, radius, startAngle);

        const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';

        const d = [
            'M', start.x, start.y,
            'A', radius, radius, 0, largeArcFlag, 0, end.x, end.y,
        ].join(' ');

        return d;
    }

}

export interface CircularProgressTextConfig {
    text?: string;
    styles?: {
        fontSize?: string;
        color?: string;
        [key: string]: string;
    };
    class?: string | string[];
}
