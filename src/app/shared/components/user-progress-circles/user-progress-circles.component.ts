import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HomePageActivityStatusModel } from '@feats/user-activity/models/home-page-activity-status.model';
import { CircularProgressTextConfig } from '../circular-progress/circular-progress.component';
import { TranslationsService } from '@core/services/translations.service';

@Component({
    selector: 'app-user-progress-circles',
    templateUrl: './user-progress-circles.component.html',
    styleUrls: ['./user-progress-circles.component.scss'],
})
export class UserProgressCirclesComponent implements OnInit, OnChanges {

    @Input()
    public model: HomePageActivityStatusModel;

    public dayCircleConfig: CircleConfig;
    public monthCircleConfig: CircleConfig;

    public get safeModel(): HomePageActivityStatusModel {
        return this.model || {profileName: '', monthPercent: 0, todayPercent: 0, todayTitle: ''};
    }

    constructor(
        public trans: TranslationsService,
    ) {
        this.updateCirclesConfig();
        this.trans.languageChanged.subscribe(_ => {
            this.updateCirclesConfig();
        });
    }

    public ngOnInit() {
    }

    private updateCirclesConfig() {
        const titleStyles = {
            color: 'black',
            fontSize: '32px',
            fontWeight: 'bold',
        };
        const subTitleStyles = {
            color: '#707070',
            fontSize: '10px',
            paddingBottom: '5px',
        };
        this.dayCircleConfig = {
            title: {
                text: this.model?.todayPercent?.toString(),
                styles: {...titleStyles, paddingBottom: '5px'},
            },
            titleUnit: {
                text: '%',
                styles: {...titleStyles, fontSize: '24px', paddingBottom: '5px'},
            },
            subtitle: {
                text: this.trans.trans('todays-achievements'),
                styles: {...subTitleStyles},
            },
        };
        this.monthCircleConfig = {
            title: {
                text: this.model?.monthPercent?.toString(),
                styles: {...titleStyles, paddingBottom: '5px', paddingTop: '10px'},
            },
            titleUnit: {
                text: '%',
                styles: {...titleStyles, fontSize: '24px', paddingBottom: '5px', paddingTop: '10px'},
            },
            subtitle: {
                text: this.trans.trans('last-month').toUpperCase(),
                styles: subTitleStyles,
            },
        };
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.model) {
            this.updateCirclesConfig();
        }
    }
}

interface CircleConfig {
    title: CircularProgressTextConfig;
    subtitle: CircularProgressTextConfig;
    titleUnit: CircularProgressTextConfig;
}
