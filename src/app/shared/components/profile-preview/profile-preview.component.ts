import { Component, Input } from '@angular/core';
import { UserActivityModel } from '@feats/user-activity/models/user-activity.model';
import { TranslationsService } from '@core/services/translations.service';

@Component({
    selector: 'app-profile-preview',
    templateUrl: './profile-preview.component.html',
    styleUrls: ['./profile-preview.component.scss'],
})
export class ProfilePreviewComponent {
    @Input()
    public model: UserActivityModel;
    @Input()
    public big: boolean;
    @Input()
    public column: boolean;

    constructor(public trans: TranslationsService) {
    }
}
