import { Component, Input } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';

@Component({
    selector: 'app-feature-demo-header',
    templateUrl: './feature-demo-header.component.html',
    styleUrls: ['./feature-demo-header.component.scss'],
})
export class FeatureDemoHeaderComponent {
    @Input()
    public featureName: Features;

    constructor(
        public trans: TranslationsService,
    ) {
    }

    public get data(): FeatureDemoType {
        return featureDemoData[this.featureName];
    }
}

const featureDemoData: {
    [key in Features]?: FeatureDemoType
} = {
    newsfeed: {
        title: 'newsfeed',
        description: 'newsfeed-description',
        imageName: 'newsfeed-demo-image.svg',
    },
    tips: {
        title: 'tips',
        description: 'tips-description',
        imageName: 'tips-demo-image.svg',
    },
    todo: {
        title: 'to-do',
        description: 'to-do-description',
        imageName: 'todo-demo-image.svg',
    },
    menus: {
        title: 'menus',
        description: 'menus-description',
        imageName: 'menus-demo-image.svg',
    },
};

type Features = 'newsfeed' | 'todo' | 'menus' | 'tips';

interface FeatureDemoType {
    title: string;
    description: string;
    imageName: string;
}
