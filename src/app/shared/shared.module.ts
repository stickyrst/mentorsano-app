import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CircularProgressComponent } from './components/circular-progress/circular-progress.component';
import { UserProgressCirclesComponent } from './components/user-progress-circles/user-progress-circles.component';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';
import { RouterModule } from '@angular/router';
import { FeatureDemoHeaderComponent } from './components/feature-demo-header/feature-demo-header.component';

const components = [
    FeatureDemoHeaderComponent,
    CircularProgressComponent,
    UserProgressCirclesComponent,
    ProfilePreviewComponent,
];

@NgModule({
    declarations: [
        ...components,
    ],
    imports: [
        CommonModule,
        RouterModule,
    ],
    exports: [
        ...components,
    ],
})
export class SharedModule {
}
