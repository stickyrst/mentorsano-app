import { Injectable } from '@angular/core';
import { SimpleTextPageConfigModel } from '@feats/simple-text/model/simple-text-page-config.model';

@Injectable({
    providedIn: 'root',
})
export class SimpleTextService {

    constructor() {
    }

    private list: SimpleTextPageConfigModel[] = [
        {
            id: 'follow-program',
            title: 'follow-program-title',
            nextRoute: '/simple-text',
            nextRouteQuery: {id: 'reach-goal'},
            blockMenu: true,
        },
        {
            id: 'reach-goal',
            title: 'reach-goal-title',
            text: 'reach-goal-text',
            // nextRoute: '/advices',
            nextRoute: '/home/articles',
            nextRouteQuery: {type: 'advice'},
            blockMenu: true,
        },

    ];

    public get(id: string): SimpleTextPageConfigModel {
        const config = this.list.find(c => c.id === id);
        return config;
    }
}
