import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SimpleTextPageRoutingModule } from './simple-text-routing.module';

import { SimpleTextPage } from './page/simple-text.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SimpleTextPageRoutingModule,
    ],
    declarations: [SimpleTextPage],
})
export class SimpleTextPageModule {
}
