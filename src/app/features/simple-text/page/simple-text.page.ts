import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslationsService } from '@core/services/translations.service';
import { SimpleTextService } from '@feats/simple-text/services/simple-text.service';
import { SimpleTextPageConfigModel } from '@feats/simple-text/model/simple-text-page-config.model';
import { MenuController, NavController } from '@ionic/angular';

@Component({
    selector: 'app-simple-text',
    templateUrl: './simple-text.page.html',
    styleUrls: ['./simple-text.page.scss'],
})
export class SimpleTextPage {

    public config: SimpleTextPageConfigModel;

    constructor(
        private route: ActivatedRoute,
        public trans: TranslationsService,
        private simpleTextService: SimpleTextService,
        private navController: NavController,
        private zone: NgZone,
        private menuController: MenuController,
    ) {
        this.route.queryParamMap.subscribe(async paramMap => {
            if (paramMap.has('id')) {
                await this.zone.run(async () => {
                    this.config = this.simpleTextService.get(paramMap.get('id'));
                    await this.menuController.enable(!this.config.blockMenu);
                });
            }
        });
    }

    public async next() {
        if (!this.config) {
            await this.navController.navigateForward('/');
            return;
        }
        if (this.config.nextRoute) {
            await this.navController.navigateBack(this.config.nextRoute, {queryParams: this.config.nextRouteQuery});
            return;
        }
        if (this.config.nextAction && typeof this.config.nextAction === 'function') {
            this.config.nextAction();
        }
    }

    public async ionViewWillLeave() {
        await this.menuController.enable(true);
    }
}
