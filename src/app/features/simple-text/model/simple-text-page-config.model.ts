import { Params } from '@angular/router';

export interface SimpleTextPageConfigModel {
    id: string;
    title?: string;
    text?: string;
    nextRoute?: string;
    nextRouteQuery?: Params;
    nextAction?: () => void;
    blockMenu?: boolean;
}
