import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimpleTextPage } from './page/simple-text.page';

const routes: Routes = [
    {
        path: '',
        component: SimpleTextPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class SimpleTextPageRoutingModule {
}
