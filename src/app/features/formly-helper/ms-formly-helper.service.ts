import { Injectable } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Injectable({
    providedIn: 'root',
})
export class MsFormlyHelperService {

    constructor() {
    }

    public patchForCheckForm(fields: FormlyFieldConfig[]) {
        if (!fields) {
            return;
        }
        for (const field of fields) {
            switch (field.type) {
                case 'checkbox':
                    field.type = 'ms-check';
                    break;
                case 'input':
                    field.type = 'ms-input';
                    break;
                case 'custom-select':
                    field.type = 'ms-custom-select';
                    break;
                case 'select':
                    field.type = 'ms-select';
                    break;
            }
        }
    }
}
