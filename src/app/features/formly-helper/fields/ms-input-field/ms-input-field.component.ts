import { Component, OnInit } from '@angular/core';
import { FieldType, FormlyFieldConfig } from '@ngx-formly/core';
import { TranslationsService } from '@core/services/translations.service';
import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';

@Component({
    selector: 'app-ms-input-field',
    templateUrl: './ms-input-field.component.html',
    styleUrls: ['./ms-input-field.component.scss'],
})
export class MsInputFieldComponent extends FieldType<CheckQuestionFormlyFieldConfig> implements OnInit {

    public value;

    constructor(public trans: TranslationsService) {
        super();
    }

    public ngOnInit(): void {
        this.value = this.formControl.value;
        setTimeout(() => {
            this.field.showActivityTime = false;
            this.field.hideTimeBar = true;
        });
    }

    public inputValueChanged() {
        this.formControl.setValue(this.value);
    }
}
