import { Component, OnInit } from '@angular/core';
import { BaseCustomSelectDirective } from '@feats/formly-helper/components/base-custom-select/base-custom-select.directive';

@Component({
    selector: 'app-custom-select',
    templateUrl: './simple-custom-select.component.html',
    styleUrls: ['./simple-custom-select.component.scss'],
})
export class SimpleCustomSelectComponent extends BaseCustomSelectDirective {

}
