import { Component, OnInit } from '@angular/core';
import { BaseCustomSelectDirective } from '@feats/formly-helper/components/base-custom-select/base-custom-select.directive';

@Component({
    selector: 'app-ms-custom-select',
    templateUrl: './ms-custom-select.component.html',
    styleUrls: ['./ms-custom-select.component.scss'],
})
export class MsCustomSelectComponent extends BaseCustomSelectDirective {
}
