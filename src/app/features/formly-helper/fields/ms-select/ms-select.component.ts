import { Component, ViewChild } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { IonSelect } from '@ionic/angular';

@Component({
    selector: 'app-ms-select',
    templateUrl: './ms-select.component.html',
    styleUrls: ['./ms-select.component.scss'],
})
export class MsSelectComponent extends FieldType {
    @ViewChild(IonSelect) public select: IonSelect;

    public open() {
        this.select.open().then();
    }

    public clicked($event: MouseEvent) {
        $event.stopPropagation();
    }
}
