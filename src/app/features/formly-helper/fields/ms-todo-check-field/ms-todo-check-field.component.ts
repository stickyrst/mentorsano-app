import { Component } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { TranslationsService } from '@core/services/translations.service';
import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';

@Component({
    selector: 'app-ms-todo-check-field',
    templateUrl: './ms-todo-check-field.component.html',
    styleUrls: ['./ms-todo-check-field.component.scss'],
})
export class MsTodoCheckFieldComponent extends FieldType<CheckQuestionFormlyFieldConfig> {
    public get isChecked(): boolean {
        return this.model && this.model[this.field.key as string];
    }

    constructor(public trans: TranslationsService) {
        super();
    }

    public checkedChanged(value: boolean) {
        if (value !== this.formControl.value) {
            this.formControl.setValue(value);
            if (value && this.field.templateOptions.animateShow) {
                this.field.templateOptions.animateHide.emit();
            }
        }
    }
}
