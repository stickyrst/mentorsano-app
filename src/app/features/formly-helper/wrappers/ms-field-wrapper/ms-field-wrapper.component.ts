import { Component, ElementRef, HostBinding, Input, OnInit, ViewChild } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { FieldWrapper } from '@ngx-formly/core';
import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';
import { AnimationController, Animation } from '@ionic/angular';

@Component({
    selector: 'app-ms-field-wrapper',
    templateUrl: './ms-field-wrapper.component.html',
    styleUrls: ['./ms-field-wrapper.component.scss'],
})
export class MsFieldWrapperComponent extends FieldWrapper<CheckQuestionFormlyFieldConfig> implements OnInit {
    // public recipesTagsParam: string;

    @ViewChild('card', {read: ElementRef})
    public card: ElementRef;

    @HostBinding('class.force-hidden') public forceHidden = false;

    @Input()
    public field: CheckQuestionFormlyFieldConfig;

    constructor(
        public trans: TranslationsService,
        private animationCtrl: AnimationController,
        private elRef: ElementRef,
    ) {
        super();
    }

    public ngOnInit(): void {
        // if (this.field.showRecipesLink && this.field.recipesTags) {
        //     this.recipesTagsParam = this.field.recipesTags.map(t => t.id).join();
        // }

        if (this.field.templateOptions.isHidden) {
            this.forceHidden = true;
        }

        this.field.templateOptions.animateShow?.subscribe(() => {
            this.triggerHideAnimations(true);
        });

        this.field.templateOptions.animateHide?.subscribe(() => {
            this.triggerHideAnimations();
        });
    }


    public triggerHideAnimations(show?: boolean) {
        const ranges = {
            opacity: ['1', '0'],
            height: [this.card.nativeElement.clientHeight + 10, 0],
        };
        if (show) {
            ranges.opacity = [ranges.opacity[1], ranges.opacity[0]];
            ranges.height = [ranges.height[1], ranges.height[0]];
        }

        const opacityAnimation = () => {
            return this.animationCtrl.create('')
                .addElement(this.elRef.nativeElement)
                .duration(500)
                .fromTo('opacity', ranges.opacity[0], ranges.opacity[1])
                .play();
        };
        const heightAnimation = () => {
            return this.animationCtrl.create('')
                .addElement(this.elRef.nativeElement)
                .duration(250)
                .keyframes([
                    {offset: 0, height: ranges.height[0] + 'px'},
                    {offset: 1, height: ranges.height[1] + 'px'},
                ])
                .play();
        };
        if (show) {
            this.field.templateOptions.isHidden = false;
            heightAnimation().then(() => {
                opacityAnimation().then(() => {
                });
            });
        } else {
            opacityAnimation().then(() => {
                heightAnimation().then(() => {
                    this.field.templateOptions.isHidden = true;
                });
            });
        }
    }
}
