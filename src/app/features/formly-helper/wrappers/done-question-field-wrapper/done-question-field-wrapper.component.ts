import { Component } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
    selector: 'app-done-question-field-wrapper',
    templateUrl: './done-question-field-wrapper.component.html',
    styleUrls: ['./done-question-field-wrapper.component.scss'],
})
export class DoneQuestionFieldWrapperComponent extends FieldWrapper {
    public get keyStr(): string {
        return this.field.key as string;
    }
}
