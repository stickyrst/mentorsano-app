import { AfterViewInit, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
    selector: 'app-measure-unit-field-wrapper',
    templateUrl: './measure-unit-field-wrapper.component.html',
    styleUrls: ['./measure-unit-field-wrapper.component.scss'],
})
export class MeasureUnitFieldWrapperComponent extends FieldWrapper {
}
