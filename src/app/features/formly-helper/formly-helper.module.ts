import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyIonicModule } from '@ngx-formly/ionic';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SimpleCustomSelectComponent } from './fields/simple-custom-select/simple-custom-select.component';
import { CustomSelectModalComponent } from './components/custom-select-modal/custom-select-modal.component';
import { CoreModule } from '@core/core.module';
import { MsTodoCheckFieldComponent } from './fields/ms-todo-check-field/ms-todo-check-field.component';
import { RouterModule } from '@angular/router';
import { SwiperControlComponent } from './components/swiper-control/swiper-control.component';
import { MsFieldWrapperComponent } from './wrappers/ms-field-wrapper/ms-field-wrapper.component';
import { MsInputFieldComponent } from './fields/ms-input-field/ms-input-field.component';
import { MeasureUnitFieldWrapperComponent } from './wrappers/measure-unit-field-wrapper/measure-unit-field-wrapper.component';
import { MsCustomSelectComponent } from '@feats/formly-helper/fields/ms-custom-select/ms-custom-select.component';
import { MsSelectComponent } from '@feats/formly-helper/fields/ms-select/ms-select.component';
import { DoneQuestionFieldWrapperComponent } from './wrappers/done-question-field-wrapper/done-question-field-wrapper.component';

export function minlengthValidationMessage(err, field) {
    return `Should have at least ${field.templateOptions.minLength} characters`;
}

export function maxlengthValidationMessage(err, field) {
    return `This value should be less than ${field.templateOptions.maxLength} characters`;
}

export function minValidationMessage(err, field) {
    return `This value should be more than ${field.templateOptions.min}`;
}

export function maxValidationMessage(err, field) {
    return `This value should be less than ${field.templateOptions.max}`;
}

@NgModule({
    declarations: [
        SimpleCustomSelectComponent,
        CustomSelectModalComponent,
        MsTodoCheckFieldComponent,
        SwiperControlComponent,
        MsFieldWrapperComponent,
        MsInputFieldComponent,
        MeasureUnitFieldWrapperComponent,
        MsCustomSelectComponent,
        MsSelectComponent,
        DoneQuestionFieldWrapperComponent,
    ],
    imports: [
        CommonModule,
        FormlyIonicModule,
        FormlyModule.forRoot({
            types: [
                {
                    name: 'custom-select',
                    component: SimpleCustomSelectComponent,
                },
                {
                    name: 'ms-check',
                    component: MsTodoCheckFieldComponent,
                    wrappers: ['ms-field-wrapper'],
                },
                {
                    name: 'ms-input',
                    component: MsInputFieldComponent,
                    wrappers: ['ms-field-wrapper'],
                },
                {
                    name: 'ms-custom-select',
                    component: MsCustomSelectComponent,
                    wrappers: ['ms-field-wrapper'],
                },
                {
                    name: 'ms-select',
                    component: MsSelectComponent,
                    wrappers: ['ms-field-wrapper'],
                },
            ],
            validators: [
                {name: 'email', validation: Validators.email},
            ],
            validationMessages: [
                {name: 'email', message: 'Invalid email format'},
                {name: 'required', message: 'This field is required'},
                {name: 'minlength', message: minlengthValidationMessage},
                {name: 'maxlength', message: maxlengthValidationMessage},
                {name: 'min', message: minValidationMessage},
                {name: 'max', message: maxValidationMessage},
            ],
            wrappers: [
                {
                    name: 'measure-unit',
                    component: MeasureUnitFieldWrapperComponent,
                },
                {
                    name: 'ms-field-wrapper',
                    component: MsFieldWrapperComponent,
                },
                {
                    name: 'done-question-wrapper',
                    component: DoneQuestionFieldWrapperComponent,
                },
            ],
        }),
        IonicModule,
        FormsModule,
        ReactiveFormsModule,
        CoreModule,
        RouterModule,
    ],
    exports: [
        FormlyModule,
        MsFieldWrapperComponent,
    ],
})
export class FormlyHelperModule {
    public static forRoot(): ModuleWithProviders<FormlyHelperModule> {
        return {
            ngModule: FormlyHelperModule,
            providers: [],
        };
    }
}
