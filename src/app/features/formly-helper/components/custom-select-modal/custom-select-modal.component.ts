import { Component, Input, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormlyTemplateOptions } from '@ngx-formly/core';
import { ModalController } from '@ionic/angular';
import { TranslationsService } from '@core/services/translations.service';
import { GlobalsService } from '@core/services/globals.service';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';

@Component({
    selector: 'app-custom-modal',
    templateUrl: './custom-select-modal.component.html',
    styleUrls: ['./custom-select-modal.component.scss'],
})
export class CustomSelectModalComponent implements OnInit, OnDestroy {

    @Input()
    public model: any[];
    @Input()
    public templateOptions: FormlyTemplateOptions;

    @ViewChildren('otherValueInputs')
    public otherValueInputs: QueryList<any>;

    constructor(
        public modalController: ModalController,
        public trans: TranslationsService,
        private blockHwdBackBtn: BlockHwdBackButtonService,
    ) {
    }

    public selected: boolean[];
    public others: { value?: string }[] = [];

    public otherFieldLabel: string;

    public options: any[] = [];

    public get origOptions(): any[] {
        return this.to.options as any[];
    }

    public get to(): FormlyTemplateOptions {
        return this.templateOptions;
    }

    public get multipleOther(): boolean {
        return this.to.lastIsOther && this.to.multipleOther;
    }

    public ngOnInit() {
        if (!this.model) {
            this.model = [];
        }
        if (this.to.lastIsOther) {
            this.otherFieldLabel = this.origOptions[this.origOptions.length - 1].label;
            this.options = this.origOptions.slice(0, this.options.length - 1);
        } else {
            this.options = this.origOptions;
        }
        this.selected = this.options.map(_ => false);
        const tmpModel = this.model;
        this.others = [];
        for (const optVal of tmpModel) {
            const indexOfOption = this.options.indexOf(this.options.find(o => o.value === optVal));
            if (indexOfOption !== -1) {
                this.selected[indexOfOption] = true;
            } else {
                if (this.to.lastIsOther) {
                    this.others.push({value: optVal});
                }
            }
        }
        this.blockHwdBackBtn.push({
            block: true, ref: this,
            oneTimeBlockCallback: async () => {
                await this.modalController.dismiss();
            },
        });
    }

    public optionChanged(index: number) {
        if (this.to.firstIsNo && this.selected[index]) {
            if (index === 0) {
                for (let i = 1; i < this.options.length; i++) {
                    this.selected[i] = false;
                }
                this.others = [];
            } else {
                this.selected[0] = false;
            }
        }
        if (!this.to.multiple && this.selected[index]) {
            for (let i = 0; i < this.options.length; i++) {
                if (i === index) {
                    continue;
                }
                this.selected[i] = false;
            }
        }
    }

    public async save() {
        let finalModel = [];
        for (let i = 0; i < this.options.length; i++) {
            if (this.selected[i]) {
                finalModel.push(this.options[i].value);
            }
        }
        if (this.to.lastIsOther) {
            finalModel.push(...this.others.map(o => o.value).filter(o => o));
        }
        if (!finalModel.length) {
            finalModel = undefined;
        }
        await this.modalController.dismiss(finalModel, 'ok');
    }

    public ngOnDestroy(): void {
    }
}
