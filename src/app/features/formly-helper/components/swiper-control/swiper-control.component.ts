import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';

@Component({
    selector: 'app-swiper-control',
    templateUrl: './swiper-control.component.html',
    styleUrls: ['./swiper-control.component.scss'],
})
export class SwiperControlComponent {

    @ViewChild(IonSlides)
    public ionSlides: IonSlides;

    @Input()
    public isChecked: boolean;

    @Output()
    public changed: EventEmitter<boolean> = new EventEmitter<boolean>();

    public swiperInitialized: boolean;
    public swiperIsTouched: boolean;

    public slideOpts = {
        initialSlide: 1,
        speed: 400,
        spaceBetween: -50,
    };

    public ionSlidesDidLoad($event) {
        this.swiperInitialized = true;
        if (this.isChecked) {
            this.ionSlides.slideTo(0).then();
        }
    }

    public ionSlidDrag($event) {
        this.swiperIsTouched = true;
    }

    public ionSlideTouchEnd($event) {
        setTimeout(() => {
            this.swiperIsTouched = false;
        }, 500);
    }

    public ionSlideDidChange($event) {
        if (!this.swiperInitialized) {
            return;
        }
        this.ionSlides.getActiveIndex().then(index => {
            if (index === 0 !== this.isChecked) {
                // this.formControl.setValue(index === 0);
                this.changed.emit(index === 0);
            }
        });
    }
}
