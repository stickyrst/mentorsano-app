import { Directive, OnInit } from '@angular/core';
import { FieldType } from '@ngx-formly/core';
import { CustomSelectModalComponent } from '@feats/formly-helper/components/custom-select-modal/custom-select-modal.component';
import { ModalController } from '@ionic/angular';
import { DisplayMessageService } from '@core/services/display-message.service';

@Directive()
export abstract class BaseCustomSelectDirective extends FieldType implements OnInit {

    constructor(
        public modalController: ModalController,
        private messages: DisplayMessageService,
    ) {
        super();
    }

    public ngOnInit() {
        this.formControl.valueChanges.subscribe(value => {
            this.processPreview();
        });
        this.processPreview();
    }

    private processPreview() {
        const value = this.formControl.value;
        if (!value || typeof value[Symbol.iterator] !== 'function' || value.length === 0) {
            this.to.valuePreview = '';
            return;
        }
        const vals = [];
        for (const optVal of value) {
            const indexOfOption = this.opts.indexOf(this.opts.find(o => o.value === optVal));
            if (indexOfOption !== -1) {
                vals.push(this.opts[indexOfOption].label);
            } else {
                if (this.to.lastIsOther) {
                    vals.push(optVal);
                }
            }
        }
        this.to.valuePreview = vals.join(', ');
    }

    public get opts(): any[] {
        return this.to.options as any[];
    }

    public async open() {
        await this.messages.clear();
        const modal = await this.modalController.create({
            component: CustomSelectModalComponent,
            componentProps: {
                model: this.model[this.field.key as string],
                templateOptions: this.to,
            },
            swipeToClose: true,
        });
        await modal.present();
        const result = await modal.onDidDismiss();
        if (result.role === 'ok') {
            this.formControl.setValue(result.data);
        }
        this.formControl.markAsTouched();
    }
}
