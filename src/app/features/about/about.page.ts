import { Component, NgZone, OnInit } from '@angular/core';
import { GlobalsService } from '@core/services/globals.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { TranslationsService } from '@core/services/translations.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
    selector: 'app-about',
    templateUrl: './about.page.html',
    styleUrls: ['./about.page.scss'],
})
export class AboutPage {

    private counter = 0;
    private counterTarget = 8;
    public version: string;

    constructor(
        private globals: GlobalsService,
        private messages: DisplayMessageService,
        public trans: TranslationsService,
        private navController: NavController,
        private appVersion: AppVersion,
        private zone: NgZone,
    ) {
        this.appVersion.getVersionNumber().then(version => {
            this.version = version;
        }).catch(() => {
            this.version = null;
        });
    }

    public async titleClick(): Promise<void> {
        this.counter++;
        if (this.counter === this.counterTarget) {
            this.counter = 0;
            this.globals.developer = !this.globals.developer;
            await this.messages.clear();
            await this.messages.display('Developer mode ' + (this.globals.developer ? 'en' : 'dis') + 'abled!');
            await this.navController.navigateRoot('/developer');
            return;
        }
        if (this.counter > 6) {
            await this.messages.display('Press ' + (this.counterTarget - this.counter) + ' more times.');
        }
    }
}
