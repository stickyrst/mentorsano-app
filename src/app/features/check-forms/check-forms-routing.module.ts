import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/check-form/check-form.module').then(m => m.CheckFormPageModule),
    },
    {
        path: ':type',
        loadChildren: () => import('./pages/check-form/check-form.module').then(m => m.CheckFormPageModule),
    },
    {
        path: '**',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CheckFormsRoutingModule {
}
