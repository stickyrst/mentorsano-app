import { EventEmitter, Injectable, Injector } from '@angular/core';
import { Storage } from '@ionic/storage';
import { CheckFormType } from '../models/check-form-type';
import { LoadingAbleService, LoadingAbleServiceConfig } from '@core/services/base-services/loading-able.service';
import { CheckFormModel } from '@feats/check-forms/models/check-form-model';
import { ConstantsService } from '@core/services/constants.service';
import { DateService } from '@core/services/date.service';
import { MsFormlyHelperService } from '@feats/formly-helper/ms-formly-helper.service';
import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';
import { NotifDataModel } from '@core/models/notif-data.model';

@Injectable({providedIn: 'root'})
export class CheckFormsService extends LoadingAbleService {

    constructor(
        private storage: Storage,
        injector: Injector,
        private dateService: DateService,
        private msFormlyHelperService: MsFormlyHelperService
    ) {
        super(injector);
    }

    public async getConfig(type: CheckFormType, config?: LoadingAbleServiceConfig): Promise<CheckFormModel> {
        let path = 'CheckForms/Get/' + type;
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            path = path + '?lang=' + lang;
        }

        const form = await this.get<CheckFormModel>(path, true, config);
        if (form.questions) {
            for (const qa of form.questions) {
                if (qa.activityTime && typeof qa.activityTime === 'string') {
                    if (qa.activityTime === '0001-01-01T00:00:00') {
                        qa.activityTime = null;
                    } else {
                        qa.activityTime = new Date(qa.activityTime);
                    }
                }
                this.patchCheckQuestionWithAnimation(qa);
            }
        }
        if (type === 'checkIn') {
            this.msFormlyHelperService.patchForCheckForm(form.questions);
        } else {
            this.addAnsweredQuestionWrapper(form.questions);
        }
        return form;
    }

    private patchCheckQuestionWithAnimation(config: CheckQuestionFormlyFieldConfig) {
        const to = config.templateOptions;
        if (!to) {
            return;
        }
        to.animateHide = new EventEmitter<void>();
        to.animateShow = new EventEmitter<void>();
    }

    public async getTimeFor(type: CheckFormType): Promise<Date> {
        const path = 'CheckForms/GetTimeFor/' + type;
        const dateStr = await this.get<string>(path, true, {uiSilent: true});
        return new Date(dateStr);
    }

    public async getResponse(type: CheckFormType, config?: LoadingAbleServiceConfig): Promise<any> {
        const path = 'CheckFormResponses/GetFor/' + type + '?currentDateTime=' + this.dateService.currentDate.toISOString();
        const response = await this.get<CheckFormModel>(path, true, config);
        if (typeof response.day === 'string') {
            response.day = new Date(response.day);
        }
        return response;
    }

    public async saveResponse(type: CheckFormType, response: any): Promise<number> {
        const path = 'CheckFormResponses/SaveFor/' + type + '?currentDateTime=' + this.dateService.currentDate.toISOString();
        return await this.post(path, {data: response}, {uiSilent: true});
    }

    public async getFor(type: CheckFormType, showAlertOnError?: boolean):
        Promise<ICheckFormPair> {

        try {
            await this.toggleLoadingDialog(this.config, true);
            const resp = await Promise.all([
                this.getResponse(type, {uiSilent: true, goBackOnError: true}),
                this.getConfig(type, {uiSilent: true, goBackOnError: true}),
            ]);
            await this.toggleLoadingDialog(this.config, false);
            const result: ICheckFormPair = {config: resp[1], response: resp[0]};

            if (result.response && result.response.data) {
                for (const question of result.config.questions) {
                    if (result.response.data[question.key as string] === true) {
                        question.templateOptions.isHidden = true;
                    }
                }
            }

            return result;
        } catch (e) {
            await this.loading.dismiss();
            await this.messages.showError(e, showAlertOnError);
            throw  e;
        }
    }

    public async getMealsNotifications(config?: LoadingAbleServiceConfig): Promise<NotifDataModel[]> {
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        let path = 'CheckForms/GetLocalNotifications/';
        if (lang) {
            path = path + '?lang=' + lang;
        }
        return await this.get<NotifDataModel[]>(path, true, config);
    }

    private addAnsweredQuestionWrapper(questions: CheckQuestionFormlyFieldConfig[]) {
        const needsFormField = ['select', 'checkbox'];
        for (const question of questions) {
            question.wrappers = question.wrappers || [];
            // if (question.wrappers.indexOf('measure-unit') === -1 && question.type !== 'custom-select' || question.type === 'select') {
            //     question.wrappers.push('form-field');
            // }
            if (needsFormField.indexOf(question.type) !== -1) {
                question.wrappers.push('form-field');
            }
            question.wrappers = [
                'done-question-wrapper',
                ...question.wrappers,
            ];
        }
    }
}

interface ICheckFormPair {
    config: CheckFormModel;
    response: { data: { [key: string]: any }, percent: number, day: Date };
}
