import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckFormsRoutingModule } from './check-forms-routing.module';
import { CheckFormsService } from './services/check-forms.service';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        CheckFormsRoutingModule,
    ],
    providers: [
        CheckFormsService,
    ],
})
export class CheckFormsModule {
}
