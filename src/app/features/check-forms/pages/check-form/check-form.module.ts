import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CheckFormPageRoutingModule } from './check-form-routing.module';

import { CheckFormPage } from './check-form.page';
import { FormlyHelperModule } from '@feats/formly-helper/formly-helper.module';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CheckFormPageRoutingModule,
        ReactiveFormsModule,
        FormlyHelperModule,
        CoreModule,
        SharedModule,
    ],
    declarations: [CheckFormPage],
})
export class CheckFormPageModule {
}
