import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CheckFormsService } from '../../services/check-forms.service';
import { CheckFormModel } from '@feats/check-forms/models/check-form-model';
import { FormGroup } from '@angular/forms';
import { TranslationsService } from '@core/services/translations.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { CheckFormType } from '@feats/check-forms/models/check-form-type';
import { DateService } from '@core/services/date.service';
import { debounce } from 'rxjs/operators';
import { Subscription, timer } from 'rxjs';
import { MeasureUnitsService } from '@core/services/measure-units.service';
import { NavController } from '@ionic/angular';
import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';
import { UserActivityService } from '@feats/user-activity/services/user-activity.service';
import { UserDefinedTodosService } from '@feats/user-defined-todos/services/user-defined-todos.service';

@Component({
    selector: 'app-check-form',
    templateUrl: './check-form.page.html',
    styleUrls: ['./check-form.page.scss'],
})
export class CheckFormPage implements OnInit, OnDestroy {

    public isSubmitting: boolean;
    private isSomethingNewToSave: boolean;
    public checkForm: CheckFormModel;

    public form = new FormGroup({});
    public model = {};
    private justLoaded = true;

    public loadBarValue: number;

    public showLinkToCheckOut: boolean;
    public checkoutType: 'daily' | 'weekly';

    private lastValue: any;

    private debouncingFields: string[] = [];

    public canShowHiddenTasks: boolean;
    public thereAreDoneTasks: boolean;

    public type: CheckFormType;

    private langChangedSubscription: Subscription;
    private checkInReloadSubscription: Subscription;

    public get fields(): CheckQuestionFormlyFieldConfig[] {
        return this.checkForm && this.checkForm.questions;
    }

    constructor(
        private route: ActivatedRoute,
        private checkForms: CheckFormsService,
        public trans: TranslationsService,
        private messages: DisplayMessageService,
        private dateService: DateService,
        private measureUnitsService: MeasureUnitsService,
        private navCtrl: NavController,
        private userActivityService: UserActivityService,
        private userDefinedTodosService: UserDefinedTodosService,
    ) {
        this.type = route.snapshot.params.type || 'checkIn';
        this.checkForms.setDefaultConfig();
    }

    public ngOnInit() {
        this.form.valueChanges.pipe(
            debounce(value => timer(this.isDebouncingFieldChanged(value, this.lastValue) ? 3000 : 1000))
        ).subscribe(value => {
            if (this.justLoaded) {
                this.justLoaded = false;
                return;
            }
            this.lastValue = value;
            this.save().then();
        });
        this.load().then();

        this.langChangedSubscription = this.trans.languageChanged.subscribe(x => {
            this.load().then();
        });

        if (this.type === 'checkIn') {
            this.checkInReloadSubscription = this.userDefinedTodosService.forceReloadCheckIn.subscribe(_ => {
                this.load().then();
            });
        }
    }


    public async save(): Promise<void> {
        if (this.isSubmitting) {
            this.isSomethingNewToSave = true;
            return;
        }
        this.isSomethingNewToSave = false;
        this.isSubmitting = true;

        let formResp = Object.assign({}, this.model);
        for (const pKey in formResp) {
            if (formResp.hasOwnProperty(pKey) && !this.checkForm.questions.find(q => q.key === pKey)) {
                delete formResp[pKey];
            }
        }
        try {
            formResp = await this.measureUnitsService.patchForSubmit(this.fields, formResp);
            const resp = await this.checkForms.saveResponse(this.checkForm.type, formResp);
            this.userActivityService.clearCache();
            if (typeof resp === 'number') {
                this.loadBarValue = resp / 100;
            }
        } catch (e) {
            await this.messages.showError(e);
        }
        this.isSubmitting = false;
        if (this.isSomethingNewToSave) {
            await this.save();
        }
    }

    private async load() {
        if (this.type === 'checkOut') {
            this.type = this.dateService.currentDate.getDay() !== 0 ? 'dailyCheckOut' : 'weeklyCheckOut';
        }
        let config, response;
        try {
            const result = await this.checkForms.getFor(this.type, true);
            config = result.config;
            response = result.response;
        } catch (e) {
            console.error(e);
            await this.navCtrl.back();
            return;
        }

        await this.measureUnitsService.patchForDisplay(config.questions, response.data, {
            showModalIfNotSet: true,
            skipLabelEdit: true,
            addMeasureUnitWrapper: true,
        });

        this.lastValue = this.model = response.data;
        if (typeof response.percent === 'number') {
            this.loadBarValue = response.percent / 100;
        }
        this.checkForm = config;

        this.debouncingFields = this.checkForm.questions.filter(q => q.type === 'input').map(q => q.key as string);

        if (this.type === 'checkIn') {
            this.shouldDisplayLinkToCheckOut().then(should => {
                this.showLinkToCheckOut = should;
            });
        }
        this.updateCanShowHiddenTasks(this.model);
        this.form.valueChanges.subscribe(value => {
            setTimeout(() => {
                this.updateCanShowHiddenTasks(this.model);
            }, 1000);
        });
    }

    private async shouldDisplayLinkToCheckOut(): Promise<boolean> {
        this.checkoutType = this.dateService.currentDate.getDay() !== 0 ? 'daily' : 'weekly';
        const time = await this.checkForms.getTimeFor(this.checkoutType + 'CheckOut' as any);
        const now = this.dateService.currentDate;
        const diff = now.getTime() - new Date(now.getFullYear(), now.getMonth(), now.getDate(),
            time.getHours(), time.getMinutes(), time.getSeconds()).getTime();
        return diff > 0;
    }

    private isDebouncingFieldChanged(newValue: any, oldValue: any): boolean {
        if (!newValue || !oldValue) {
            return false;
        }
        for (const debouncingField of this.debouncingFields) {
            if (newValue[debouncingField] !== oldValue[debouncingField]) {
                return true;
            }
        }
        return false;
    }

    public toggleDoneTasksVisibility(show?: boolean) {
        for (const field of this.fields) {
            if (this.model[field.key as string] === true) {
                if (show) {
                    if (field.templateOptions.isHidden && field.templateOptions.animateShow) {
                        field.templateOptions.animateShow.emit();
                    }
                } else {
                    if (!field.templateOptions.isHidden && field.templateOptions.animateHide) {
                        field.templateOptions.animateHide.emit();
                    }
                }
            }
        }
        setTimeout(() => {
            this.updateCanShowHiddenTasks(this.model);
        }, 1000);
    }

    public updateCanShowHiddenTasks(value?: any) {
        this.canShowHiddenTasks = this.fields.some(f => value && value[f.key as string] === true && f.templateOptions.isHidden);
        this.thereAreDoneTasks = this.fields.some(f => value && value[f.key as string] === true);
    }

    public ngOnDestroy(): void {
        if (this.langChangedSubscription) {
            this.langChangedSubscription.unsubscribe();
            this.langChangedSubscription = null;
        }
        if (this.checkInReloadSubscription) {
            this.checkInReloadSubscription.unsubscribe();
            this.checkInReloadSubscription = null;
        }
    }
}
