import { CheckFormType } from './check-form-type';
import { CheckQuestionFormlyFieldConfig } from './check-question-formly-field-config';

export interface CheckFormModel {
    name: string;
    slug: string;
    type: CheckFormType;
    questions: CheckQuestionFormlyFieldConfig[];
    day?: Date;
}
