import { FormlyFieldConfig, FormlyTemplateOptions } from '@ngx-formly/core/lib/components/formly.field.config';
import { EventEmitter } from '@angular/core';

export interface CheckQuestionFormlyFieldConfig extends FormlyFieldConfig {
    activityTime: Date;
    showActivityTime: boolean;
    hideTimeBar: boolean;
    notification: 'push' | 'local' | 'none';
    showRecipesLink: boolean;
    recipesTags: { id: string }[];
    templateOptions: CheckQuestionFormlyTemplateOptions;
    category?: string;
}

export interface CheckQuestionFormlyTemplateOptions extends FormlyTemplateOptions {
    animateShow?: EventEmitter<void>;
    animateHide?: EventEmitter<void>;
    isHidden?: boolean;
}
