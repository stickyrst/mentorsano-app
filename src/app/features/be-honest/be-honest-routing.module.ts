import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeHonestPage } from './be-honest.page';

const routes: Routes = [
    {
        path: '',
        component: BeHonestPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class BeHonestPageRoutingModule {
}
