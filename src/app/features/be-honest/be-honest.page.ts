import { Component, OnInit } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { MenuController } from '@ionic/angular';
import { GlobalsService } from '@core/services/globals.service';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';

@Component({
    selector: 'app-be-honest',
    templateUrl: './be-honest.page.html',
    styleUrls: ['./be-honest.page.scss'],
})
export class BeHonestPage {
    constructor(
        public trans: TranslationsService,
        private menuController: MenuController,
        private blockHwdBackBtn: BlockHwdBackButtonService,
    ) {
    }

    public async ionViewWillEnter() {
        await this.menuController.enable(false);
        this.blockHwdBackBtn.push(true);
    }

    public async ionViewWillLeave() {
        await this.menuController.enable(true);
        this.blockHwdBackBtn.pop();
    }
}
