import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BeHonestPageRoutingModule } from './be-honest-routing.module';

import { BeHonestPage } from './be-honest.page';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        BeHonestPageRoutingModule,
    ],
    declarations: [BeHonestPage],
})
export class BeHonestPageModule {
}
