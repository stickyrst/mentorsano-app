import { Component } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-achievements',
    templateUrl: './achievements-page.component.html',
    styleUrls: ['./achievements-page.component.scss'],
})
export class AchievementsPage {

    public profileActivityIndex: number;

    constructor(
        public trans: TranslationsService,
        private route: ActivatedRoute,
    ) {
        this.profileActivityIndex = +this.route.snapshot.queryParams.profileActivityIndex;
    }
}
