import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AchievementsPage } from './achievements-page.component';

const routes: Routes = [
    {
        path: '',
        component: AchievementsPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class AchievementsPageRoutingModule {
}
