import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { AchievementsPage } from './achievements-page.component';
import { AchievementsPageRoutingModule } from '@feats/achievements/achievements-routing.module';
import { CoreModule } from '@core/core.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AchievementsPageRoutingModule,
        CoreModule,
    ],
    declarations: [AchievementsPage],
})
export class AchievementsPageModule {
}
