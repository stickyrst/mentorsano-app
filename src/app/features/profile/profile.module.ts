import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileService } from './services/profile.service';
import { FormsModule } from '@angular/forms';
import { MeasureUnitModalComponent } from './components/measure-unit-modal/measure-unit-modal.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
    declarations: [
        MeasureUnitModalComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        ProfileRoutingModule,
        FormsModule,
    ],
    providers: [
        ProfileService,
    ],
})
export class ProfileModule {
    public static forRoot(): ModuleWithProviders<ProfileModule> {
        return {
            ngModule: ProfileModule,
            providers: [
                ProfileService,
            ],
        };
    }
}
