import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditProfilePage } from './edit-profile.page';
import { FormConfigResolver } from '@core/resolvers/form-config.resolver';

const routes: Routes = [
    {
        path: '',
        component: EditProfilePage,
        resolve: {formConfig: FormConfigResolver},
        data: {formSchemaName: 'ProfileViewModel'},
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class EditProfilePageRoutingModule {
}
