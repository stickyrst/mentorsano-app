import { Component, OnInit, ViewChild } from '@angular/core';
import { ProfileService } from '../../services/profile.service';
import { ProfileModel } from '../../models/profile.model';
import { TranslationsService } from '@core/services/translations.service';
import { PredefinedFormComponent } from '@core/components/predefined-form/predefined-form.component';
import { Task } from '@core/models/task';
import { ModalController } from '@ionic/angular';
import { MeasureUnitsService } from '@core/services/measure-units.service';
import { LocalNotificationsHelperService } from '@core/services/local-notifications-helper.service';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.page.html',
    styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {

    @ViewChild('profileForm', {static: true}) public profileForm: PredefinedFormComponent;

    public profile: ProfileModel;
    public measureUnitSystem: string;

    constructor(private profileService: ProfileService,
                public transService: TranslationsService,
                private modalController: ModalController,
                private measureUnitsService: MeasureUnitsService,
                private localNotifService: LocalNotificationsHelperService
    ) {
        this.profileService.setDefaultConfig();
    }

    public async ngOnInit() {
        this.profile = await this.profileService.getOwn();
        if (!this.profile) {
            this.profile = {} as any;
            return;
        }
        if (this.transService.currentLanguage && this.profile.language !== this.transService.currentLanguage) {
            this.profile.language = this.transService.currentLanguage;
            await this.submit();
        }
        this.measureUnitSystem = (await this.measureUnitsService.getMeasureUnitSystem()) || 'metric';
    }

    public async submit() {
        await this.profileService.saveOwn(this.profile);
        this.profileForm.markAsClean();
        await this.checkAndFixLanguage();
        await this.localNotifService.setForMeals();
    }

    private async checkAndFixLanguage(): Task {
        if (this.profile && this.profile.language) {
            await this.transService.loadLanguageIfIsNew(this.profile.language);
        }
    }

    public async openMeasureUnitModal() {
        await this.measureUnitsService.presentUnitSystemAlert();
        this.measureUnitSystem = await this.measureUnitsService.getMeasureUnitSystem();
    }
}
