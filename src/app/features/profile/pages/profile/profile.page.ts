import { Component, OnInit } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { UserActivityService } from '@feats/user-activity/services/user-activity.service';
import { UserActivityModel } from '@feats/user-activity/models/user-activity.model';
import { MeasureUnitsService } from '@core/services/measure-units.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

    public model: UserActivityModel;
    public volumeMeasureUnit: string;
    public weightMeasureUnit: string;
    public measureUnitSystem: 'metric' | 'imperial';
    public recommendedWaterAmount: number;
    public language: string;
    public idealWeight: {minimum?: number, maximum?: number} = {};

    constructor(
        public trans: TranslationsService,
        private userActivityService: UserActivityService,
        private measureUnitsService: MeasureUnitsService,
    ) {
    }

    public async ngOnInit() {
        this.model = await this.userActivityService.getForProfilePage();
        this.afterLoadData().then();
    }

    public ionViewDidEnter() {
        this.afterLoadData().then();
    }

    public async afterLoadData() {
        this.measureUnitSystem = (await this.measureUnitsService.getMeasureUnitSystem()) || 'metric';
        this.language = this.trans.currentLanguage;
        if (this.measureUnitSystem === 'imperial') {
            this.volumeMeasureUnit = 'oz';
            this.weightMeasureUnit = 'lbs';
            if (this.model) {
                this.recommendedWaterAmount =
                    this.measureUnitsService.convertUnit(this.model.recommendedWaterAmount, 'volume', 'imperial');
                this.idealWeight.minimum = this.measureUnitsService.convertUnit(this.model.minimumIdealWeight, 'weight', 'imperial');
                this.idealWeight.maximum = this.measureUnitsService.convertUnit(this.model.maximumIdealWeight, 'weight', 'imperial');
            }
        } else {
            this.volumeMeasureUnit = 'ml';
            this.weightMeasureUnit = 'kg';
            this.recommendedWaterAmount = this.model && this.model.recommendedWaterAmount;
            this.idealWeight.minimum = this.model && this.model.minimumIdealWeight || 0;
            this.idealWeight.maximum = this.model && this.model.maximumIdealWeight || 0;
        }
    }
}
