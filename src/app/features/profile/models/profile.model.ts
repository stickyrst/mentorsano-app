import { TagModel } from './tag.model';

export interface ProfileModel {
    firstName: string;
    lastName: string;
    language: string;
    tags: TagModel[];
    bmi: number;
    bmiCategory: string;
    hasPremium: boolean;
}
