import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MeasureUnitsService } from '@core/services/measure-units.service';

@Component({
    selector: 'app-measure-unit-modal',
    templateUrl: './measure-unit-modal.component.html',
    styleUrls: ['./measure-unit-modal.component.scss'],
})
export class MeasureUnitModalComponent implements OnInit {
    public measureUnit;

    constructor(private measureUnitsService: MeasureUnitsService,
                private modalCtrl: ModalController) {
    }

    public async ngOnInit() {
        this.measureUnit = await this.measureUnitsService.getMeasureUnitSystem() || 'metric';
    }

    public async save() {
        await this.measureUnitsService.setMeasureUnit(this.measureUnit);
        await this.modalCtrl.dismiss({ measureUnit: this.measureUnit });
    }

    public changeValue(event) {
        this.measureUnit = event.detail.value;
    }

    public async ionViewWillLeave() {
        await this.measureUnitsService.setMeasureUnit(this.measureUnit);
    }
}
