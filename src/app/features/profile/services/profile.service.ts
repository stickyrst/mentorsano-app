import { Injectable, Injector } from '@angular/core';
import { ProfileModel } from '../models/profile.model';
import { OwnedEntityService } from '@auth/services/owned-entity.service';

@Injectable()
export class ProfileService extends OwnedEntityService<ProfileModel> {
    constructor(injector: Injector) {
        super(injector);
        this.controllerName = 'Profiles';
    }
}
