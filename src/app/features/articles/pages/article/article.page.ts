import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleModel } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';
import { TranslationsService } from '@core/services/translations.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { AdmobService } from '@core/services/admob.service';

@Component({
    selector: 'app-article',
    templateUrl: './article.page.html',
    styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

    public article: ArticleModel;
    private readonly articleId: string;

    public body: SafeHtml;

    constructor(
        private route: ActivatedRoute,
        private articlesService: ArticlesService,
        public trans: TranslationsService,
        private socialSharing: SocialSharing,
        private zone: NgZone,
        private sanitizer: DomSanitizer,
        private admobService: AdmobService,
    ) {
        this.articleId = this.route.snapshot.params.id;
        this.articlesService.setDefaultConfig();
    }

    public async ngOnInit(): Promise<void> {
        this.zone.run(async () => {
            this.article = await this.articlesService.getOne(this.articleId);
            this.body = this.sanitizer.bypassSecurityTrustHtml(this.article.body);
            this.articlesService.setIdAsRead(this.articleId).then();
        }).then();
        this.admobService.showInterstitial();
    }

    public share() {
        this.socialSharing.share('Shared via MentorSano', null, null, this.article.externalUrl).then();
    }
}
