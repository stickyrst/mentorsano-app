import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArticlesPageRoutingModule } from './articles-routing.module';

import { ArticlesPage } from './articles.page';
import { ArticlePreviewComponent } from '../../components/article-preview/article-preview.component';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ArticlesPageRoutingModule,
        SharedModule,
    ],
    declarations: [
        ArticlesPage,
        ArticlePreviewComponent,
    ],
})
export class ArticlesPageModule {
}
