import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { ArticlesService } from '../../services/articles.service';
import { ArticleModel } from '../../models/article.model';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { ArticleType } from '@feats/articles/models/article.type';
import { PremiumService } from '@feats/premium/services/premium.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Task } from '@core/models/task';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.page.html',
    styleUrls: ['./articles.page.scss'],
})
export class ArticlesPage implements OnInit, OnDestroy {

    public filteredArticles: ArticleModel[];
    public allArticles: ArticleModel[];

    private autoOpenId: string;

    public type: ArticleType;

    public category: string;
    // private recipesTagsFilter: string;

    private premiumListenerSubscription: Subscription;
    private readChangedSubscription: Subscription;

    constructor(
        public trans: TranslationsService,
        private articlesService: ArticlesService,
        private navController: NavController,
        private route: ActivatedRoute,
        private premiumService: PremiumService,
    ) {
        this.articlesService.setDefaultConfig();
        this.route.queryParams.subscribe(queryParams => {
            const type = queryParams.hasOwnProperty('type') ? queryParams.type : 'anyArticleMenu';
            if (!this.type || this.type !== type) {
                this.type = queryParams.type;
                this.clearArticles().then(() => {
                    this.getArticles().then();
                });
            }
            if (queryParams.hasOwnProperty('autoOpen')) {
                if (this.autoOpenId !== queryParams.autoOpen) {
                    this.autoOpenId = queryParams.autoOpen;
                    this.tryAutoOpen().then();
                }
            } else {
                this.autoOpenId = null;
            }
            this.category = queryParams.hasOwnProperty('category') ? queryParams.category : undefined;
            // this.recipesTagsFilter = queryParams.hasOwnProperty('recipesTagsParam') ? queryParams.recipesTagsParam : null;
            this.filter();
        });
        this.readChangedSubscription = this.articlesService.readListChanged.subscribe(readArticles => {
            this.updateReadArticles(readArticles).then();
        });
    }

    public async ngOnInit(): Promise<void> {
        this.premiumListenerSubscription = this.premiumService.onChange.subscribe(() => {
            this.clearArticles().then(() => {
                this.getArticles(true).then();
            });
        });
    }

    public async getArticles(silent?: boolean) {
        if (!this.allArticles) {
            const config = silent ? {uiSilent: true} : {};
            this.allArticles = await this.articlesService.getAll(this.type, config);
            this.filter();
        }
        await this.updateReadArticles();
        await this.tryAutoOpen();
    }

    public async clearArticles(): Promise<void> {
        this.allArticles = null;
        this.filteredArticles = null;
    }

    public filter() {
        if (!this.allArticles) {
            return;
        }
        // console.log('filtering with category: ' + this.category);
        // console.log(this.allArticles.map(a => a.category));
        if (this.category && this.category !== 'None') {
            this.filteredArticles = this.allArticles.filter(a => a.category === this.category);
            // if (this.recipesTagsFilter && typeof this.recipesTagsFilter === 'string' && this.recipesTagsFilter.length) {
            //     const filterTags = this.recipesTagsFilter.split(',');
            //     this.filteredArticles = this.allArticles.filter(a => a.tags.some(t => filterTags.indexOf(t.id) >= 0));
        } else {
            this.filteredArticles = this.allArticles;
        }
    }

    public async tryAutoOpen(): Promise<void> {
        if (this.autoOpenId && this.allArticles) {
            const article = this.allArticles.find(a => a.slug === this.autoOpenId || a.id === this.autoOpenId);
            if (article) {
                setTimeout(async () => {
                    if (article.youNeedPremiumToAccessThis) {
                        await this.navController.navigateForward(['/premium']);
                    } else {
                        await this.navController.navigateForward('/articles/article/' + article.slug);
                    }
                }, 500);
            }
        }
    }

    public ngOnDestroy(): void {
        if (this.premiumListenerSubscription) {
            this.premiumListenerSubscription.unsubscribe();
            this.premiumListenerSubscription = null;
        }
        if (this.readChangedSubscription) {
            this.readChangedSubscription.unsubscribe();
            this.readChangedSubscription = null;
        }
    }

    private async updateReadArticles(readArticles?: string[]): Task {
        if (!this.allArticles || !this.allArticles.length) {
            return;
        }
        readArticles = readArticles || (await this.articlesService.getReadArticles() || []) as string[];
        for (const id of readArticles) {
            const article = this.allArticles.find(a => a.id === id || a.slug === id);
            if (article) {
                article.read = true;
            }
        }
    }
}
