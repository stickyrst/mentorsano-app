import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';

import { IonicModule } from '@ionic/angular';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ArticlesRoutingModule,
        IonicModule,
    ],
    providers: [],
    exports: [],
})
export class ArticlesModule {
}
