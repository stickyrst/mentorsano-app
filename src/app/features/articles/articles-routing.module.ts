import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/articles/articles.module').then(m => m.ArticlesPageModule),
    },
    {
        path: 'article/:id',
        loadChildren: () => import('./pages/article/article.module').then(m => m.ArticlePageModule),
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ArticlesRoutingModule {
}
