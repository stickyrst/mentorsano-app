import { TagModel } from '@feats/profile/models/tag.model';

export interface ArticleModel {
    id: string;
    slug: string;
    title: string;
    body: string;
    language: string;
    shortDescription: string;
    image?: { link?: string };
    externalUrl: string;
    type: string;
    tags?: TagModel[];
    isPremium?: boolean;
    youNeedPremiumToAccessThis?: boolean;
    read?: boolean;
    category?: string;
}
