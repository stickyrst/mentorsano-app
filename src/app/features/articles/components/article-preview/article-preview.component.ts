import { Component, Input, OnInit } from '@angular/core';
import { ArticleModel } from '../../models/article.model';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-article-preview',
    templateUrl: './article-preview.component.html',
    styleUrls: ['./article-preview.component.scss'],
})
export class ArticlePreviewComponent {
    @Input()
    public article: ArticleModel;

    constructor(
        private navController: NavController,
    ) {
    }

    public onClick() {
        if (!this.article.youNeedPremiumToAccessThis) {
            this.navController.navigateForward(['/articles', 'article', this.article.slug]).then();
        } else {
            this.navController.navigateForward(['/premium']).then();
        }
    }
}
