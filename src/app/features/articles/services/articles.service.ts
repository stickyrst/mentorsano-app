import { EventEmitter, Injectable, Injector } from '@angular/core';
import { LoadingAbleService, LoadingAbleServiceConfig } from '@core/services/base-services/loading-able.service';
import { ArticleModel } from '../models/article.model';
import { ConstantsService } from '@core/services/constants.service';
import { Storage } from '@ionic/storage';
import { ArticleType } from '@feats/articles/models/article.type';
import { Observable } from 'rxjs';
import { MeasureUnitsService } from '@core/services/measure-units.service';

@Injectable({providedIn: 'root'})
export class ArticlesService extends LoadingAbleService {

    constructor(
        injector: Injector,
        protected constants: ConstantsService,
        private storage: Storage,
        private measureUnitsService: MeasureUnitsService,
    ) {
        super(injector);
    }

    public get readListChanged(): Observable<string[]> {
        return this.readListChangedEE.asObservable();
    }

    private readListChangedEE: EventEmitter<string[]> = new EventEmitter<string[]>();

    public async getOne(id: string): Promise<ArticleModel> {
        let path = 'Articles/GetOne/' + id;
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            path = path + '?lang=' + lang;
        }
        const measureUnit = (await this.measureUnitsService.getMeasureUnitSystem()) || 'metric';
        if (measureUnit) {
            path += (lang ? '&' : '?') + 'unit=' + measureUnit;
        }
        const article = await this.get<ArticleModel>(path, true);
        this.fixImagePath([article]);
        return article;
    }

    public async getAll(type: ArticleType = 'article', config?: LoadingAbleServiceConfig): Promise<ArticleModel[]> {
        let path = 'Articles/GetAll?type=' + type;
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            path = path + '&lang=' + lang;
        }
        const articles = await this.get<ArticleModel[]>(path, true, config);
        if (!articles) {
            return [];
        }
        this.fixImagePath(articles);
        return articles;
    }

    private fixImagePath(articles: ArticleModel[]) {
        const url = this.constants.apiUrl.replace('/api/', '');
        for (const article of articles) {
            if (article.image && article.image.link) {
                if (article.image.link.indexOf('http') !== 0) {
                    article.image.link = url + article.image.link;
                }
            } else {
                article.image = {};
            }
        }
    }

    public async setIdAsRead(articleId: string) {
        let readArticles: string[] = await this.storage.get('read-articles');
        readArticles = readArticles || [];
        if (readArticles.indexOf(articleId) === -1) {
            readArticles.push(articleId);
            this.storage.set('read-articles', readArticles).then();
            this.readListChangedEE.emit(readArticles);
        }
    }

    public async getReadArticles(): Promise<string[]> {
        const readArticles: string[] = await this.storage.get('read-articles');
        return readArticles || [];
    }
}
