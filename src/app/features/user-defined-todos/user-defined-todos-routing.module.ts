import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserDefinedTodosListPage } from './user-defined-todos-list-page/user-defined-todos-list-page.component';

const routes: Routes = [
    {
        path: '',
        component: UserDefinedTodosListPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserDefinedTodosPageRoutingModule {
}
