import { Injectable, Injector } from '@angular/core';
import { LoadingAbleService } from '@core/services/base-services/loading-able.service';
import { UserDefinedTodoModel } from '@feats/user-defined-todos/models/user-defined-todo.model';
import { Observable, Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class UserDefinedTodosService extends LoadingAbleService {

    private forceReloadCheckInSubject: Subject<void> = new Subject<void>();

    constructor(injector: Injector) {
        super(injector);
    }

    public async addOrUpdate(todo: UserDefinedTodoModel): Promise<UserDefinedTodoModel> {
        const path = 'UserDefinedTodos/Patch';

        const bag = {
            id: todo.id,
            model: todo,
            propertiesToUpdate: {
                text: true,
                repeatType: true,
                date: true,
            },
        };
        const result = await this.patch<UserDefinedTodoModel, any>(path, bag);
        this.fixDates([result]);
        return result;
    }

    public async getAll(): Promise<UserDefinedTodoModel[]> {
        const path = 'UserDefinedTodos/GetAll';
        const todos = await this.get<UserDefinedTodoModel[]>(path, true);
        this.fixDates(todos);
        return todos;
    }

    public async remove(id: string): Promise<void> {
        const path = `UserDefinedTodos/Delete/${id}`;
        await this.delete(path);
    }

    public get forceReloadCheckIn(): Observable<void> {
        return this.forceReloadCheckInSubject.asObservable();
    }

    public triggerForceReloadCheckIn(): void {
        this.forceReloadCheckInSubject.next();
    }

    private fixDates(todos: UserDefinedTodoModel[]) {
        for (const todo of todos) {
            if (typeof todo.date === 'string') {
                todo.date = new Date(todo.date);
            }
            if (typeof todo.day === 'string') {
                todo.day = new Date(todo.day);
            }
            if (typeof todo.time === 'string') {
                todo.time = new Date(todo.time);
            }
        }
    }
}
