import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslationsService } from '@core/services/translations.service';
import { UserDefinedTodoModel } from '@feats/user-defined-todos/models/user-defined-todo.model';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';
import { UserDefinedTodosService } from '@feats/user-defined-todos/services/user-defined-todos.service';

@Component({
    selector: 'app-user-defined-todo-form-modal',
    templateUrl: './user-defined-todo-form.component.html',
    styleUrls: ['./user-defined-todo-form.component.scss'],
})
export class UserDefinedTodoFormComponent implements OnInit {

    public form: FormGroup = new FormGroup({});

    constructor(
        public modalController: ModalController,
        public trans: TranslationsService,
        private blockHwdBackBtn: BlockHwdBackButtonService,
        private userDefinedTodosService: UserDefinedTodosService,
    ) {
    }

    @Input()
    public action: string;
    @Input()
    public model: UserDefinedTodoModel;


    public ngOnInit() {
        if (this.action === 'create') {
            this.model = {};
        }

        this.formConfig[0].templateOptions.label = this.trans.trans('user-defined-todo_message') || 'Text';
        this.formConfig[1].templateOptions.label = this.trans.trans('user-defined-todo_repeat-type') || 'Repeat Type';
        this.formConfig[1].templateOptions.options[0].label = this.trans.trans('once') || 'Once';
        this.formConfig[1].templateOptions.options[1].label = this.trans.trans('daily') || 'Daily';
        this.formConfig[2].templateOptions.label = this.trans.trans('date') || 'Date';
        this.formConfig[3].templateOptions.label = this.trans.trans('hour') || 'Hour';

        this.blockHwdBackBtn.push({
            block: true, ref: this,
            oneTimeBlockCallback: async () => {
                await this.modalController.dismiss();
            },
        });
    }

    public async close() {
        await this.modalController.dismiss();
    }

    public async submit() {
        const result = await this.userDefinedTodosService.addOrUpdate(this.model);
        if (!result) {
            return;
        }
        await this.modalController.dismiss({reload: true, model: result});
    }

    // tslint:disable-next-line:member-ordering
    public formConfig: FormlyFieldConfig[] = [{
        key: 'text',
        type: 'textarea',
        validators: {validation: []},
        validation: {messages: {}},
        templateOptions: {type: 'multiline', label: 'Text', required: true, labelPosition: 'floating', hideRequiredMarker: true},
    }, {
        key: 'repeatType',
        type: 'select',
        validators: {validation: []},
        validation: {messages: {}},
        templateOptions: {
            type: 'select',
            label: 'Repeat Type',
            options: [
                {value: 'Once', label: 'O singură dată', description: null},
                {value: 'Daily', label: 'Zilnic', description: null},
            ],
            required: true,
            hideRequiredMarker: true,
        },
        defaultValue: 'Once',
    }, {
        key: 'day',
        type: 'input',
        validators: {validation: []},
        validation: {messages: {}},
        templateOptions: {type: 'date', label: 'Ziua', required: true, labelPosition: 'floating', hideRequiredMarker: true},
        hideExpression: 'model.repeatType !== \'Once\'',
        className: 'col-6 d-inline-block-nf',
    }, {
        key: 'time',
        type: 'input',
        validators: {validation: []},
        validation: {messages: {}},
        templateOptions: {type: 'time', label: 'Time', required: true, labelPosition: 'floating', hideRequiredMarker: true},
        className: 'col-6 d-inline-block-nf',
    }];
}
