import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { UserDefinedTodosPageRoutingModule } from './user-defined-todos-routing.module';
import { UserDefinedTodosListPage } from './user-defined-todos-list-page/user-defined-todos-list-page.component';
import { CoreModule } from '@core/core.module';
import { UserDefinedTodoFormComponent } from '@feats/user-defined-todos/user-defined-todo-form-modal/user-defined-todo-form.component';
import { FormlyHelperModule } from '@feats/formly-helper/formly-helper.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        UserDefinedTodosPageRoutingModule,
        CoreModule,
        FormlyHelperModule,
    ],
    declarations: [
        UserDefinedTodosListPage,
        UserDefinedTodoFormComponent,
    ],
})
export class UserDefinedTodosPageModule {
}
