import { Component, OnInit } from '@angular/core';
import { UserDefinedTodoModel } from '@feats/user-defined-todos/models/user-defined-todo.model';
import { TranslationsService } from '@core/services/translations.service';
import { UserDefinedTodosService } from '@feats/user-defined-todos/services/user-defined-todos.service';
import { ModalController } from '@ionic/angular';
import { UserDefinedTodoFormComponent } from '@feats/user-defined-todos/user-defined-todo-form-modal/user-defined-todo-form.component';
import { LocalNotificationsHelperService } from '@core/services/local-notifications-helper.service';

@Component({
    selector: 'app-user-defined-todos',
    templateUrl: './user-defined-todos-list-page.component.html',
    styleUrls: ['./user-defined-todos-list-page.component.scss'],
})
export class UserDefinedTodosListPage implements OnInit {

    public shouldReloadCheckIn: boolean;
    public userDefinedTodos: UserDefinedTodoModel[];

    constructor(
        public trans: TranslationsService,
        private userDefinedTodosService: UserDefinedTodosService,
        private modalController: ModalController,
        private localNotifsHelper: LocalNotificationsHelperService,
    ) {
    }

    public ionViewDidLeave() {
        if (this.shouldReloadCheckIn) {
            this.userDefinedTodosService.triggerForceReloadCheckIn();
        }
    }

    public async ngOnInit() {
        await this.load();
    }

    private async load() {
        this.userDefinedTodos = await this.userDefinedTodosService.getAll();
    }

    public async add() {
        const modal = await this.modalController.create({
            component: UserDefinedTodoFormComponent,
            componentProps: {
                action: 'create',
            },
            swipeToClose: true,
        });
        await modal.present();
        const result = await modal.onDidDismiss();
        if (result.data && result.data.model) {
            this.userDefinedTodos.push(result.data.model);
        }
        if (result.data && result.data.reload) {
            await this.listChanged();
        }
    }

    public async edit(index: number) {
        const todo = this.userDefinedTodos[index];
        const clone: UserDefinedTodoModel = JSON.parse(JSON.stringify(todo));
        clone.time = this.formatTime(todo.time as Date);
        clone.day = this.formatDate(todo.day as Date);
        delete clone.date;
        const modal = await this.modalController.create({
            component: UserDefinedTodoFormComponent,
            componentProps: {
                model: clone,
                action: 'edit',
            },
            swipeToClose: true,
        });
        await modal.present();
        const result = await modal.onDidDismiss();
        if (result.data && result.data.model) {
            this.userDefinedTodos.splice(index, 1, result.data.model);
        }
        if (result.data && result.data.reload) {
            await this.listChanged();
        }
    }

    private async listChanged() {
        await this.load();
        this.localNotifsHelper.setForMeals();
        this.shouldReloadCheckIn = true;
    }

    public async delete(index: number) {
        await this.userDefinedTodosService.remove(this.userDefinedTodos[index].id);
        this.userDefinedTodos.splice(index, 1);
        this.shouldReloadCheckIn = true;
    }

    private formatDate(dt: Date) {
        let day: string | number = dt.getDate();
        if (day < 10) {
            day = '0' + day;
        }
        let month: string | number = dt.getMonth() + 1;
        if (month < 10) {
            month = '0' + month;
        }
        return `${dt.getFullYear()}-${month}-${day}`;
    }

    private formatTime(dt: Date) {
        let hour: string | number = dt.getHours();
        if (hour < 10) {
            hour = '0' + hour;
        }
        let minute: string | number = dt.getMinutes();
        if (minute < 10) {
            minute = '0' + minute;
        }
        return `${hour}:${minute}`;
    }
}
