export interface UserDefinedTodoModel {
    id?: string;
    text?: string;
    repeatType?: string;
    day?: string | Date;
    time?: string | Date;
    date?: string | Date;
}
