import { Component, NgZone, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslationsService } from '@core/services/translations.service';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';

@Component({
    selector: 'app-landing-page',
    templateUrl: './landing-page.page.html',
    styleUrls: ['./landing-page.page.scss'],
})
export class LandingPagePage implements OnInit {

    constructor(
        private menuController: MenuController,
        private navController: NavController,
        private storage: Storage,
        public trans: TranslationsService,
        private zone: NgZone,
        private blockHwdBackBtn: BlockHwdBackButtonService,
    ) {
    }

    public async ngOnInit() {
        await this.menuController.enable(false);
    }

    public async ok() {
        await this.storage.set('landingPageSkipped', true);
        await this.zone.run(async () => {
            await this.menuController.enable(true);
            await this.navController.navigateRoot('/login');
        });
    }

    public ionViewWillEnter() {
        this.blockHwdBackBtn.push(true);
    }

    public ionViewWillLeave() {
        this.blockHwdBackBtn.pop();
    }
}
