import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./pages/custom-form/custom-form.module').then(m => m.CustomFormPageModule),
    },
    {
        path: ':formId',
        loadChildren: () => import('./pages/custom-form/custom-form.module').then(m => m.CustomFormPageModule),
    },
    {
        path: '**',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CustomFormsRoutingModule {
}
