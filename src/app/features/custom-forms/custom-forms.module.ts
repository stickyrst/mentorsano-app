import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomFormsRoutingModule } from './custom-forms-routing.module';
import { FormHelperService } from './services/form-helper.service';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        CustomFormsRoutingModule,
    ],
    providers: [
        FormHelperService,
    ],
})
export class CustomFormsModule {
}
