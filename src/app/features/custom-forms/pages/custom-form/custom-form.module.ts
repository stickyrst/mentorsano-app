import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomFormPageRoutingModule } from './custom-form-routing.module';

import { CustomFormPage } from './custom-form.page';
import { FormlyModule } from '@ngx-formly/core';
import { CoreModule } from '@core/core.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        CustomFormPageRoutingModule,
        ReactiveFormsModule,
        FormlyModule,
        CoreModule,
    ],
    declarations: [CustomFormPage],
})
export class CustomFormPageModule {
}
