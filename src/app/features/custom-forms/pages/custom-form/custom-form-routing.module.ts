import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CustomFormPage } from './custom-form.page';

const routes: Routes = [
    {
        path: '',
        component: CustomFormPage,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CustomFormPageRoutingModule {
}
