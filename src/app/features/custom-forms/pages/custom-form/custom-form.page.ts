import { Component, OnInit } from '@angular/core';
import { FormHelperService } from '../../services/form-helper.service';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { CustomFormModel } from '@core/models/custom-form.model';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DisplayMessageService } from '@core/services/display-message.service';
import { TranslationsService } from '@core/services/translations.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { MeasureUnitsService } from '@core/services/measure-units.service';
import { MenuController, NavController } from '@ionic/angular';
import { LocalNotificationsHelperService } from '@core/services/local-notifications-helper.service';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';
import { UserActivityService } from '@feats/user-activity/services/user-activity.service';

@Component({
    selector: 'app-custom-form',
    templateUrl: './custom-form.page.html',
    styleUrls: ['./custom-form.page.scss'],
})
export class CustomFormPage implements OnInit {

    constructor(private formHelperService: FormHelperService,
                private loading: CustomLoaderService,
                private route: ActivatedRoute,
                private messages: DisplayMessageService,
                private router: Router,
                private navCtrl: NavController,
                public trans: TranslationsService,
                private measureUnitsService: MeasureUnitsService,
                private localNotifs: LocalNotificationsHelperService,
                private menuController: MenuController,
                private blockHwdBackBtn: BlockHwdBackButtonService,
                private userActivityService: UserActivityService,
    ) {
        this.formId = route.snapshot.params.formId || 'general-information';
        this.isGeneralInformationForm = this.formId === 'general-information';
        this.form.valueChanges.subscribe(async event => {
            await this.formUpdated(event);
        });
    }

    private readonly formId: string;

    public fields: FormlyFieldConfig[];
    public customForm: CustomFormModel;
    public form = new FormGroup({});

    public model = {};

    public progressValue = 0;

    public hideBackButton: boolean;

    public isFirstResponse: boolean;
    public firstTimeFormChanged: boolean;
    private readonly isGeneralInformationForm: boolean;

    public async ngOnInit(): Promise<void> {

        await this.loading.present();

        try {
            this.customForm = await this.formHelperService.getForm(this.formId);

        } catch (e) {
            await this.navCtrl.back();
            await this.messages.showError(e);
            await this.loading.dismiss();
            return;
        }
        try {
            const data = await this.formHelperService.getResponseFor(this.formId);
            Object.assign(this.model, data);
        } catch (e) {
            if (this.isGeneralInformationForm) {
                this.hideBackButton = true;
                this.blockHwdBackBtn.push({block: true, ref: this});
                await this.menuController.enable(false);
                this.isFirstResponse = true;
            }
        }
        await this.measureUnitsService.patchForDisplay(this.customForm.questions, this.model, {
            showModalIfNotSet: true,
            addMeasureUnitWrapper: true,
            skipLabelEdit: true,
        });
        this.fields = this.customForm.questions;
        this.addAnsweredQuestionWrapper();
        await this.loading.dismiss();
    }

    public async submit(): Promise<void> {
        // console.log(this.model);
        if (!this.form.valid) {
            await this.messages.display('Check the fields before submitting.');
            this.form.markAllAsTouched();
            return;
        }
        await this.loading.present();

        const formResp = await this.measureUnitsService.patchForSubmit(this.fields, this.model);

        for (const pKey in formResp) {
            if (formResp.hasOwnProperty(pKey) && !this.fields.find(q => q.key === pKey)) {
                delete formResp[pKey];
            }
        }
        try {
            const resp = await this.formHelperService.saveResponseFor(this.formId, formResp);
            this.userActivityService.clearCache();
            await this.localNotifs.handleResponse(this.customForm, formResp);
            await this.loading.dismiss();
            await this.messages.display('Saved!');
            if (this.isGeneralInformationForm && this.isFirstResponse) {
                await this.navCtrl.navigateForward(['/simple-text'], {queryParams: {id: 'follow-program'}});
            } else {
                await this.navCtrl.navigateRoot(['/']);
            }
        } catch (e) {
            await this.loading.dismiss();
            await this.messages.showError(e);
        }
    }

    public async formUpdated($event) {
        let cnt = 0;
        let total = 0;
        for (const field of this.fields) {
            if (!field.templateOptions.required) {
                continue;
            }

            total++;
            if (typeof this.model[field.key as string] !== 'undefined') {
                cnt++;
            }
        }
        setTimeout(() => {
            this.progressValue = cnt / total;
        });
        if (!this.firstTimeFormChanged) {
            this.firstTimeFormChanged = true;
        } else {
            this.hideBackButton = true;
            if (!this.blockHwdBackBtn.isRefPushed(this)) {
                this.blockHwdBackBtn.push({block: true, ref: this});
            }
        }
    }

    public async ionViewWillLeave() {
        await this.menuController.enable(true);
        this.blockHwdBackBtn.pop(this);
    }

    private addAnsweredQuestionWrapper() {
        for (const question of this.fields) {
            question.wrappers = question.wrappers || [];
            if (question.wrappers.indexOf('measure-unit') === -1 && question.type !== 'custom-select') {
                question.wrappers.push('form-field');
            }
            question.wrappers = [
                'done-question-wrapper',
                ...question.wrappers,
            ];
        }
    }
}
