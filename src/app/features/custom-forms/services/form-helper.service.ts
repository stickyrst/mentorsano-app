import { Injectable, Injector } from '@angular/core';
import { ConstantsService } from '@core/services/constants.service';
import { CustomFormModel } from '@core/models/custom-form.model';
import { BaseHttpedService } from '@auth/services/base-httped.service';
import { Storage } from '@ionic/storage';

@Injectable()
export class FormHelperService extends BaseHttpedService {

    constructor(private constants: ConstantsService,
                private storage: Storage,
                injector: Injector) {
        super(injector);
    }

    private forms: { [key: string]: CustomFormModel } = {};

    public async getForm(formId: string): Promise<CustomFormModel> {
        if (this.forms.hasOwnProperty(formId)) {
            return this.forms[formId];
        }
        let url = this.constants.apiUrl + 'CustomForms/GetOne/' + formId;
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            url = url + '?lang=' + lang;
        }
        const form = await this.aHttp.get<CustomFormModel>(url);
        this.patchIonicLabels(form);
        return form;
    }

    private patchIonicLabels(form: CustomFormModel): void {
        for (const question of form.questions) {
            if (question.type !== 'toggle' && question.type !== 'select' && question.type !== 'radio') {
                question.templateOptions.labelPosition = 'floating';
            }
            if (question.type === 'toggle') {
                question.className = 'custom-toggle';
            }
        }
    }

    public async getResponseFor(formId: string): Promise<any> {
        const url = this.constants.apiUrl + 'CustomFormResponses/GetFor/' + formId;
        return await this.aHttp.get<any>(url);
    }

    public async saveResponseFor(formId: string, response: any): Promise<any> {
        const url = this.constants.apiUrl + 'CustomFormResponses/SaveFor/' + formId;
        return await this.aHttp.post<any>(url, {data: response});
    }

}
