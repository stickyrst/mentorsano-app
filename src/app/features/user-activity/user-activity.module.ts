import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserActivityRoutingModule } from '@feats/user-activity/user-activity-routing.module';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        UserActivityRoutingModule,
    ],
})
export class UserActivityModule {
}
