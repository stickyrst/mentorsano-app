import { Injectable, Injector } from '@angular/core';
import { LoadingAbleService, LoadingAbleServiceConfig } from '@core/services/base-services/loading-able.service';
import { Storage } from '@ionic/storage';
import { UserActivityModel } from '@feats/user-activity/models/user-activity.model';
import { HomePageActivityStatusModel } from '../models/home-page-activity-status.model';
import { DateService } from '@core/services/date.service';
import { ConstantsService } from '@core/services/constants.service';

@Injectable({providedIn: 'root'})
export class UserActivityService extends LoadingAbleService {

    private cache: { [key: string]: any } = {};

    public colorIntervals: { color: string, minPercent?: number }[];

    constructor(
        private storage: Storage,
        injector: Injector,
        private dateService: DateService,
    ) {
        super(injector);
        this.popColorIntervals();
    }

    public async getFullActivity(from?: Date, to?: Date): Promise<UserActivityModel> {
        const path = 'Activity/GetOwn';
        const params: { [key: string]: string } = {};
        if (typeof from !== 'undefined') {
            params.from = from.toISOString();
        }
        if (typeof to !== 'undefined') {
            params.to = to.toISOString();
        }
        params.lang = await this.storage.get(ConstantsService.languageStorageKey) || 'en';
        params.today = this.dateService.currentDay.toISOString();

        const resp = await this.get<UserActivityModel>(path, true, {}, {params});
        if (resp.days && resp.days.length) {
            for (const day of resp.days) {
                if (typeof day.day === 'string') {
                    day.day = new Date(day.day);
                }
            }
        }
        resp.profileActivityIndex = this.getActivityCategoryIndex(resp.monthPercent);
        resp.profileActivityTitle = 'activity-title-' + resp.profileActivityIndex;
        return resp;
    }

    public async getForProfilePage(): Promise<UserActivityModel> {
        const path = 'Activity/GetForProfilePage';
        const params: { [key: string]: string } = {};
        params.lang = await this.storage.get(ConstantsService.languageStorageKey) || 'en';

        const resp = await this.get<UserActivityModel>(path, true, {}, {params});
        resp.profileActivityIndex = this.getActivityCategoryIndex(resp.monthPercent);
        resp.profileActivityTitle = 'activity-title-' + resp.profileActivityIndex;
        if (resp.created && typeof resp.created === 'string') {
            resp.created = new Date(resp.created);
        }

        return resp;
    }

    public getColorForPercent(percent?: number): string {
        return this.colorIntervals[this.getActivityCategoryIndex(percent)].color;
    }

    public getActivityCategoryIndex(percent?: number): number {
        if (percent === undefined) {
            return 0;
        }
        for (let i = this.colorIntervals.length - 1; i >= 0; i--) {
            if (percent >= this.colorIntervals[i].minPercent) {
                return i;
            }
        }
        return 0;
    }

    public async getHomePageActivity(config?: LoadingAbleServiceConfig): Promise<HomePageActivityStatusModel> {
        let lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (!lang) {
            lang = 'en';
        }
        const cacheKey = 'home-activity-' + lang;
        if (this.cache[cacheKey]) {
            return this.cache[cacheKey];
        }
        const path = 'Activity/GetHomePageActivity?today=' + this.dateService.currentDay.toISOString() + '&lang=' + lang;
        const result = await this.get<HomePageActivityStatusModel>(path, true, config);
        if (result && result.todayPercent) {
            result.todayActivityCategoryIndex = this.getActivityCategoryIndex(result.todayPercent);
        }

        this.cache['home-activity-' + lang] = result;
        return result;
    }

    public clearCache() {
        this.cache = {};
    }

    private popColorIntervals(): void {
        this.colorIntervals = [
            {
                color: 'grey',
            },
            {
                minPercent: 0,
                color: '#ff7c6e',
            },
            {
                minPercent: 20,
                color: '#ffb217',
            },
            {
                minPercent: 50,
                color: '#fff88b',
            },
            {
                minPercent: 70,
                color: '#9aba43',
            },
            {
                minPercent: 90,
                color: '#5dc74d',
            },
        ];
    }
}
