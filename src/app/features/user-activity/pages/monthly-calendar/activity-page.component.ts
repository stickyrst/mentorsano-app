import { Component, OnInit, ViewChild } from '@angular/core';
import { UserActivityService } from '@feats/user-activity/services/user-activity.service';
import { UserActivityModel } from '@feats/user-activity/models/user-activity.model';
import { CalendarComponent } from 'ionic2-calendar';
import { TranslationsService } from '@core/services/translations.service';
import { DateService } from '@core/services/date.service';

@Component({
    selector: 'app-monthly-calendar',
    templateUrl: './activity-page.component.html',
    styleUrls: ['./activity-page.component.scss'],
})
export class ActivityPage implements OnInit {

    @ViewChild(CalendarComponent)
    public cal: CalendarComponent;

    public pageReady: boolean;
    public model: UserActivityModel;

    public calendar = {
        mode: 'month',
        currentDate: new Date(),
        startingDayMonth: 1,
        locale: 'en-GB',
        allDayLabel: 'Activity',
        dateFormatter: {
            formatMonthViewDay(date: Date) {
                return date.getDate().toString();
            },
            formatMonthViewDayHeader: (date: Date) => {
                return this.dayShortNames[date.getDay()];
            },
            formatMonthViewTitle: (date: Date) => {
                return this.monthNames[date.getMonth()] + ' ' + date.getFullYear();
            },
        },
        eventSource: [],
        lockSwipeToPrev: false,
    };

    private dayShortNames: string[] = [];
    private monthNames: string[] = [];

    public viewTitle: string;

    constructor(
        private userActivityService: UserActivityService,
        public trans: TranslationsService,
        private dateService: DateService,
    ) {
        this.userActivityService.setDefaultConfig();
    }

    public async ngOnInit() {

        this.dayShortNames = (await this.trans.transA('calendar_days_short')).split(',').map(n => n.trim());
        this.monthNames = (await this.trans.transA('month_names')).split(',').map(n => n.trim());

        const now = this.dateService.currentDate;
        const start = new Date(now);
        start.setDate(start.getDate() - 31);

        const resp = await this.userActivityService.getFullActivity(start, now);
        this.model = resp;
        this.calendar.eventSource = [...resp.days.map(e => {
            return {
                startTime: new Date(Date.UTC(e.day.getFullYear(), e.day.getMonth(), e.day.getDate())),
                endTime: new Date(Date.UTC(e.day.getFullYear(), e.day.getMonth(), e.day.getDate() + 1)),
                allDay: true,
                title: e.percent,
                backColor: this.userActivityService.getColorForPercent(e.percent),
                activityCategoryIndex: this.userActivityService.getActivityCategoryIndex(e.percent),
            };
        })];
        this.pageReady = true;
    }

    public onViewTitleChanged(title: string) {
        this.viewTitle = title;
    }

    public onCurrentDateChanged(selected: Date): void {
        const now = this.dateService.currentDate;
        this.calendar.lockSwipeToPrev = selected.getMonth() < now.getMonth();
        if (selected.getMonth() > now.getMonth()) {
            this.calendar.currentDate = now;
        }
    }
}
