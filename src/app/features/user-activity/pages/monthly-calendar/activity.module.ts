import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ActivityPageRoutingModule } from './activity-routing.module';
import { ActivityPage } from './activity-page.component';
import { NgCalendarModule } from 'ionic2-calendar';
import { CoreModule } from '@core/core.module';
import { SharedModule } from '../../../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ActivityPageRoutingModule,
        NgCalendarModule,
        CoreModule,
        SharedModule,
    ],
    declarations: [ActivityPage],
})
export class ActivityPageModule {
}
