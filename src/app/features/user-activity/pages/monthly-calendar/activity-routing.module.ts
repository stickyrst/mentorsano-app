import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivityPage } from './activity-page.component';

const routes: Routes = [
    {
        path: '',
    //     redirectTo: 'monthly-calendar',
    // },
    // {
    //     path: 'monthly-calendar',
        component: ActivityPage,
    // },
    // {
    //     path: '**',
    //     redirectTo: 'monthly-calendar',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class ActivityPageRoutingModule {
}
