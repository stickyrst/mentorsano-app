import { HomePageActivityStatusModel } from '@feats/user-activity/models/home-page-activity-status.model';

export interface UserActivityModel extends HomePageActivityStatusModel {
    days: ActivityCalendarEntryModel[];
    objective: string;
    profileActivityTitle?: string;
    profileActivityIndex?: number;
    avatarLink: string;
    created: Date;
    bmi: number;
    bmiCategoryName: string;
    recommendedWaterAmount: number;
    height: number;
    weight: number;
    minimumIdealWeight: number;
    maximumIdealWeight: number;
}

export interface ActivityCalendarEntryModel {
    day: Date;
    percent: number;
}
