import { CheckQuestionFormlyFieldConfig } from '@feats/check-forms/models/check-question-formly-field-config';

export interface HomePageActivityStatusModel {
    profileName: string;
    profileFullName?: string;
    todayPercent: number;
    todayTitle: string;
    todayActivityCategoryIndex?: number;
    monthPercent: number;
    firstAvailableCheckQuestion?: CheckQuestionFormlyFieldConfig;
}
