import { Component, OnInit } from '@angular/core';
import { ConstantsService } from '@core/services/constants.service';
import { GlobalsService } from '@core/services/globals.service';
import { TranslationsService } from '@core/services/translations.service';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { ILocalNotification, ILocalNotificationTrigger, LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications';
import { AppUpdateResponse, AppUpdateService } from '@core/services/app-update.service';

@Component({
    selector: 'app-developer',
    templateUrl: './developer.page.html',
    styleUrls: ['./developer.page.scss'],
})
export class DeveloperPage implements OnInit {
    public windowUrl: string;
    public version: string;
    public updateResponse: AppUpdateResponse;

    constructor(
        public constants: ConstantsService,
        public globals: GlobalsService,
        public translationsService: TranslationsService,
        private navController: NavController,
        private storage: Storage,
        public appVersion: AppVersion,
        private localNotifications: LocalNotifications,
        public appUpdateService: AppUpdateService,
    ) {
        this.windowUrl = window.location.href;
        this.appVersion.getVersionNumber().then(version => {
            this.version = version;
            this.appUpdateService.get().then(resp => {
                this.updateResponse = resp;
            });
        }).catch(err => {
            this.version = err.message || err;
        });
    }

    public ngOnInit() {
        this.windowUrl = window.location.href;
    }

    public async clearStorage() {
        await this.storage.clear();
        await this.storage.set(ConstantsService.developerKey, true);
        window.location.href = '';
    }

    public reload() {
        window.location.href = '/';
    }

    public async testNotif() {
        const time = new Date();
        // time.setMinutes(time.getMinutes() + 1);
        time.setSeconds(time.getSeconds() + 5);

        const trigger1: ILocalNotificationTrigger = {
            at: time,
            // every: ELocalNotificationTriggerUnit.MINUTE,
        };
        const trigger2: ILocalNotificationTrigger | any = {
            every: {
                hour: time.getHours(),
                minute: time.getMinutes(),
                second: 0,
            },
        };
        const trigger3: any = {
            // at: time.getTime(),
            every: ELocalNotificationTriggerUnit.MINUTE,
        };
        const trigger4: any = {
            at: new Date(1589485140000),
        };

        const opt: ILocalNotification = {
            id: 100,
            title: 'notif title ' + time.getTime(),
            text: 'notif text ' + time.getTime(),
            trigger: trigger1,
            smallIcon: 'res://notif',
            icon: 'res://ic_launcher',
            color: '#169948',
        };
        console.log(JSON.stringify(opt));
        // (cordova.plugins as any).notification.local.schedule(opt);
        await this.localNotifications.schedule(opt);
        console.log(opt);
        console.log(opt.trigger);
    }

    public async reloadTranslations() {
        await this.translationsService.forceLoad(true);
        await this.navController.navigateRoot('/');
    }
}
