import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyPremiumPage } from './pages/buy-premium/buy-premium.page';
import { PremiumPage } from '@feats/premium/pages/premium/premium.page';

const routes: Routes = [
    {
        path: '',
        component: PremiumPage,
    },
    {
        path: 'buy',
        component: BuyPremiumPage,
    },
    {
        path: '**',
        redirectTo: '',
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class PremiumPageRoutingModule {
}
