export enum PlatformType {
    ANDROID = 'android-playstore',
    IOS = 'ios-appstore',
}
