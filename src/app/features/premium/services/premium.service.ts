import { EventEmitter, Injectable, Injector } from '@angular/core';

import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { PlatformType } from '@feats/premium/models/platform-type';
import { BaseHttpedService } from '@auth/services/base-httped.service';
import { ConstantsService } from '@core/services/constants.service';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class PremiumService extends BaseHttpedService {
    public static readonly productIDs: string[] = ['premium_2'];

    private onChangeEmitter: EventEmitter<void> = new EventEmitter<void>();
    public onChange: Observable<void> = this.onChangeEmitter.asObservable();

    constructor(
        injector: Injector,
        private inAppPurchase: InAppPurchase2,
        private constants: ConstantsService,
    ) {
        super(injector);
    }

    public initialize() {
        // console.log('adding products ...');
        PremiumService.productIDs.forEach(productId => {
            this.inAppPurchase.register({
                id: productId,
                type: this.inAppPurchase.NON_CONSUMABLE,
                alias: productId,
            });
        });
        this.refresh();
    }

    public refresh() {
        // console.log('refreshing products ...');
        this.inAppPurchase.refresh();
    }

    public async validate(platform: PlatformType, receipt: string): Promise<{ valid: boolean, message: string }> {
        const url = this.constants.apiUrl + 'PurchasesCheck/Validate';
        // console.log('validating ... ' + receipt + '   on ' + platform);
        const response = await this.aHttp.post<{ valid: boolean, message: string }>(url, {store: platform, receipt});
        // console.log('got validation response, ', response);
        if (response.valid) {
            this.onChangeEmitter.emit();
        }
        return response;
    }

    public async alreadyPremiumProfile(): Promise<boolean> {
        const url = this.constants.apiUrl + 'PurchasesCheck/AlreadyPremiumProfile';
        return this.aHttp.get<boolean>(url);
    }

}
