import { Component, NgZone, OnInit } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { IAPProduct, InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { Task } from '@core/models/task';
import { PremiumService } from '@feats/premium/services/premium.service';
import { PlatformType } from '@feats/premium/models/platform-type';
import { NavController, Platform } from '@ionic/angular';
import { ConstantsService } from '@core/services/constants.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { AdmobService } from '@core/services/admob.service';

@Component({
    selector: 'app-premium',
    templateUrl: './buy-premium.page.html',
    styleUrls: ['./buy-premium.page.scss'],
})
export class BuyPremiumPage implements OnInit {

    public gotProduct: IAPProduct;
    public validProductDisplayFields: string[] = ['title', 'description', 'price'];
    public alreadyPremiumProfile: boolean;
    public forceRefreshDisabled: boolean;
    public justBeganPurchase: boolean;
    private callbacks: { [key: string]: (product) => void } = {};

    constructor(
        public trans: TranslationsService,
        public inAppPurchase: InAppPurchase2,
        private loader: CustomLoaderService,
        private platform: Platform,
        private premiumService: PremiumService,
        private constants: ConstantsService,
        private messages: DisplayMessageService,
        private navController: NavController,
        private zone: NgZone,
        private admobService: AdmobService,
    ) {
    }

    public ngOnInit() {
    }

    public async buy(): Task {
        // console.log('begin order');
        this.justBeganPurchase = true;
        this.inAppPurchase.order(this.gotProduct.id);
    }

    public async ionViewDidEnter() {
        if (!this.constants.hasCordova) {
            await this.messages.display('Not working right now.', true, true);
            await this.navController.pop();
            return;
        }
        await this.loader.present();
        this.alreadyPremiumProfile = await this.premiumService.alreadyPremiumProfile();
        if (!this.alreadyPremiumProfile) {

            this.inAppPurchase.ready(() => {
                // console.log('inAppPurchase ready');
                this.initProductListeners();

                for (const productId of PremiumService.productIDs) {
                    this.refreshProduct(productId);
                }
                this.loader.dismiss().then();
            });
        } else {
            this.loader.dismiss().then();
        }
    }

    public ionViewDidLeave() {
        if (this.constants.hasCordova) {
            for (const callbacksKey in this.callbacks) {
                if (!this.callbacks.hasOwnProperty(callbacksKey)) {
                    continue;
                }
                if (typeof this.callbacks[callbacksKey] === 'function') {
                    this.inAppPurchase.off(this.callbacks[callbacksKey]);
                }
            }
            this.inAppPurchase.validator = null;
        }
    }

    public forceRefresh() {
        if (this.forceRefreshDisabled) {
            return;
        }
        // console.log('force-refresh');
        this.forceRefreshDisabled = true;
        if (this.gotProduct && this.gotProduct.state === 'approved') {
            this.gotProduct.verify();
        } else {
            this.inAppPurchase.refresh();
        }
        setTimeout(() => {
            this.forceRefreshDisabled = false;
        }, 5000);
    }

    public goBack() {
        this.navController.back();
    }

    private initProductListeners() {
        this.inAppPurchase.validator = async (product: IAPProduct, callback) => {
            // console.log('inAppPurchase.validator', product);
            // Verify the transaction data
            const transactionType: PlatformType = this.platform.is('ios') ? PlatformType.IOS : PlatformType.ANDROID;
            const transactionReceipt: string = this.platform.is('ios') ? product.transaction.appStoreReceipt : product.transaction.receipt;
            await this.loader.present();
            try {
                const response = await this.premiumService.validate(transactionType, transactionReceipt);
                if (response.valid) {
                    callback(true, product.transaction);
                    this.showSuccessPurchaseMessage();
                } else {
                    const message = this.trans.trans(response.message);
                    // console.log('showPurchaseErrorMessage');
                    this.messages.showErrorMessage(message, true, true).then(() => {

                        if (response.message === 'orderid-already-confirmed') {
                            callback(true, product.transaction);
                        } else {
                            callback(false, {
                                code: this.inAppPurchase.PURCHASE_CONSUMED, // **Validation error code
                                error: {
                                    message: response.message,
                                },
                            });
                        }
                    });
                }
            } catch (e) {
                // console.log('validator request errored');
                // console.error(e);
                this.showPurchaseErrorMessage('unknown_error_occurred');
                callback(false, {
                    code: this.inAppPurchase.PURCHASE_CONSUMED,
                    error: {
                        message: 'should try again validation',
                    },
                });

            }
            await this.loader.dismiss();

        };

        for (const productId of PremiumService.productIDs) {
            this.inAppPurchase.when(productId).approved(this.callbacks.approved = (product: IAPProduct) => {
                // console.log('product approved', product);
                if (this.justBeganPurchase) {
                    // console.log(' verifying...');
                    this.justBeganPurchase = false;
                    product.verify();
                }
                // product.verify();
            });

            this.inAppPurchase.when(productId).cancelled(this.callbacks.cancelled = (product: IAPProduct) => {
                // console.log('product cancelled', product);
                this.justBeganPurchase = false;
                this.messages.showErrorMessage(this.trans.trans('premium-purchase-cancelled')).then();
            });

            this.inAppPurchase.when(productId).error(this.callbacks.error = (product: IAPProduct) => {
                // console.log('product error', product);
            });

            this.inAppPurchase.when(productId).updated(this.callbacks.updated = (product: IAPProduct) => {
                // console.log('product updated', product);
                this.zone.run(() => {
                    this.gotProduct = product;
                });
            });
            this.inAppPurchase.when(productId).loaded(this.callbacks.loaded = (product: IAPProduct) => {
                // console.log('product loaded', product);
                this.zone.run(() => {
                    this.refreshProduct(product.id);
                });
            });
            this.inAppPurchase.when(productId).verified(this.callbacks.verified = (product: IAPProduct) => {
                // console.log('product verified', product);
                product.finish();
                // console.log('called finish!!');
            });

            this.inAppPurchase.when(productId).unverified(this.callbacks.unverified = (product: IAPProduct) => {
                // console.log('product unverified', product);
            });
            this.inAppPurchase.when(productId).refunded(this.callbacks.unverified = (product: IAPProduct) => {
                // console.log('product refunded', product);
            });
            this.inAppPurchase.when(productId).finished(this.callbacks.finished = (product: IAPProduct) => {
                // console.log('product finished', product);
            });
        }
    }

    private refreshProduct(productId: string) {
        const product = this.inAppPurchase.get(productId);
        // console.log('refreshProduct', product);
        // console.log(JSON.stringify(product));
        // if (product.transaction && !product.owned && product.state === 'approved') {
        //     // product.verify();
        //     console.log('should verify');
        // }

        this.gotProduct = product;

        if (!!product.id && !!product.priceMicros) {
            // this.zone.run(() => {
            // this.displayedPrices[product.id] = (product.priceMicros / 1000000).toFixed(2) +
            // '<sup class="currency" >' + product.currency + '</sup>';
            // });
            // console.log(product);
        }
    }

    private showPurchaseErrorMessage(message: string) {

    }

    private showSuccessPurchaseMessage() {
        // console.log('showSuccessPurchaseMessage');
        this.admobService.setPremium(true);
        const message = this.trans.trans('premium-purchase-successful');
        this.messages.showErrorMessage(message, true, true).then(() => {
            this.zone.run(() => {
                this.navController.navigateRoot('/').then();
            });
        });
    }
}
