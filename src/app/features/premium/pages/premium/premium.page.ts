import { Component } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-premium',
    templateUrl: './premium.page.html',
    styleUrls: ['./premium.page.scss'],
})
export class PremiumPage {
    constructor(
        public trans: TranslationsService,
        public navController: NavController,
    ) {
    }

    public goBack() {
        this.navController.pop().then();
    }
}
