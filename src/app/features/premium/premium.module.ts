import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PremiumPageRoutingModule } from './premium-routing.module';

import { BuyPremiumPage } from './pages/buy-premium/buy-premium.page';
import { PremiumPage } from '@feats/premium/pages/premium/premium.page';
import { CoreModule } from '@core/core.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        PremiumPageRoutingModule,
        CoreModule,
    ],
    declarations: [
        PremiumPage,
        BuyPremiumPage,
    ],
})
export class PremiumPageModule {
}
