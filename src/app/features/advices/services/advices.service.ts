import { Injectable, Injector } from '@angular/core';
import { LoadingAbleService } from '@core/services/base-services/loading-able.service';
import { ConstantsService } from '@core/services/constants.service';
import { Storage } from '@ionic/storage';
import { AdviceModel } from '../models/advice.model';
import { MeasureUnitsService } from '@core/services/measure-units.service';

@Injectable()
export class AdvicesService extends LoadingAbleService {

    constructor(
        injector: Injector,
        protected constants: ConstantsService,
        private storage: Storage,
        private measureUnitsService: MeasureUnitsService
    ) {
        super(injector);
    }

    public async getOwn(): Promise<AdviceModel[]> {
        let path = 'Advices/GetOwn';
        const lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            path = path + '?lang=' + lang;
        }
        const measureUnit = (await this.measureUnitsService.getMeasureUnitSystem()) || 'metric';
        if (measureUnit) {
            path += (lang ? '&' : '?') + 'unit=' + measureUnit;
        }
        return await this.get<AdviceModel[]>(path, true);
    }
}
