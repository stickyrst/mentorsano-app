import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvicesRoutingModule } from './advices-routing.module';
import { AdvicesService } from './services/advices.service';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        AdvicesRoutingModule,
    ],
    providers: [AdvicesService],
})
export class AdvicesModule {
}
