import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslationsService } from '@core/services/translations.service';
import { AdvicesService } from '../../services/advices.service';
import { AdviceModel } from '../../models/advice.model';
import { PremiumService } from '@feats/premium/services/premium.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-advices',
    templateUrl: './advices.page.html',
    styleUrls: ['./advices.page.scss'],
})
export class AdvicesPage implements OnInit, OnDestroy {

    private premiumListenerSubscription: Subscription;

    constructor(
        public trans: TranslationsService,
        public advicesService: AdvicesService,
        private premiumService: PremiumService,
        private navController: NavController,
    ) {
        this.advicesService.setDefaultConfig();
    }

    public advices: AdviceModel[];

    public async ngOnInit() {
        await this.load();
        this.premiumListenerSubscription = this.premiumService.onChange.subscribe(() => {
            this.load().then();
        });
    }

    public async load() {
        this.advices = null;
    }

    public async ionViewWillEnter() {
        if (!this.advices) {
            this.advices = await this.advicesService.getOwn();
        }
    }

    public ngOnDestroy(): void {
        if (this.premiumListenerSubscription) {
            this.premiumListenerSubscription.unsubscribe();
            this.premiumListenerSubscription = null;
        }
    }

    public cardClick(advice: AdviceModel): void {
        if (advice.youNeedPremiumToAccessThis) {
            this.navController.navigateForward('/premium').then();
        }
    }

}
