export interface AdviceModel {
    name: string;
    description: string;
    isPremium?: boolean;
    youNeedPremiumToAccessThis?: boolean;
}
