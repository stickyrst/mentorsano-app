import { APP_INITIALIZER, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router, RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from '@core/core.module';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { OpenApiToFormlyService } from '@core/services/openApi-to-formly.service';
import { AuthModuleModule } from '@auth/auth-module.module';
import { FormlyHelperModule } from '@feats/formly-helper/formly-helper.module';
import { Device } from '@ionic-native/device/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { FormsModule } from '@angular/forms';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { SignInWithApple } from '@ionic-native/sign-in-with-apple/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import * as Sentry from '@sentry/angular';
import { Market } from '@ionic-native/market/ngx';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot(),
        AppRoutingModule,
        CoreModule,
        AuthModuleModule.forRoot(),
        FormlyHelperModule.forRoot(),
        FormsModule,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        Device,
        AppVersion,
        OneSignal,
        SocialSharing,
        GooglePlus,
        Facebook,
        LocalNotifications,
        InAppPurchase2,
        SignInWithApple,
        GoogleAnalytics,
        Market,
        {
            provide: ErrorHandler,
            useValue: Sentry.createErrorHandler({}),
        },
        {
            provide: Sentry.TraceService,
            deps: [Router],
        },
        {
            provide: APP_INITIALIZER,
            useFactory: () => () => {
            },
            deps: [Sentry.TraceService],
            multi: true,
        },
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(private formlyConfigProvider: OpenApiToFormlyService) {
        this.formlyConfigProvider.formsUi = 'ionic';
    }
}
