import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-custom-loading-popup',
    templateUrl: './custom-loading-popup.component.html',
    styleUrls: ['./custom-loading-popup.component.scss'],
})
export class CustomLoadingPopupComponent implements OnInit {

    constructor() {
    }

    public ngOnInit() {
    }

}
