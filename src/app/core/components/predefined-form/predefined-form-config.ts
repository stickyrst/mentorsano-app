export interface PredefinedFormConfig {
    schemaName: string;
    getUrl: string;
    saveUrl: string;
    saveButtonText: string;
}
