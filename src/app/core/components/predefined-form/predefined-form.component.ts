import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { OpenApiToFormlyService } from '../../services/openApi-to-formly.service';
import { FormGroup } from '@angular/forms';
import { DisplayMessageService } from '../../services/display-message.service';
import { TranslationsService } from '../../services/translations.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';

@Component({
    selector: 'app-predefined-form',
    templateUrl: './predefined-form.component.html',
    styleUrls: ['./predefined-form.component.scss'],
})
export class PredefinedFormComponent implements OnInit, OnChanges {

    @Input()
    public submitButtonText: string;
    @Input()
    public formSchemaName: string;
    @Input()
    public alwaysShowSubmitButton: boolean;

    @Input()
    public buttonColor: string;

    @Output()
    public submit: EventEmitter<any> = new EventEmitter<any>();

    public fields: FormlyFieldConfig[];
    public form = new FormGroup({});

    @Output()
    public modelChange = new EventEmitter();
    @Input()
    public model: any;

    constructor(private formProvider: OpenApiToFormlyService,
                private messages: DisplayMessageService,
                private loading: CustomLoaderService,
                private translationsService: TranslationsService,
    ) {
        this.translationsService.languageChanged.subscribe(() => {
            this.ngOnInit().then(() => {
            });
        });
    }

    public markAsClean(): void {
        this.form.markAsUntouched({onlySelf: false});
    }

    public async ngOnInit() {
        if (!this.formSchemaName) {
            await this.loading.present();
            return;
        }
        await this.loadConfig();
    }

    public async ngOnChanges(changes: SimpleChanges): Promise<void> {
        if (changes.config && !changes.config.firstChange && changes.config.previousValue !== changes.config.currentValue) {
            await this.loadConfig();
        }
        if (changes.model && !changes.model.firstChange) {
            if (!this.model) {
                // this.modelChange.emit(this.model = {});
            }
        }
    }

    private async loadConfig(): Promise<void> {
        this.fields = await this.formProvider.getConfig(this.formSchemaName);

        for (const field of this.fields) {
            field.templateOptions.hideRequiredMarker = true;
        }
        // const copyFields = JSON.parse(JSON.stringify(this.fields));
        // console.log(copyFields);
        await this.loading.dismiss();
    }

    public async submitAction($event): Promise<void> {
        $event.stopPropagation();
        if (!this.form.valid) {
            await this.messages.display('Check the fields before submitting.');
            this.form.markAllAsTouched();
            return;
        }
        await this.messages.clear();
        await this.submit.next(this.model);
    }
}
