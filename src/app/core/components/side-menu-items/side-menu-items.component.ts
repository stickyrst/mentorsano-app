import { Component } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { ConstantsService } from '../../services/constants.service';
import { GlobalsService } from '../../services/globals.service';
import { TranslationsService } from '../../services/translations.service';
import { NavController } from '@ionic/angular';

@Component({
    selector: 'app-side-menu',
    templateUrl: './side-menu-items.component.html',
    styleUrls: ['./side-menu-items.component.scss'],
})
export class SideMenuItemsComponent {
    public appPages: MenuLink[] = [];

    public loggedInLinks: MenuLink[] = [
        {
            title: 'General information form',
            url: '/forms/general-information',
            icon: 'newspaper-outline',
        },
        // {
        //     title: 'Tips',
        //     url: '/advices',
        //     icon: 'bulb-outline',
        // },
        {
            title: 'tips',
            url: '/home/articles',
            icon: 'bulb-outline',
            queryParams: {type: 'advice'},
        },
        {
            title: 'activity-calendar',
            url: '/activity',
            icon: 'calendar-outline',
        },
        {
            title: 'menu_list_page',
            url: '/home/articles',
            icon: 'fast-food-outline',
            queryParams: {type: 'menu'},
            bottomSeparator: true,
        },
        {
            title: 'My profile',
            url: '/profile',
            icon: 'person',
        },
        {
            title: 'Logout',
            action: async () => {
                await this.auth.logout();
                await this.navController.navigateRoot('/login');
            },
            icon: 'exit',
        },
    ];

    public nonLoggedInLinks: MenuLink[] = [
        {
            title: 'Login',
            url: '/login',
            icon: 'person',
        },
    ];

    public aboutPage: MenuLink = {
        title: 'About app',
        url: '/about',
        icon: 'information-circle-outline',
    };
    public developerPage: MenuLink = {
        title: 'Developer',
        url: '/developer',
        icon: 'bug',
    };

    constructor(public auth: AuthService,
                public constants: ConstantsService,
                public globals: GlobalsService,
                public trans: TranslationsService,
                private navController: NavController,
    ) {
    }
}

export interface MenuLink {
    title: string;
    url?: string;
    action?: () => void;
    icon: string;
    hidden?: boolean;
    queryParams?: { [key: string]: string };
    bottomSeparator?: boolean;
}
