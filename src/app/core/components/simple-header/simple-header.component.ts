import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-simple-header',
    templateUrl: './simple-header.component.html',
    styleUrls: ['./simple-header.component.scss'],
})
export class SimpleHeaderComponent implements OnInit {

    @Input() public title: string;
    @Input() public defaultBackHref: string;
    @Input() public transparentBackground: boolean;
    @Input() public hideBackButton: boolean;
    @Input() public noPaddingTop: boolean;
    @Input() public showLogo: boolean;

    constructor() {
    }

    public ngOnInit() {
    }

}
