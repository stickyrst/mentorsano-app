import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-form-progress-bar',
    templateUrl: './form-progress-bar.component.html',
    styleUrls: ['./form-progress-bar.component.scss'],
})
export class FormProgressBarComponent implements OnInit {

    @Input()
    public text: string;
    @Input()
    public value: number;
    @Input()
    public color: string;

    @Input()
    public isIndeterminate: boolean;

    constructor() {
    }

    public ngOnInit() {
    }

}
