import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormConfigResolver } from './resolvers/form-config.resolver';
import { DisplayMessageService } from './services/display-message.service';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SideMenuItemsComponent } from './components/side-menu-items/side-menu-items.component';
import { ConstantsService } from './services/constants.service';
import { PredefinedFormComponent } from './components/predefined-form/predefined-form.component';
import { FormlyModule } from '@ngx-formly/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormProgressBarComponent } from '@core/components/form-progress-bar/form-progress-bar.component';
import { StripHtmlTagsPipe } from '@core/pipes/strip-html-tags.pipe';
import { CustomLoadingPopupComponent } from '@core/components/custom-loading-popup/custom-loading-popup.component';
import { SimpleHeaderComponent } from '@core/components/simple-header/simple-header.component';
import { MsCardComponent } from '@core/components/ms-card/ms-card.component';

const exportedComponents = [
    SideMenuItemsComponent,
    PredefinedFormComponent,
    FormProgressBarComponent,
    SimpleHeaderComponent,
    MsCardComponent,
];

@NgModule({
    declarations: [
        ...exportedComponents,
        StripHtmlTagsPipe,
        CustomLoadingPopupComponent,
    ],
    imports: [
        CommonModule,
        IonicModule,
        RouterModule,
        FormlyModule,
        ReactiveFormsModule,
    ],
    providers: [
        FormConfigResolver,
        DisplayMessageService,
        ConstantsService,
    ],
    exports: [
        ...exportedComponents,
        StripHtmlTagsPipe,
    ],
})
export class CoreModule {
}
