import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { ConstantsService } from '@core/services/constants.service';

@Injectable({
    providedIn: 'root',
})
export class FirstOpenGuard implements CanActivate {
    constructor(
        private storage: Storage,
        private navController: NavController,
    ) {
    }

    public async canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Promise<boolean> {
        const landingPageSkipped = await this.storage.get(ConstantsService.landingPageSkippedKey) === null;

        if (landingPageSkipped) {
            await this.navController.navigateRoot('/landing-page');
            return false;
        }
        await this.navController.navigateRoot('/home');
        return false;
    }
}
