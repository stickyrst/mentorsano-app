import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { OpenApiToFormlyService } from '../services/openApi-to-formly.service';
import { Injectable } from '@angular/core';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { NavController } from '@ionic/angular';
import { DisplayMessageService } from '@core/services/display-message.service';
import { EMPTY } from 'rxjs';

@Injectable()
export class FormConfigResolver implements Resolve<FormlyFieldConfig[]> {
    constructor(
        private formProvider: OpenApiToFormlyService,
        private loading: CustomLoaderService,
        private navController: NavController,
        private messages: DisplayMessageService,
    ) {
    }

    public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<FormlyFieldConfig[]> {
        if (!route.data) {
            throw new Error('formSchemaName not provided!');
        }
        await this.loading.present();
        try {
            const config = await this.formProvider.getConfig(route.data.formSchemaName);
            await this.loading.dismiss();
            return config;
        } catch (e) {
            await this.loading.dismiss();
            await this.messages.showError(e, true, true);
            await this.navController.navigateRoot('/');
            return undefined;
        }
    }

}
