import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'stripHtmlTags',
})
export class StripHtmlTagsPipe implements PipeTransform {
    public transform(value: string): any {
        if (!value || typeof value.replace !== 'function') {
            return value;
        }
        return value.replace(/<.*?>/g, ''); // replace tags
    }
}
