import { FormlyFieldConfig } from '@ngx-formly/core';

export interface CustomFormModel {
    slug: string;
    questions: FormlyFieldConfig[];
    name: string;
}
