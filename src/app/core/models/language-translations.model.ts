export interface LanguageTranslationsModel {
    language: string;
    timestamp: Date;
    translations: { [key: string]: string };
}
