export interface NotifDataModel {
    title: string;
    text: string;
    time: Date;
    data: any;
    actions: any;
}
