import { ErrorHandler, Injectable } from '@angular/core';
import { AuthService } from '@auth/services/auth.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CustomLoaderService } from '@core/services/custom-loader.service';

@Injectable({
    providedIn: 'root',
})
export class ForceReloadAppService {

    public isForceReloading: boolean;

    constructor(
        private auth: AuthService,
        private localNotifications: LocalNotifications,
        private loaderService: CustomLoaderService,
        private errorHandler: ErrorHandler
    ) {
    }

    public async blockUi() {
        this.isForceReloading = true;
        await this.loaderService.present();
    }

    public async forceReloadWithReason(reason: string, error: Error) {
        this.isForceReloading = true;
        await this.loaderService.present();
        await this.localNotifications.cancelAll();
        await this.auth.logout();

        setTimeout(() => {
            window.location.href = '/';
        }, 5000);

        this.errorHandler.handleError(error);
    }
}
