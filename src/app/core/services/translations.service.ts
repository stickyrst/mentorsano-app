import { EventEmitter, Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { LanguageTranslationsModel } from '../models/language-translations.model';
import { Storage } from '@ionic/storage';
import { OpenApiToFormlyService } from './openApi-to-formly.service';
import { ConstantsService } from './constants.service';
import { Task } from '@core/models/task';
import { AlertController } from '@ionic/angular';
import { CustomLoaderService } from '@core/services/custom-loader.service';

@Injectable({
    providedIn: 'root',
})
export class TranslationsService {

    public readonly languageChanged: EventEmitter<any> = new EventEmitter<any>();
    public readonly loaded: EventEmitter<any> = new EventEmitter<any>();

    private transObj: LanguageTranslationsModel;

    public get currentLanguage(): string {
        if (this.transObj && this.transObj.language) {
            return this.transObj.language;
        } else {

        }
    }

    constructor(
        private apiService: ApiService,
        private storage: Storage,
        private openApiToFormlyService: OpenApiToFormlyService,
        private alertController: AlertController,
        private loading: CustomLoaderService,
    ) {
    }

    public trans(slug: string, fallBackToNull?: boolean): string {
        if (this.translationExits(slug)) {
            return this.transObj.translations[slug];
        }
        return fallBackToNull ? null : slug;
    }

    public transf(slug: string, ...args: string[]): string {
        return this.format(this.trans(slug), ...args);
    }

    public async transA(slug: string): Promise<string> {
        if (this.translationExits(slug)) {
            return this.transObj.translations[slug];
        }
        if (!this.transObj) {
            await this.loaded.toPromise();
        }
        return this.trans(slug);
    }

    public async loadLanguage(lang: string): Task {
        this.log('getting translation from backend');
        this.transObj = await this.apiService.get('Translation/Get/' + lang);
        this.storage.set(ConstantsService.languageStorageKey, lang).then(() => {
            this.log('saved language in storage (' + lang + ')');
            this.openApiToFormlyService.clearCache();
            this.languageChanged.emit();
        });
        this.storage.set(ConstantsService.translationsStorageKey, this.transObj).then(() => {
            this.log('saved translations in storage');
        });
        this.loaded.emit();
        this.loaded.complete();
    }

    public async loadLanguageIfIsNew(lang: string): Task {
        if (lang && this.currentLanguage !== lang) {
            await this.loadLanguage(lang);
        }
    }

    public async loadSavedOrDefaultLanguage(askIfNotSet?: boolean): Task {
        let lang = await this.storage.get(ConstantsService.languageStorageKey);
        if (lang) {
            this.log('loading translations from storage...');
            const transObj = await this.storage.get(ConstantsService.translationsStorageKey);
            if (transObj) {
                this.transObj = transObj;
                this.log('loaded translations from storage...');
                this.loaded.emit();
                this.loaded.complete();
                return;
            } else {
                await this.loadLanguage(lang);
            }
        } else {
            if (askIfNotSet) {
                const loadingVisible = this.loading.currentlyVisible;
                lang = await this.presentLanguageSelectorAlert();
            }
            lang = lang || 'en';
            await this.loadLanguage(lang);
        }
    }

    public async forceLoad(resetSetLanguage?: boolean): Task {
        if (resetSetLanguage) {
            await this.storage.remove(ConstantsService.languageStorageKey);
        }
        await this.storage.remove(ConstantsService.translationsStorageKey);
        await this.loadSavedOrDefaultLanguage(resetSetLanguage);
    }

    private log(msg: string): void {
        // console.log(msg);
    }


    public format(format: string, ...args: string[]) {
        if (typeof format !== 'string') {
            throw new Error('Invalid format \'' + format + '\'.');
        }
        args = args || [];
        return format.replace(/{(\d+)}/g, (match, nr) => typeof args[nr] !== 'undefined' ? args[nr] : match);
    }

    public translationExits(slug: string): boolean {
        return this.transObj && this.transObj.translations && this.transObj.translations.hasOwnProperty(slug);
    }


    public async presentLanguageSelectorAlert(): Promise<string> {
        const lang = 'en';

        const langs = await this.apiService.get('Translation/GetLanguages');
        const inputs = langs.map(l => {
            return {...l, name: 'language', type: 'radio'};
        });
        inputs[0].checked = true;
        const alert = await this.alertController.create({
            header: 'Select Language',
            backdropDismiss: false,
            inputs,
            buttons: [
                {
                    text: 'Ok',
                },
            ],
        });

        await alert.present();

        const alertResult = await alert.onDidDismiss();

        return alertResult.data.values;
    }
}
