import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class ReferencesHelperService {
    public populateReferences(obj) {
        const cache: ILocalCache = {};
        if (obj == null || typeof obj !== 'object' || obj.$populated) {
            return;
        }

        try {
            this.recFunc(obj, cache);
            obj.$populated = true;
        } catch (exc) {
            console.log('can\'t populate?');
            console.error(exc);
        }
    }

    protected recFunc(obj: any, cache: ILocalCache) {
        if (!obj || typeof obj !== 'object') {
            return;
        }

        if (obj.hasOwnProperty('$id') && !cache.hasOwnProperty(obj.$id)) {
            const id = obj.$id;
            delete obj.$id;
            cache[id] = obj;
            this.iteratePropsOrElements(obj, cache);
        } else if (obj instanceof Array) {
            this.iteratePropsOrElements(obj, cache);
        }
    }

    protected iteratePropsOrElements(obj: any, cache: ILocalCache) {
        const keys = Object.keys(obj);
        for (const key of keys) {
            const child = obj[key];
            if (typeof child !== 'object' || !child) {
                continue;
            }
            if (child.hasOwnProperty('$ref')) {
                const id = child.$ref;
                if (!cache.hasOwnProperty(id)) {
                    console.log('id not in cache: ' + id);
                    continue;
                }
                obj[key] = cache[id];
            } else {
                this.recFunc(child, cache);
            }
        }
    }

    public removeRefArtifacts(obj: any) {
        if (!obj || typeof obj !== 'object') {
            return;
        }
        if (!obj.$id && !obj.$ref) {
            return;
        }
        delete obj.$id;
        delete obj.$ref;
        const keys = Object.keys(obj);
        for (const key of keys) {
            if (obj[key] && typeof obj[key] === 'object') {
                this.removeRefArtifacts(obj[key]);
            }
        }
    }
}

interface ILocalCache {
    [key: string]: any;
}
