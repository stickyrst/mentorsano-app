import { Injectable, NgZone } from '@angular/core';
import { NavController } from '@ionic/angular';
import { OSNotificationOpenedResult } from '@ionic-native/onesignal';
import { GlobalsService } from '@core/services/globals.service';

@Injectable({
    providedIn: 'root',
})
export class NotificationsHandlerService {

    constructor(
        private zone: NgZone,
        private navController: NavController,
        private globals: GlobalsService,
    ) {
    }

    public openTypeMap: { [ket: string]: string } = {
        article: '/home/articles?type=article',
        menu: '/home/articles?type=menu',
        advice: '/home/articles?type=advice',
        checkIn: '/home/check?',
        checkOut: '/home/check/checkOut?',
    };

    public async handleNotificationOpened(osNotifyOpenedResult: OSNotificationOpenedResult): Promise<void> {
        // await this.zone.run(async () => {
        //   await this.navController.navigateForward(aData.openPage);
        // });
        // console.log(osNotifyOpenedResult);
        // if (osNotifyOpenedResult.action && osNotifyOpenedResult.action.actionID !== 'open') {
        //     return;
        // }
        const data = osNotifyOpenedResult.notification.payload.additionalData;
        if (!data || !data.open) {
            return;
        }
        await this.handleNotifOpenedData(data);
    }

    public async handleNotifOpenedData(data: any) {
        let url: string;
        if (data.open.type && this.openTypeMap.hasOwnProperty(data.open.type)) {
            url = this.openTypeMap[data.open.type];
            if (data.open.id) {
                url += '&autoOpen=' + data.open.id;
            }
        } else {
            url = data.link;
        }
        if (!url) {
            return;
        }
        await this.zone.run(async () => {
            if (this.globals.appReady) {
                await this.navController.navigateForward(url);
            } else {
                this.globals.onReady.subscribe(async () => {
                    await this.navController.navigateForward(url);
                });
            }
        });
    }
}
