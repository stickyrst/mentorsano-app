import { ErrorHandler, Injectable } from '@angular/core';
import { AlertController, ToastController } from '@ionic/angular';
import { TranslationsService } from '@core/services/translations.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ForceReloadAppService } from '@core/services/force-reload-app.service';

@Injectable()
export class DisplayMessageService {
    private DEFAULT_DURATION = 6 * 1000;

    private preventAlertsOrMessages: boolean;

    public alertIsVisible: boolean;

    constructor(
        private toasts: ToastController,
        private alertController: AlertController,
        private trans: TranslationsService,
        private forceReload: ForceReloadAppService,
        private errorHandler: ErrorHandler,
    ) {

    }

    private lastToast: HTMLIonToastElement;

    public async clear(): Promise<void> {
        if (await this.toasts.getTop()) {
            await this.toasts.dismiss();
        }
    }

    public async display(message: string, asAlert: boolean = false,
                         blockUntilAlertDismiss?: boolean, duration = this.DEFAULT_DURATION): Promise<void> {
        if (this.preventAlertsOrMessages) {
            return;
        }
        if (!asAlert) {
            if (this.lastToast) {
                await this.lastToast.dismiss();
                await this.clear();
            }
            const toast = await this.toasts.create({message, duration});
            this.lastToast = toast;
            this.lastToast.onDidDismiss().then(() => {
                if (this.lastToast === toast) {
                    this.lastToast = null;
                }
            });
            await toast.present();
        } else {
            // this.dialog.open(MessageDialogComponent, {data: {message: message}});
            const alert = await this.alertController.create({message, buttons: ['Ok']});
            this.alertIsVisible = true;
            await alert.present();
            if (blockUntilAlertDismiss) {
                await alert.onDidDismiss();
                this.alertIsVisible = false;
            } else {
                alert.onDidDismiss().then(() => {
                    this.alertIsVisible = false;
                });
            }
        }
    }

    public async showErrorMessage(message: string, asAlert?: boolean, blockUntilAlertDismiss?: boolean): Promise<void> {
        await this.display(`${message}`, asAlert, blockUntilAlertDismiss);
    }

    public async showError(response: any, asAlert?: boolean, blockUntilAlertDismiss?: boolean): Promise<void> {
        let finalMessage = null;
        let sendToSentry = false;
        try {
            if (response === 'cordova_not_available') {
                finalMessage = 'You are not running on a device.';
            } else if (response.error && typeof response.error === 'string') {
                finalMessage = response.error;
            } else if (response.error && response.error.error && typeof response.error.error === 'string') {
                finalMessage = response.error.error;
            } else if (response && response.error && response.error.length && response.error[0].description) {
                finalMessage = response.error.map(x => x.description).join(' ');
            } else if (response instanceof HttpErrorResponse && response.status === 0) {
                // this is a network error maybe? so use the default message.
                console.error('network error');
                // console.error(response);
            } else {
                // use the default message too.
                // console.error(response);
                if (response && response.status === 401) {
                    await this.forceResetApp('Your session is invalid or expired. Please login again.' +
                        '<br/><br/> Sesiunea este invalidă sau a expirat. Te rugăm încearcă din nou.');
                    return;
                }
            }
        } catch (error) {
            // console.error(error);
            // console.error(response);
        }
        if (!finalMessage) {
            finalMessage = 'unknown-error-network';
        }
        if (this.trans.translationExits(finalMessage)) {
            finalMessage = this.trans.trans(finalMessage);
            sendToSentry = true;
        }
        if (finalMessage === 'unknown-error-network') {
            finalMessage = 'Oops! Something went wrong! Make sure you are connected to internet. ' +
                'Then try again and if the problem persists please contact us.';
            sendToSentry = true;
        }
        await this.showErrorMessage(finalMessage, asAlert, blockUntilAlertDismiss);
        if (sendToSentry) {
            this.errorHandler.handleError(response);
        }
    }

    public async forceResetApp(reason: string, error?: any) {
        if (!error) {
            error = new Error(reason);
            error.name = 'force-reload';
        }
        console.error('Force reset app: ' + reason);
        await this.showErrorMessage(reason, true, true);
        this.preventAlertsOrMessages = true;
        await this.forceReload.forceReloadWithReason(reason, error);
    }
}
