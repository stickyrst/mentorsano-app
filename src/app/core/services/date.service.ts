import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class DateService {

    constructor() {
    }

    public get currentDate(): Date {
        const now = new Date();
        // now.setDate(now.getDate() + 3);
        return now;
    }

    public get currentDay(): Date {
        const now = this.currentDate;
        return new Date(now.getFullYear(), now.getMonth(), now.getDate());
    }
}
