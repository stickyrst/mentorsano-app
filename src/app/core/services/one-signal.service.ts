import { EventEmitter, Injectable } from '@angular/core';
import { ConstantsService } from './constants.service';
import { Platform } from '@ionic/angular';
import { OneSignal, OSDisplayType } from '@ionic-native/onesignal/ngx';
import { OSNotification, OSNotificationOpenedResult } from '@ionic-native/onesignal';
import { NotifDeviceApiService } from './notif-device-api.service';
import { NotificationsHandlerService } from './notifications-handler.service';
import { AuthService } from '@auth/services/auth.service';
import { debounceTime } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class OneSignalService {

    private identity: { userId?: string, pushToken?: string } = {};

    private shouldRegister: EventEmitter<void> = new EventEmitter<void>();

    constructor(
        private platform: Platform,
        private oneSignal: OneSignal,
        private constants: ConstantsService,
        private ndaS: NotifDeviceApiService,
        private notifHandler: NotificationsHandlerService,
        private auth: AuthService,
    ) {
    }

    public get oneSignalUserId() {
        return this.identity.userId;
    }

    public initialize(): void {
        if (this.platform.is('android')) {
            // console.log('is android');
            this.oneSignal.startInit(this.constants.oneSignalAppId, this.constants.googleProjectNumber);
        } else {
            // console.log('is not android');
            this.oneSignal.startInit(this.constants.oneSignalAppId);
        }

        this.oneSignal.inFocusDisplaying(OSDisplayType.Notification);

        this.oneSignal.handleNotificationReceived().subscribe(async (osNotif: OSNotification) => {
            // do something when notification is received
            // console.log('received notification', new Date());
            // console.log(osNotif);
            await this.handleNotificationReceived(osNotif);
        });

        this.oneSignal.handleNotificationOpened().subscribe(async (osNotifyOpenedResult: OSNotificationOpenedResult) => {
            // do something when a notification is opened
            await this.handleNotificationOpened(osNotifyOpenedResult);
        });

        this.oneSignal.endInit();
        this.oneSignal.getIds().then(async identity => {
            // console.log(identity.pushToken + ' is pushToken');
            // console.log(identity.userId + ' is  userId');
            this.identity = {pushToken: identity.pushToken, userId: identity.userId};
            this.shouldRegister.emit();
        });

        this.auth.authStateChanged.subscribe(async s => {
            this.shouldRegister.emit();
        });
        this.shouldRegister.asObservable().pipe(debounceTime(2000)).subscribe(() => {
            this.doRegister().then();
        });
    }

    public async doRegister(): Promise<void> {
        if (!this.identity.userId) {
            return;
        }
        await this.ndaS.registerDevice(this.identity.pushToken, this.identity.userId);
    }

    private async handleNotificationReceived(osNotif: OSNotification) {
    }

    private async handleNotificationOpened(osNotifyOpenedResult: OSNotificationOpenedResult) {
        await this.notifHandler.handleNotificationOpened(osNotifyOpenedResult);
    }
}
