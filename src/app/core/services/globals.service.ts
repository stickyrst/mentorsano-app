import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';
import { ConstantsService } from '@core/services/constants.service';

@Injectable({
    providedIn: 'root',
})
export class GlobalsService {
    constructor(
        private storage: Storage,
    ) {
        this.storage.get(ConstantsService.developerKey).then(value => {
            this.developerValue = value || false;
        });
    }

    private developerValue: boolean;

    public get developer(): boolean {
        return this.developerValue;
    }

    public set developer(value: boolean) {
        this.developerValue = value;
        this.storage.set('developer', value).then(() => {
        });
    }

    private isAppReady = false;
    private subject: Subject<void> = new Subject<void>();

    public get onReady() {
        return this.subject.asObservable();
    }

    public get appReady(): boolean {
        return this.isAppReady;
    }

    public set appReady(value: boolean) {
        this.isAppReady = value;
        if (this.isAppReady) {
            this.subject.next();
            this.subject.complete();
        }
    }
}

