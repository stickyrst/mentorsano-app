import { ErrorHandler, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstantsService } from '@core/services/constants.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { AlertController, ModalController, Platform } from '@ionic/angular';
import { TranslationsService } from '@core/services/translations.service';
import { Market } from '@ionic-native/market/ngx';
import { ForceReloadAppService } from '@core/services/force-reload-app.service';
import { DisplayMessageService } from '@core/services/display-message.service';

@Injectable({
    providedIn: 'root',
})
export class AppUpdateService {
    private cache: AppUpdateResponse;

    constructor(
        private http: HttpClient,
        private constants: ConstantsService,
        public appVersion: AppVersion,
        private platform: Platform,
        private modalCtrl: ModalController,
        private alertController: AlertController,
        private trans: TranslationsService,
        private market: Market,
        private forceReload: ForceReloadAppService,
        private messages: DisplayMessageService,
        private errorHandler: ErrorHandler
    ) {
    }

    public async get(): Promise<AppUpdateResponse> {
        if (this.cache) {
            return this.cache;
        }
        const version = await this.appVersion.getVersionNumber() as string;
        // tslint:disable-next-line:no-unused-expression
        const store = this.platform.is('android') ? 'Android' : 'Ios';
        const url = this.constants.apiUrl + 'AppUpdates/Check';
        this.cache = await this.http.get<AppUpdateResponse>(url, {params: {store, version}, responseType: 'json'}).toPromise();
        if (this.cache.$id) {
            delete this.cache.$id;
        }
        return this.cache;
    }

    public async check(): Promise<void> {
        try {
            await this.get();
            const u = this.cache;
            if (u.forceUpdate) {
                await this.showModal(true);
            } else if (u.updateAvailable) {
                await this.showModal(false);
            }
        } catch (e) {
            this.errorHandler.handleError(e);
        }
    }

    public async showModal(force: boolean): Promise<void> {
        if (this.messages.alertIsVisible) {
            setTimeout(() => {
                this.check();
            }, 5000);
            return;
        }

        const config = {
            backdropDismiss: false,
            header: await this.trans.trans('update-' + (force ? 'required' : 'available') + '-title'),
            message: await this.trans.trans('update-' + (force ? 'required' : 'available') + '-text'),
            buttons: [
                {
                    text: await this.trans.trans('update-open-store'),
                    handler: async () => {
                        await this.forceReload.blockUi();
                        await this.market.open(await this.appVersion.getPackageName());
                    },
                },
            ],
        };
        if (!force) {
            config.buttons.splice(0, 0, {
                text: await this.trans.trans('update-maybe-later'),
                handler: async () => {
                },
            });
        }
        const alertRef = await this.alertController.create(config);
        await alertRef.present();
        await alertRef.onDidDismiss();
    }

    public triggerUpdateCheck(): void {
        setTimeout(() => {
            this.check().then();
        }, 2000);
    }
}

export interface AppUpdateResponse {
    $id: string;
    version: string;
    clearStorage: boolean;
    forceUpdate: boolean;
    updateAvailable: boolean;
}
