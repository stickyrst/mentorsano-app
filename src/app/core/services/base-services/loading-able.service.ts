import { Injector } from '@angular/core';
import { BaseHttpedService } from '@auth/services/base-httped.service';
import { ConstantsService } from '../constants.service';
import { DisplayMessageService } from '../display-message.service';
import { ReferencesHelperService } from '../references-helper.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { NavController } from '@ionic/angular';
import { CustomHttpOptions } from '@auth/services/custom-http.client';

export class LoadingAbleService extends BaseHttpedService {

    protected constants: ConstantsService;
    protected messages: DisplayMessageService;
    protected loading: CustomLoaderService;

    private referencesHelper: ReferencesHelperService;

    protected navController: NavController;

    constructor(injector: Injector) {
        super(injector);
        this.constants = injector.get(ConstantsService);
        this.messages = injector.get(DisplayMessageService);
        this.loading = injector.get(CustomLoaderService);
        this.referencesHelper = injector.get(ReferencesHelperService);
        this.navController = injector.get(NavController);
    }

    protected config: LoadingAbleServiceConfig = {};

    public async get<T>(path: string, withAuth: boolean = false,
                        config?: LoadingAbleServiceConfig,
                        options?: CustomHttpOptions,
    ): Promise<T> {
        const url = this.constants.apiUrl + path;
        config = Object.assign({}, this.config, config);
        await this.toggleLoadingDialog(config, true);
        try {
            let rez: T;
            if (withAuth) {
                rez = await this.aHttp.get<T>(url, options);
            } else {
                rez = await this.http.get<T>(url, options).toPromise();
            }
            this.referencesHelper.populateReferences(rez);

            await this.toggleLoadingDialog(config, false);

            return rez;
        } catch (e) {
            await this.processError(e, config);
            if (config.throw) {
                throw  e;
            }
            return undefined;
        }
    }

    public async post<T, T2>(path: string, model: T2, config?: LoadingAbleServiceConfig): Promise<T> {
        const url = this.constants.apiUrl + path;
        config = Object.assign({}, this.config, config);

        await this.toggleLoadingDialog(config, true);
        try {
            const rez = await this.aHttp.post<T>(url, model);

            await this.toggleLoadingDialog(config, false);

            this.referencesHelper.populateReferences(rez);
            return rez;
        } catch (e) {
            await this.processError(e, config);
            if (config.throw) {
                throw  e;
            }
            return undefined;
        }
    }

    public async patch<T, T2>(path: string, model: T2, config?: LoadingAbleServiceConfig): Promise<T> {
        const url = this.constants.apiUrl + path;
        config = Object.assign({}, this.config, config);

        await this.toggleLoadingDialog(config, true);
        try {
            const rez = await this.aHttp.patch<T>(url, model);

            await this.toggleLoadingDialog(config, false);

            this.referencesHelper.populateReferences(rez);
            return rez;
        } catch (e) {
            await this.processError(e, config);
            if (config.throw) {
                throw  e;
            }
            return undefined;
        }
    }
    public async delete<T, T2>(path: string,  config?: LoadingAbleServiceConfig): Promise<T> {
        const url = this.constants.apiUrl + path;
        config = Object.assign({}, this.config, config);

        await this.toggleLoadingDialog(config, true);
        try {
            const rez = await this.aHttp.delete<T>(url);

            await this.toggleLoadingDialog(config, false);

            this.referencesHelper.populateReferences(rez);
            return rez;
        } catch (e) {
            await this.processError(e, config);
            if (config.throw) {
                throw  e;
            }
            return undefined;
        }
    }

    private async processError(e: Error, config?: LoadingAbleServiceConfig) {
        if (!config.uiSilent) {
            setTimeout(() => {
                this.loading.dismiss().then();
            });
            await this.messages.showError(e, true, true);
            if (config.goBackOnError) {
                await this.navController.navigateRoot('/');
            }
        } else {
            throw e;
        }
    }

    protected async toggleLoadingDialog(config?: LoadingAbleServiceConfig, show?: boolean) {
        if (config?.uiSilent) {
            return;
        }
        if (show) {
            await this.loading.present();
        } else {
            await this.loading.dismiss();
        }
    }

    public setConfig(config: LoadingAbleServiceConfig): void {
        this.config = config;
    }

    public setDefaultConfig(): void {
        this.setConfig({uiSilent: false, goBackOnError: true});
    }
}

export interface LoadingAbleServiceConfig {
    uiSilent?: boolean;
    goBackOnError?: boolean;
    throw?: boolean;
}
