import { Injectable } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';

@Injectable({providedIn: 'root'})
export class BlockHwdBackButtonService {
    private stash: IHwdButtonStashElement[] = [];

    constructor(
        private navCtrl: NavController,
    ) {
    }

    public push(block: IHwdButtonStashElement | boolean) {
        if (typeof block === 'boolean') {
            block = {block};
        }
        this.stash.push(block);
    }

    public pop(ref?: any) {
        if (ref) {
            for (let i = 0; i < this.stash.length; i++) {
                if (this.stash[i].ref === ref) {
                    this.stash.splice(i, 1);
                    i--;
                }
            }
            return;
        }
        this.stash.splice(this.stash.length - 1, 1);
    }

    public isRefPushed(ref: any) {
        return this.stash.some(se => se && se.ref === ref);
    }

    private get head(): IHwdButtonStashElement {
        return this.stash.length && this.stash[this.stash.length - 1];
    }


    public initialize(platform: Platform) {
        let listenerAdded: boolean;
        platform.backButton.subscribeWithPriority(9999, () => {
            if (!listenerAdded) {
                document.addEventListener('backbutton', event => {
                    event.preventDefault();
                    event.stopPropagation();
                }, false);
                listenerAdded = true;
            }
            const head = this.head;
            if (!head || !head.block) {
                this.navCtrl.back();
            } else {
                if (head.oneTimeBlockCallback) {
                    this.pop();
                    head.oneTimeBlockCallback();
                }
            }
        });
    }
}

export interface IHwdButtonStashElement {
    block?: boolean;
    oneTimeBlockCallback?: () => void;
    ref?: any;
}
