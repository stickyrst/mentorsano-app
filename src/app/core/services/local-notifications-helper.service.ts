import { EventEmitter, Injectable } from '@angular/core';
import { ILocalNotification, LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CustomFormModel } from '@core/models/custom-form.model';
import { DisplayMessageService } from '@core/services/display-message.service';
import { CheckFormsService } from '@feats/check-forms/services/check-forms.service';
import { DateService } from '@core/services/date.service';
import { NotificationsHandlerService } from '@core/services/notifications-handler.service';
import { ConstantsService } from '@core/services/constants.service';
import { AlertController } from '@ionic/angular';
import { AlertButton } from '@ionic/core/dist/types/components/alert/alert-interface';
import { AuthService } from '@auth/services/auth.service';
import { debounceTime } from 'rxjs/operators';
import { ReferencesHelperService } from '@core/services/references-helper.service';
import { TranslationsService } from '@core/services/translations.service';
import { environment } from '../../../environments/environment';


@Injectable({providedIn: 'root'})
export class LocalNotificationsHelperService {

    private shouldSetForMeals: EventEmitter<void> = new EventEmitter<void>();
    private defaultNotif: ILocalNotification = {
        smallIcon: 'res://notif',
        icon: 'res://ic_launcher',
        color: '#169948',
        launch: true,
    };

    constructor(
        private localNotifications: LocalNotifications,
        private messages: DisplayMessageService,
        private checkForms: CheckFormsService,
        private dateService: DateService,
        private notifsHandler: NotificationsHandlerService,
        private constants: ConstantsService,
        private alertController: AlertController,
        private auth: AuthService,
        private refHelper: ReferencesHelperService,
        private trans: TranslationsService,
    ) {
        auth.authStateChanged.pipe(debounceTime(500)).subscribe(state => {
            this.setForMeals();
        });
    }

    private eachNotifCount = 5;

    public async handleResponse(customForm: CustomFormModel, response: any): Promise<void> {
        if (customForm.slug === 'general-information') {
            await this.setForMeals();
        }
    }

    public setForMeals() {
        this.shouldSetForMeals.emit();
    }

    private async setForMealsAction(): Promise<void> {
        if (!this.constants.hasCordova) {
            return;
        }

        if (!await this.ensureLocalNotificationsPermissions()) {
            return;
        }

        this.log('setForMealsAction');

        this.log('clear all local notifs');
        await this.clearAllLocalNotifs();

        if (!await this.auth.authStateAsync) {
            return;
        }

        this.log('getting notifs to set');
        let notifs;
        try {
            notifs = await this.checkForms.getMealsNotifications({uiSilent: true, goBackOnError: false});
        } catch (e) {
            return;
        }
        this.log('got notifs to set', notifs);

        const today = new Date();
        for (let i = 0; i < notifs.length; i++) {
            const notif = notifs[i];
            const timeDate = new Date(notif.time);
            const firstDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), timeDate.getHours(), timeDate.getMinutes());
            const index = i + 1;
            this.refHelper.removeRefArtifacts(notif);
            const clone = JSON.parse(JSON.stringify(notif));
            delete clone.time;
            const localNotif = clone as ILocalNotification;
            if (localNotif.actions) {
                for (const action of localNotif.actions) {
                    if ((action as ILocalNotification).id) {
                        (action as ILocalNotification).launch = true;
                    }
                }
            }
            await this.customScheduleDailyNotif(index, localNotif, firstDate, this.eachNotifCount);
        }
    }

    private customScheduleDailyNotif(index: number, notif: ILocalNotification, first: Date, count: number) {
        let date = new Date(first);
        for (let i = 0; i < count; i++) {
            const id = index * 10 + i;
            const notifOptions: ILocalNotification = {
                ...this.defaultNotif,
                ...notif,
                id,
                trigger: {at: date},
            };
            // this.log('scheduling notif: ' + id, date.toString(), notifOptions);
            this.localNotifications.schedule(notifOptions);

            date = new Date(date);
            date.setDate(date.getDate() + 1);
        }
    }

    public async clearAllLocalNotifs() {
        await this.localNotifications.cancelAll();
    }

    private async cancelCustomScheduledDailyNotif(index: number, count: number) {
        for (let i = 0; i < count; i++) {
            const id = index * 10 + i;
            if (await this.localNotifications.isScheduled(id)) {
                this.log('canceling notif: ' + index);
                await this.localNotifications.cancel(id);
            }
        }
    }


    private async ensureLocalNotificationsPermissions() {
        if (!await this.localNotifications.hasPermission()) {
            await this.localNotifications.requestPermission();

            if (!await this.localNotifications.hasPermission()) {
                await this.messages.display('Can\'t set notifications because permissions were not provided.', true);
                return false;
            }
        }
        return true;
    }

    private log(...args: any[]) {
        if (environment.debug) {
            console.log(...args);
        }
    }

    public initialize() {
        this.localNotifications.on('trigger').subscribe((event: ILocalNotification) => {
            this.log('triggered some notif?');
            this.log(event);
            // this.localNotifications.clear(event.id).then();
            // this.showInAppNotificationAlert(event).then();
        });
        this.localNotifications.on('click').subscribe((event: ILocalNotification) => {
            this.handleNotifOpen(event);
        });
        this.localNotifications.on('open').subscribe((event: ILocalNotification) => {
            this.handleNotifOpen(event);
        });
        this.localNotifications.fireQueuedEvents().then();
        this.shouldSetForMeals.asObservable().pipe(debounceTime(2000)).subscribe(() => {
            this.setForMealsAction().then();
        });
    }

    private handleNotifOpen(notif: ILocalNotification) {
        this.log('clicked some notif?');
        this.log(notif);
        if (notif.data && notif.data.open) {
            this.notifsHandler.handleNotifOpenedData(notif.data).then();
        }
    }

    public async showInAppNotificationAlert(event: ILocalNotification) {
        const buttons: AlertButton[] = [{text: this.trans.trans('dismiss')}];
        const openAction = event.actions && typeof event.actions === 'object' && event.actions.find(a => a.id === 'open');
        if (openAction) {
            buttons.push({
                text: openAction.title,
                handler: async () => {
                    this.handleNotifOpen(event);
                },
            });
        }
        const alert = await this.alertController.create({
            backdropDismiss: false,
            header: event.title,
            message: event.text.toString(),
            buttons,
        });
        await alert.present();
        await alert.onDidDismiss();
    }
}
