import { Injectable, Injector } from '@angular/core';
import { BaseHttpedService } from '@auth/services/base-httped.service';
import { ConstantsService } from './constants.service';

@Injectable({
    providedIn: 'root',
})
export class ApiService extends BaseHttpedService {

    constructor(injector: Injector, private constants: ConstantsService) {
        super(injector);
    }

    public async getAllFromPath(path: string, useCache?: boolean): Promise<any[]> {
        return await this.get(path);
    }

    public async get(path: string, useCache?: boolean): Promise<any> {
        const url = this.constants.apiUrl + path;
        return await this.aHttp.get(url);
    }
}
