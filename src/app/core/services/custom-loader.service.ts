import { Injectable } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CustomLoadingPopupComponent } from '@core/components/custom-loading-popup/custom-loading-popup.component';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class CustomLoaderService {
    private currentShownModal: HTMLIonModalElement;
    private isShown: boolean;
    public customOptions: any = {};

    public get currentlyVisible() {
        return this.isShown;
    }

    constructor(private modalController: ModalController) {
    }

    public async present(fullScreen?: boolean): Promise<void> {
        this.log('CustomLoadingService.present');
        if (this.isShown) {
            return;
        }
        this.isShown = true;
        this.log('CustomLoadingService.present creating');
        this.currentShownModal = await this.modalController.create({
            component: CustomLoadingPopupComponent,
            cssClass: 'custom-loading-popup' + (fullScreen ? 'full-screen' : ''),
            backdropDismiss: false,
            ...this.customOptions,
        });
        this.currentShownModal.onDidDismiss().then(() => {
            this.currentShownModal = null;
            this.isShown = false;
        });
        if (!this.isShown) {
            this.log('CustomLoadingService.present aborting');
            try {
                await this.currentShownModal.dismiss();
            } catch {
            }
        } else {
            this.log('CustomLoadingService.present presenting');
            try {
                await this.currentShownModal.present();
            } catch {
            }
        }
    }

    public async dismiss(): Promise<void> {
        this.log('CustomLoadingService.dismiss()');
        if (!this.isShown) {
            this.log('it\'s not shown');
            this.log('exists: ' + !!this.currentShownModal);
            return;
        }
        this.isShown = false;
        if (this.currentShownModal) {
            this.log('real dismiss');
            try {
                await this.currentShownModal.dismiss();
            } catch {
            }
        } else {
            this.log('nothing to dismiss');
        }
    }

    public async toggle(): Promise<void> {
        if (this.isShown) {
            await this.dismiss();
        } else {
            await this.present();
        }
    }

    private log(...args: any[]) {
        if (false && environment.debug) {
            console.log(...args);
        }
    }
}
