import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ConstantsService } from './constants.service';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '@auth/services/auth.service';
import { AppVersion } from '@ionic-native/app-version/ngx';
import { Device } from '@ionic-native/device/ngx';

@Injectable({providedIn: 'root'})
export class NotifDeviceApiService {

    constructor(private constants: ConstantsService,
                private http: HttpClient,
                private auth: AuthService,
                private appVersion: AppVersion,
                private device: Device,
                private platform: Platform
    ) {
    }

    public async registerDevice(pushToken: string, oneSignalUserId: string): Promise<void> {
        const url = this.constants.apiUrl + 'AppDevices/Register';
        const body = {
            deviceToken: pushToken,
            osVersion: this.device.version,
            model: this.device.model,
            appVersion: await this.appVersion.getVersionNumber(),
            type: this.platform.is('android') ? 'android' : this.platform.is('ios') ? 'ios' : 'other',
            oneSignalUserId,
        };
        const options = await this.auth.getOptions(true);
        try {
            await this.http.post(url, body, options).toPromise();
        } catch (e) {
            console.error('Couldn\'t register device: ' + e.message);
        }
    }

}
