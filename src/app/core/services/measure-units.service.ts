import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ConstantsService } from '@core/services/constants.service';
import { TranslationsService } from '@core/services/translations.service';

@Injectable({
    providedIn: 'root',
})
export class MeasureUnitsService {

    constructor(
        private storage: Storage,
        private alertController: AlertController,
        private trans: TranslationsService,
    ) {
    }

    private units = {
        metric: {
            weight: 'kg',
            distance: 'cm',
            volume: 'ml',
        },
        imperial: {
            weight: 'lbs',
            distance: 'ft',
            volume: 'oz',
        },
    };

    private measureUnit: 'metric' | 'imperial';

    private static getFieldsWithMeasureUnits(fields: FormlyFieldConfig[]): FormlyFieldConfig[] {
        return fields.filter(field => {
            if ((field.type !== 'input' && field.type !== 'ms-input') || field.templateOptions.type !== 'number') {
                return false;
            }
            const measureUnitType = field.templateOptions.measureUnitType;
            if (measureUnitType === 'weight' || measureUnitType === 'distance') {
                return true;
            }
        });
    }

    public async isImperial() {
        return (await this.getMeasureUnitSystem()) === 'imperial';
    }

    public async isMeasureUnitSet(): Promise<boolean> {
        if (!this.measureUnit) {
            this.measureUnit = await this.storage.get(ConstantsService.measureUnitKey);
        }
        return !!this.measureUnit;
    }

    public async getMeasureUnitSystem(): Promise<'metric' | 'imperial'> {
        if (!this.measureUnit) {
            this.measureUnit = await this.storage.get(ConstantsService.measureUnitKey);
        }
        if (!this.measureUnit) {
            this.measureUnit = 'metric';
        }
        return this.measureUnit;
    }

    public async setMeasureUnit(unit: 'metric' | 'imperial') {
        this.measureUnit = unit;
        await this.storage.set(ConstantsService.measureUnitKey, unit);
    }

    public async clear() {
        this.measureUnit = undefined;
        await this.storage.remove(ConstantsService.measureUnitKey);
    }

    private getDisplayName(type: 'distance' | 'weight'): string {
        return this.units[this.measureUnit][type];
    }

    public convertUnit(value, type, to) {
        const distanceFactor = 30.48;
        const weightFactor = 2.205;
        const volumeFactor = 29.574;
        let num = 0;

        if (to === 'metric') {
            if (type === 'distance') {
                num = value * distanceFactor;
            } else if (type === 'weight') {
                num = value / weightFactor;
            } else if (type === 'volume') {
                num = value * volumeFactor;
            }
        } else if (to === 'imperial') {
            if (type === 'distance') {
                num = value / distanceFactor;
            } else if (type === 'weight') {
                num = value * weightFactor;
            } else if (type === 'volume') {
                num = value / volumeFactor;
            }
        }
        return +(Math.round(parseFloat(num + 'e+1')) + 'e-1');
    }

    public async patchForDisplay(fields: FormlyFieldConfig[], model: any, opt?: PatchForDisplayOptions) {
        opt = opt || {};

        fields = MeasureUnitsService.getFieldsWithMeasureUnits(fields);
        if (!fields.length) {
            return;
        }

        if (opt.showModalIfNotSet) {
            const isSet = await this.isMeasureUnitSet();
            if (!isSet) {
                await this.presentUnitSystemAlert();
            }
        }

        for (const field of fields) {
            const measureUnitType = field.templateOptions.measureUnitType;

            field.templateOptions.measureUnit = this.getDisplayName(measureUnitType);

            if (!opt.skipLabelEdit) {
                field.templateOptions.label = field.templateOptions.label.replace(/ \([a-z]*\)/, '');
                field.templateOptions.label += ` (${field.templateOptions.measureUnit})`;
            }

            if (opt.addMeasureUnitWrapper) {
                field.wrappers = field.wrappers || opt.defaultWrappers || [];
                field.wrappers.push('measure-unit');
            }

            const key = field.key as string;
            if (await this.isImperial() && model[key] && typeof model[key] === 'number') {
                model[key] = this.convertUnit(model[key], measureUnitType, 'imperial');
            }
        }
    }

    public async patchForSubmit(fields, model) {
        if (await this.isImperial()) {
            for (const field of fields) {
                if (field.type !== 'input' || field.templateOptions.type !== 'number') {
                    continue;
                }
                const measureUnitType = field.templateOptions.measureUnitType;
                if (measureUnitType !== 'none' && typeof model[field.key] === 'number') {
                    if (await this.isImperial()) {
                        model[field.key] = this.convertUnit(model[field.key], measureUnitType, 'metric');
                    }
                }
            }
        }
        return model;
    }

    public async presentUnitSystemAlert() {
        await this.getMeasureUnitSystem();
        const alert = await this.alertController.create({
            backdropDismiss: false,
            header: this.trans.trans('measure-unit-system'),
            message: this.trans.trans('measure-unit-system-question'),
            buttons: [
                {
                    text: 'Metric',
                    handler: async () => {
                        await this.setMeasureUnit('metric');
                    },
                },
                {
                    text: 'Imperial',
                    handler: async () => {
                        await this.setMeasureUnit('imperial');
                    },
                },
            ],
        });
        await alert.present();
        await alert.onDidDismiss();
    }
}


export interface PatchForDisplayOptions {
    showModalIfNotSet?: boolean;
    skipLabelEdit?: boolean;
    addMeasureUnitWrapper?: boolean;
    defaultWrappers?: string[];
}
