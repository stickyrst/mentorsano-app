import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { environment } from '../../../environments/environment';

@Injectable()
export class ConstantsService {

    public static readonly languageStorageKey = 'language';
    public static readonly translationsStorageKey = 'translations';
    public static readonly landingPageSkippedKey = 'landingPageSkipped';
    public static readonly developerKey = 'developer';
    public static readonly measureUnitKey = 'measureUnit';


    public static storageVersion = '0.5.5';
    public static storageVersionStorageKey = 'storage-version';

    private readonly hasCordovaValue: boolean;

    public constructor(private platform: Platform) {
        this.hasCordovaValue = this.platform.is('cordova');
        this.currentPlatforms = this.platforms.filter(pl => this.platform.is(pl as any));
    }

    public readonly oneSignalAppId = '2be5c952-327c-4baf-9b2c-a07618cdcb6c';
    public readonly googleProjectNumber = '900805810115';

    public get apiUrl() {
        if (environment.production) {
            return 'https://app.mentorsano.com/api/';
        }

        return 'https://app.mentorsano.com/api/';
        // return '/api/';
        // return 'http://192.168.7.101:7100/api/';
        // return 'http://192.168.7.100:7100/api/';
        // return 'http://192.168.100.3:7100/api/';
        // return 'http://192.168.1.101:7100/api/';
        // return 'http://192.168.43.44:7100/api/';
    }

    public get hasCordova() {
        return this.hasCordovaValue;
    }

    public currentPlatforms: string[] = [];
    private platforms: string[] = ['android', 'cordova', 'ios', 'ipad', 'iphone', 'phablet', 'tablet',
        'electron', 'pwa', 'mobile', 'mobileweb', 'desktop', 'hybrid'];
}

