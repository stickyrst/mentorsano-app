import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Observable, Observer } from 'rxjs';
import { ConstantsService } from './constants.service';
import { ApiService } from './api.service';
import { Storage } from '@ionic/storage';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root',
})
export class OpenApiToFormlyService {

    private get docUrl(): string {
        return this.constants.apiUrl + 'ModelMetadata/GetTranslatedSwaggerJson';
    }

    constructor(private http: HttpClient,
                private constants: ConstantsService,
                private api: ApiService,
                private storage: Storage
    ) {
        this.startDocRequest();
    }

    public formsUi: string;

    private swaggerDoc: IOpenApiDocument;

    private docRequestObservable: Observable<void>;
    private loadingDoc: boolean;

    private fieldConfigs: { [key: string]: FormlyFieldConfig[] } = null;

    private get schemasFieldConfigs(): { [key: string]: FormlyFieldConfig[] } {
        return this.fieldConfigs || (this.fieldConfigs = {ActivationCodeForm: this.activationCodeFormConfig});
    }

    private typeMap: { [key: string]: string } = {
        string: 'input',
        boolean: 'checkbox',
        integer: 'input',
        object: 'formly-group',
    };

    private formatChangeTypeMap: { [key: string]: string } = {
        multiline: 'textarea',
        select: 'select',
        date: 'date',
        time: 'time',
        float: 'input',
    };

    private typeChangeFormatMap: { [key: string]: string } = {
        boolean: 'checkbox',
    };

    private formatMap: { [key: string]: string } = {
        int32: 'number',
        float: 'number',
    };

    private fieldsToCopyToTemplateObject: Array<string | string[]> = [
        'minLength', 'maxLength', ['minimum', 'min'], ['maximum', 'max'],
    ];

    public async getConfig(schemaName: string): Promise<FormlyFieldConfig[]> {
        this.log('getConfig(' + schemaName + ')');
        if (!this.swaggerDoc) {
            this.log('giving toPromise result');
            await this.docRequestObservable.toPromise();
        }
        this.log('now creating and getting config');
        return await this.createAndGetConfig(schemaName);
    }

    private async createAndGetConfig(schemaName: string): Promise<FormlyFieldConfig[]> {
        if (!this.schemasFieldConfigs.hasOwnProperty(schemaName)) {
            await this.createFormlyFieldConfigsFromOpenApiSchema(schemaName);
        }
        return this.schemasFieldConfigs[schemaName];
    }

    public hasConfig(schemaName: string) {
        return this.swaggerDoc && this.schemasFieldConfigs.hasOwnProperty(schemaName);
    }

    private async createFormlyFieldConfigsFromOpenApiSchema(schemaName: string): Promise<void> {
        const schema = this.getSchema(schemaName);
        const fieldConfigs: FormlyFieldConfig[] = [];
        for (const key in schema.properties) {
            if (!schema.properties.hasOwnProperty(key)) {
                continue;
            }
            let prop = schema.properties[key];
            if (prop.$ref) {
                prop = {
                    ...prop,
                    ...this.swaggerDoc.components.schemas[schemaName + '.' + key],
                };
            }
            if (prop['x-formlyIgnore']) {
                continue;
            }
            fieldConfigs.push(await this.getFieldConfigFromProperty(prop as IExtendedOpenApiProperty, key, schema));
        }
        this.schemasFieldConfigs[schemaName] = fieldConfigs;
    }

    private async getFieldConfigFromProperty(prop: IExtendedOpenApiProperty, key: string, schema: IOpenApiSchema):
        Promise<FormlyFieldConfig> {

        const fieldConfig: FormlyFieldConfig = {
            key,
            type: this.typeMap[prop.type] || prop.type,
            validators: Object.assign({}, prop.validators),
            validation: {messages: {}},
            templateOptions: Object.assign({}, {type: this.formatMap[prop.format] || prop.format}, prop['x-templateOptions']),
        };
        // fieldConfig.templateOptions.type = this.typeChangeFormatMap[prop.type] || fieldConfig.templateOptions.type;
        fieldConfig.type = this.formatChangeTypeMap[prop.format] || fieldConfig.type;

        this.copyNeededFields(prop, fieldConfig);
        if (schema.required && schema.required.indexOf(key) !== -1) {
            fieldConfig.templateOptions.required = true;
        }
        if (prop.$ref && fieldConfig.type === 'formly-group') {
            fieldConfig.fieldGroup = await this.createAndGetConfig(this.getSchemaNameFromRef(prop.$ref));
        }
        if (fieldConfig.type === 'array' && prop.items) {
            fieldConfig.fieldArray = {
                fieldGroup: await this.createAndGetConfig(this.getSchemaNameFromRef(prop.items.$ref)),
                ...fieldConfig.fieldArray,
            };
        }

        if (fieldConfig.type === 'select') {
            await this.patchCustomSelectOptions(fieldConfig);
        }
        if (this.formsUi === 'ionic') {
            if (fieldConfig.type !== 'toggle' && fieldConfig.type !== 'select'
                && fieldConfig.type !== 'radio' && fieldConfig.type !== 'checkbox') {
                delete fieldConfig.templateOptions.placeholder;
                fieldConfig.templateOptions.labelPosition = 'floating';
            }
        }
        this.buildValidators(prop, fieldConfig);
        return fieldConfig;
    }

    private async patchCustomSelectOptions(fieldConfig: FormlyFieldConfig): Promise<void> {
        if (fieldConfig.templateOptions.select && fieldConfig.templateOptions.select.url) {
            fieldConfig.templateOptions.options = await this.api.getAllFromPath(fieldConfig.templateOptions.select.url);
        }
    }

    private buildValidators(prop: IOpenApiProperty, fieldConfig: FormlyFieldConfig) {
        if (!prop['x-validators']) {
            return;
        }
        for (const validator of prop['x-validators']) {
            if (Validators.hasOwnProperty(validator.name)) {
                let fValidator = Validators[validator.name];
                if (typeof validator.args !== 'undefined') {
                    fValidator = fValidator(validator.args);
                }
                fieldConfig.validators.validation.push(fValidator);
                fieldConfig.validation.messages[validator.name] = validator.message;
            }
        }
    }

    private copyNeededFields(prop: IOpenApiProperty, fieldConfig: FormlyFieldConfig) {
        const setIfNotUndefined: (target, propName, value) => void = (target, propName, value) => {
            if (typeof value !== 'undefined') {
                target[propName] = value;
            }
        };
        for (const f of this.fieldsToCopyToTemplateObject) {
            if (typeof f === 'string') {
                setIfNotUndefined(fieldConfig.templateOptions, f, prop[f]);
            } else {
                setIfNotUndefined(fieldConfig.templateOptions, f[1], prop[f[0]]);
            }
        }

        const xProps = prop['x-props'];
        if (xProps) {
            for (const propKey in xProps) {
                if (!xProps.hasOwnProperty(propKey)) {
                    continue;
                }
                let targetObj = fieldConfig;
                const propPath = propKey.split('/').filter((pp) => pp);
                const propName = propPath[propPath.length - 1];
                propPath.length--;
                for (const pathPart of propPath) {
                    if (typeof targetObj[pathPart] === 'undefined') {
                        targetObj[pathPart] = {};
                    }
                    targetObj = targetObj[pathPart] = targetObj[pathPart] || {};
                }
                targetObj[propName] = xProps[propKey];
            }
        }
    }

    private getSchemaNameFromRef(refPath: string): string {
        const pathParts = refPath.split('/').filter((part) => part !== '#');
        return pathParts[2];
    }

    private getSchema(schemaName: string): IOpenApiSchema {
        return this.swaggerDoc.components.schemas[schemaName];
    }

    public clearCache(): void {
        this.swaggerDoc = null;
        this.fieldConfigs = null;
        this.loadingDoc = false;
        this.startDocRequest();
    }

    private startDocRequest(): void {
        let observers: Array<Observer<void>> = [];
        this.log('startDocRequest');
        this.docRequestObservable = new Observable(observer => {
            this.log('in docRequestObservable, pushing new observer');
            observers.push(observer);
            if (!this.loadingDoc) {
                this.log('not loading doc, loading it now');
                this.loadingDoc = true;
                this.log('getting language');
                this.storage.get(ConstantsService.languageStorageKey).then(async lang => {
                    this.log('got language: ' + lang);
                    let url = this.docUrl;
                    if (lang) {
                        url = this.docUrl + '?lang=' + lang;
                    }
                    this.log('making request ...');
                    const result: IOpenApiDocument = await this.http.get<IOpenApiDocument>(url).toPromise();
                    this.log('got response from server, nexting and completing observables');
                    this.swaggerDoc = result;
                    for (const observer2 of observers) {
                        observer2.next();
                        observer2.complete();
                    }
                    observers = [];
                }).catch(e => {
                    this.log('catched in openApiToFormly service');
                    for (const observer2 of observers) {
                        this.log('sending error to observer');
                        observer2.error(e);
                        observer2.complete();
                    }
                    observers = [];
                    this.loadingDoc = false;
                });
            }
        });
    }

    private log(...args: any[]) {
        if (environment.debug) {
            console.log(...args);
        }
    }

    //
    // private setCategories(config: ElementConfig[]) {
    //     const categories = new Set(config.map((x: any) => x.category));
    //
    //     // Add delimiters
    //     categories.forEach(category => {
    //         if (!category) {
    //             return;
    //         }
    //         // Find first item with that category
    //         const index = config.findIndex((x: any) => x.category === category);
    //
    //         config.splice(index, 0, this.gfFormBuilderHelper.makeViewElement('delimiter'));
    //         config.splice(index, 0, this.gfFormBuilderHelper.makeViewElement('text', category));
    //     });
    // }

    // private processDefinition(defName: string, refModel?: any): ElementConfig[] {
    //     const definition = this.swaggerDoc.definitions[defName];
    //     if (!definition) {
    //         throw new Error('No definition for ' + defName + ' in Swagger Document!');
    //     }
    //
    //     const config = this.mapDefinitionToConfig(definition, refModel);
    //
    //     this.setCategories(config);
    //
    //     return config;
    // }

    // private mapDefinitionToConfig(definition: any, refModel?: any): ElementConfig[] {
    //     const config = [];
    //     const props = definition.properties;
    //     for (const definitionKey in props) {
    //         if (!props.hasOwnProperty(definitionKey)) {
    //             continue;
    //         }
    //         const prop = props[definitionKey];
    //         if (!prop.usedInGenericForm) {
    //             continue;
    //         }
    //         const element = this.mapPropToElement(prop, definitionKey);
    //         if (refModel) {
    //             element.finalModel = refModel;
    //         }
    //         config.push(element);
    //     }
    //     return config;
    // }

    // private mapPropToElement(property: any, name: string): FieldConfig {
    //     const element = Object.assign({}, property, {
    //         name: name,
    //         type: property.customFormat || property.format || 'input',
    //         validation: [],
    //         errorMessages: {},
    //         value: property.default,
    //     });
    //
    //     this.buildValidators(property, element);
    //
    //     return element;
    // }
    //
    // private buildValidators(property: any, element: any) {
    //     const validators: { name: string, args: any, message: string }[] = property.validators;
    //
    //     for (const validator of validators) {
    //         if (Validators.hasOwnProperty(validator.name)) {
    //             let fValidator = Validators[validator.name];
    //             if (typeof validator.args !== 'undefined') {
    //                 fValidator = fValidator(validator.args);
    //             }
    //             element.validation.push(fValidator);
    //             element.errorMessages[validator.name.toLowerCase()] = validator.message || 'still no';
    //         }
    //     }
    // }

    // tslint:disable-next-line:member-ordering
    private activationCodeFormConfig: FormlyFieldConfig[] = [
        {
            key: 'token',
            type: 'input',
            templateOptions: {
                label: 'Code',
                placeholder: 'Activation code',
                required: true,
            },
        },
    ];

}

export interface IOpenApiDocument {
    components: {
        schemas: {
            [key: string]: IOpenApiSchema,
        }
    };
    paths: any[];
}

export interface IOpenApiReferenceObject {
    $ref: string;
}

export interface IOpenApiSchema extends IOpenApiReferenceObject {
    type: 'string' | 'textarea' | 'object' | 'array';
    properties: { [key: string]: IOpenApiProperty | IOpenApiSchema };
    additionalProperties: any[];
    required: string[];
}

export interface IOpenApiProperty extends IOpenApiReferenceObject {
    type: string;
    format: string;
    minLength: number;
    nullable: boolean;
    items: IOpenApiSchema;
}

export interface IExtendedOpenApiProperty extends IOpenApiProperty {
    validators: any[];
    'x-validators': any[];
    'x-props': { [key: string]: any };
}
