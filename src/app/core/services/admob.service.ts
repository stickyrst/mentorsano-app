import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
// import { ProfileService } from '@feats/profile/services/profile.service';
import { Storage } from '@ionic/storage';
import { ConstantsService } from '@core/services/constants.service';

declare var admob;

@Injectable({providedIn: 'root'})
export class AdmobService {
    public static readonly hasPremiumStorageKey = 'hasPremium';
    private crtState: boolean;

    public idBannerObj = {
        android: 'ca-app-pub-5557347831317471/3292003518',
        ios: 'ca-app-pub-5557347831317471/4266173832',
    };
    public idInterstitialObj = {
        android: 'ca-app-pub-5557347831317471/9593767533',
        ios: 'ca-app-pub-5557347831317471/4977518060',
    };

    private urlsWithBanner: string[] = ['/home/articles', '/articles/article'];

    private hasPremium: boolean;

    constructor(
        private router: Router,
        private storage: Storage,
        private constants: ConstantsService,
    ) {
        this.hasPremium = true;
        this.storage.get(AdmobService.hasPremiumStorageKey).then(hp => {
            this.hasPremium = hp === true;
        });
    }

    public init() {
        // admob.setDevMode(true);
        this.router.events.pipe(
            filter((e): boolean => e instanceof NavigationEnd)
        ).subscribe((e: NavigationEnd) => {
            if (this.urlsWithBanner.some(u => e.url.startsWith(u))) {
                this.showBanner();
            } else {
                this.hideBanner();
            }
        });
    }

    public showInterstitial() {
        if (this.hasPremium) {
            // console.log('interstitial hit premium');
            return;
        }
        admob.interstitial.load({
            id: this.idInterstitialObj,
        }).then(() => admob.interstitial.show());
    }

    public showBanner() {
        if (!this.constants.hasCordova) {
            return;
        }
        if (this.hasPremium) {
            // console.log('showBanner hit premium');
            return;
        }
        // console.log('should show');
        if (this.crtState === undefined || !this.crtState) {
            // console.log('and show!');
            this.crtState = true;
            admob.banner.show({
                id: this.idBannerObj,
            }).then(() => {
                // console.log('banner displayed');
            }).catch((e) => {
                // console.log('show err', e);
            });
        }
    }

    public hideBanner(force?: boolean) {
        if (!this.constants.hasCordova) {
            return;
        }
        if (!force && this.hasPremium) {
            // console.log('hideBanner hit premium');
            return;
        }
        // console.log('should hide');
        if (force || this.crtState === undefined || this.crtState) {
            // console.log('and hide!');
            this.crtState = false;
            admob.banner.hide(this.idBannerObj).then(() => {
                // console.log('banner hidden');
            }).catch((e) => {
                // console.log('hide err', e);
            });
        }
    }

    public setPremium(value: boolean): void {
        this.hasPremium = value;
        this.storage.set(AdmobService.hasPremiumStorageKey, value).then();
        this.hideBanner(true);
    }

    public getPremium(): boolean {
        return this.hasPremium;
    }

}
