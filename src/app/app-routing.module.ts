import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FirstOpenGuard } from '@core/resolvers/first-open.guard';
import { AuthGuard } from '@auth/guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./features/landing-page/landing-page.module').then(m => m.LandingPagePageModule),
        pathMatch: 'full',
        canActivate: [FirstOpenGuard],
    },
    {
        path: 'home',
        loadChildren: () => import('./tabs-home/tabs-home.module').then(m => m.TabsHomePageModule),
        canActivate: [AuthGuard],
    },
    {
        path: 'login',
        loadChildren: () => import('./auth/login/login.module').then(m => m.LoginPageModule),
    },
    {
        path: 'register',
        loadChildren: () => import('./auth/register/register.module').then(m => m.RegisterPageModule),
    },
    {
        path: 'profile',
        loadChildren: () => import('./features/profile/profile.module').then(m => m.ProfileModule),
    },
    {
        path: 'forms',
        loadChildren: () => import('./features/custom-forms/custom-forms.module').then(m => m.CustomFormsModule),
    },
    {
        path: 'about',
        loadChildren: () => import('./features/about/about.module').then(m => m.AboutPageModule),
    },
    {
        path: 'articles',
        loadChildren: () => import('./features/articles/articles.module').then(m => m.ArticlesModule),
    },
    {
        path: 'advices',
        loadChildren: () => import('./features/advices/advices.module').then(m => m.AdvicesModule),
    },
    {
        path: 'activity',
        loadChildren: () => import('./features/user-activity/user-activity.module').then(m => m.UserActivityModule),
    },
    {
        path: 'developer',
        loadChildren: () => import('./features/developer/developer.module').then(m => m.DeveloperPageModule),
    },
    {
        path: 'landing-page',
        loadChildren: () => import('./features/landing-page/landing-page.module').then(m => m.LandingPagePageModule),
    },
    {
        path: 'be-honest',
        loadChildren: () => import('./features/be-honest/be-honest.module').then(m => m.BeHonestPageModule),
    },
    {
        path: 'simple-text',
        loadChildren: () => import('./features/simple-text/simple-text.module').then(m => m.SimpleTextPageModule),
    },
    {
        path: 'premium',
        loadChildren: () => import('./features/premium/premium.module').then(m => m.PremiumPageModule),
    },
    {
        path: 'achievements',
        loadChildren: () => import('./features/achievements/achievements.module').then(m => m.AchievementsPageModule),
    },
    {
        path: 'user-defined-todos',
        loadChildren: () => import('./features/user-defined-todos/user-defined-todos.module').then(m => m.UserDefinedTodosPageModule),
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules,
            relativeLinkResolution: 'legacy',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
