import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth.service';
import { AccountService } from './services/account.service';
import { CustomHttpClient } from './services/custom-http.client';


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
    ],
    providers: [
        AuthService,
        CustomHttpClient,
    ],
})
export class AuthModuleModule {
    public static forRoot(): ModuleWithProviders<AuthModuleModule> {
        return {
            ngModule: AuthModuleModule,
            providers: [
                AuthService,
                AccountService,
                CustomHttpClient,
            ],
        };
    }
}
