import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Task } from '@core/models/task';
import { NavController, Platform } from '@ionic/angular';
import { AuthService } from '@auth/services/auth.service';
import { AccountService } from '@auth/services/account.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { TranslationsService } from '@core/services/translations.service';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { ProfileService } from '@feats/profile/services/profile.service';
import { Facebook } from '@ionic-native/facebook/ngx';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { ASAuthorizationAppleIDRequest, SignInWithApple } from '@ionic-native/sign-in-with-apple/ngx';

@Component({
    selector: 'app-social-media-login',
    templateUrl: './social-media-login.component.html',
    styleUrls: ['./social-media-login.component.scss'],
})
export class SocialMediaLoginComponent implements OnInit {

    private googleRetry: boolean;
    public isIos: boolean;

    @Output()
    public loggedIn: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(
        private loading: CustomLoaderService,
        private auth: AuthService,
        private accounts: AccountService,
        private messages: DisplayMessageService,
        private navController: NavController,
        public trans: TranslationsService,
        private googlePlus: GooglePlus,
        private profileService: ProfileService,
        private fb: Facebook,
        private signInWithApple: SignInWithApple,
        private platform: Platform,
    ) {
        this.isIos = this.platform.is('ios');
    }

    public ngOnInit() {
    }

    public async loginFacebook(): Task {
        await this.loading.present();

        try {
            const permissions = ['public_profile', 'email'];
            const response = await this.fb.login(permissions);

            if (response.status !== 'connected' || !response.authResponse || !response.authResponse.accessToken) {
                await this.messages.showErrorMessage('Not connected to Facebook, please try again.');
            }

            const facebookUser = await this.fb.api('/me?fields=name,email', permissions);

            const result = await this.accounts.loginFacebook(response.authResponse);
            if (result === true) {
                this.loggedIn.emit(true);
            } else {
                await this.messages.showErrorMessage('Couldn\'t validate login, please try again.');
            }
        } catch (e) {
            await this.loading.dismiss();
            if (e === 'cordova_not_available') {
                await this.messages.showErrorMessage('Cordova platform not available. Are you running on a device?');
                return;
            }
            console.log(e);
            if (e === 'This request needs declined permission: email' || e && e.errorCode === '4201') {
                await this.messages.showErrorMessage('You closed the login dialog without providing access. ' +
                    'Access to the email address is required.');
            } else if (e && e.error) {
                await this.messages.showError(e.error);
            } else {
                await this.messages.showErrorMessage('Couldn\'t login, please try again.');
            }
        }
    }

    public async loginGoogle(): Task {
        await this.loading.present();
        // https://medium.com/enappd/implement-google-login-in-ionic-4-apps-using-firebase-57334bad0910
        try {
            const googleUser = await this.googlePlus.login({
                scopes: 'email', // optional, space-separated list of scopes, If not included or empty, defaults to `profile` and `email`.
                webClientId: '900805810115-ls12hc1mham6qkgvu5t7lc26745qjm5c.apps.googleusercontent.com',
                // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included
                // to get an idToken. On iOS, it is not required.
                offline: true, // Optional, but requires the webClientId - if set to true the plugin will also return a serverAuthCode,
                // which can be used to grant offline access to a non-Google server
            });
            // console.log(user);

            const result = await this.accounts.loginGoogle(googleUser);
            if (result === true) {
                this.loggedIn.emit(true);
            } else {
                await this.messages.showErrorMessage('Couldn\'t validate login, please try again.');
            }
            // console.log(session);
        } catch (e) {
            await this.loading.dismiss();
            if (e === 'cordova_not_available') {
                await this.messages.showErrorMessage('Cordova platform not available. Are you running on a device?');
                return;
            }
            console.error(e);
            if (e === 8 && !this.googleRetry) {
                this.googleRetry = true;
                await this.loginGoogle();
                return;
            }
            await this.messages.showErrorMessage('Couldn\'t login, please try again.');
        }
    }

    public async loginApple(): Task {
        // https://ionicframework.com/docs/native/sign-in-with-apple
        await this.loading.present();
        try {
            const res = await this.signInWithApple.signin({
                requestedScopes: [
                    ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
                    ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail,
                ],
            });
            const result = await this.accounts.loginApple(res);
            if (result === true) {
                this.loggedIn.emit(true);
            } else {
                await this.messages.showErrorMessage('Couldn\'t validate login, please try again.');
            }
        } catch (e) {
            await this.loading.dismiss();
            console.log(e);
            if (e === 'cordova_not_available') {
                await this.messages.showErrorMessage('Cordova platform not available. Are you running on a device?');
                return;
            }
            if (e.code === '1001' || e.code === '1003') {
                await this.messages.showErrorMessage('Login cancelled, please try again.');
                return;
            }
            await this.messages.showErrorMessage('Couldn\'t login, please try again.');
        }
    }
}
