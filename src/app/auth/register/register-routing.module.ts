import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterPage } from './register.page';
import { FormConfigResolver } from '@core/resolvers/form-config.resolver';

const routes: Routes = [
    {
        path: '',
        component: RegisterPage,
        resolve: {formConfig: FormConfigResolver},
        data: {formSchemaName: 'RegisterRequestModel'},
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class RegisterPageRoutingModule {
}
