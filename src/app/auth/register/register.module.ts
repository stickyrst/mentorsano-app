import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';
import { CoreModule } from '@core/core.module';
import { FormlyModule } from '@ngx-formly/core';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegisterPageRoutingModule,
        CoreModule,
        FormlyModule,
    ],
  declarations: [RegisterPage],
})
export class RegisterPageModule {}
