import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DisplayMessageService } from '@core/services/display-message.service';
import { AccountService } from '../services/account.service';
import { ConfirmationRequestModel } from '../auth.models';
import { TranslationsService } from '@core/services/translations.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage {

    constructor(private loading: CustomLoaderService,
                private accountService: AccountService,
                private messages: DisplayMessageService,
                private navController: NavController,
                public trans: TranslationsService,
    ) {
    }

    public registerModel = {
        // email: 'mentorsano-user@mailinator.com',
        // password: 'parola01',
        // confirmPassword: 'parola01',
        // firstName: 'Test',
        // lastName: 'Test',
    };

    public activationCodeModel: ConfirmationRequestModel = {};

    private processing: boolean;

    public async submitRegister() {
        if (this.processing) {
            return;
        }
        this.processing = true;
        await this.loading.present();
        await this.messages.clear();
        try {
            const result = await this.accountService.register(this.registerModel);
            const message = this.trans.trans(result, true) ||
                this.trans.trans('register-success-message', true) ||
                'Account created. You can login now.';
            await this.messages.display(message);
            await this.navController.navigateBack('/login');
        } catch (e) {
            console.log('error', e);
            await this.messages.showError(e);
        }
        await this.loading.dismiss();
        this.processing = false;
    }

    public async submitActivation() {
        if (this.processing) {
            return;
        }
        this.processing = true;
        await this.loading.present();
        await this.messages.clear();
        try {
            await this.accountService.confirmEmail(this.activationCodeModel);
            await this.navController.navigateRoot('/');
        } catch (e) {
            console.log('error', e);
            await this.messages.showError(e);
        }
        await this.loading.dismiss();
        this.processing = false;
    }
}
