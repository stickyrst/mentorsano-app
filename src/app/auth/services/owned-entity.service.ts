import { Injector } from '@angular/core';
import { LoadingAbleService } from '@core/services/base-services/loading-able.service';

export class OwnedEntityService<T> extends LoadingAbleService {

    constructor(injector: Injector) {
        super(injector);
    }

    protected controllerName: string;
    protected getOwnActionName = 'GetOwn';
    protected saveOwnActionName = 'SaveOwn';

    public async getOwn(): Promise<T> {
        const path = this.controllerName + '/' + this.getOwnActionName;
        return this.get<T>(path, true);
    }

    public async saveOwn(model: T): Promise<T> {
        const path = this.controllerName + '/' + this.saveOwnActionName;
        return await this.post<T, T>(path, model);
    }
}
