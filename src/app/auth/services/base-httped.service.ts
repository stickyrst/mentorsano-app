import { Injector } from '@angular/core';
import { CustomHttpClient } from './custom-http.client';
import { HttpClient } from '@angular/common/http';

export class BaseHttpedService {
    protected http: HttpClient;
    protected aHttp: CustomHttpClient;

    constructor(protected injector: Injector) {
        this.http = injector.get(HttpClient);
        this.aHttp = injector.get(CustomHttpClient);
        this.aHttp.useAuthToken = true;
    }
}
