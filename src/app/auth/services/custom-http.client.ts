import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class CustomHttpClient {

    private useAuthTokenValue: boolean;

    public set useAuthToken(value: boolean) {
        this.useAuthTokenValue = value;
    }

    private http: HttpClient;
    private auth: AuthService;

    constructor(private injector: Injector) {
        this.http = injector.get(HttpClient);
        this.auth = injector.get(AuthService);
    }

    public async get<T>(url, options?: CustomHttpOptions): Promise<T> {
        if (this.useAuthTokenValue) {
            options = {
                ...options,
                headers: {
                    ...(options && options.headers),
                    Authorization: `Bearer ${await this.auth.getToken()}`,
                },
            };
        }

        const x = this.http.get<T>(url, options).toPromise();
        return await x;
    }

    public async post<T>(url, data: any, options?: CustomHttpOptions): Promise<T> {
        if (this.useAuthTokenValue) {
            options = {
                ...options,
                headers: {
                    ...(options && options.headers),
                    Authorization: `Bearer ${await this.auth.getToken()}`,
                },
            };
        }
        return this.http.post<T>(url, data, options).toPromise();
    }

    public async patch<T>(url, data: any, options?: CustomHttpOptions): Promise<T> {
        if (this.useAuthTokenValue) {
            options = {
                ...options,
                headers: {
                    ...(options && options.headers),
                    Authorization: `Bearer ${await this.auth.getToken()}`,
                },
            };
        }
        return this.http.patch<T>(url, data, options).toPromise();
    }

    public async delete<T>(url, options?: CustomHttpOptions): Promise<T> {
        if (this.useAuthTokenValue) {
            options = {
                ...options,
                headers: {
                    ...(options && options.headers),
                    Authorization: `Bearer ${await this.auth.getToken()}`,
                },
            };
        }
        return this.http.delete<T>(url, options).toPromise();
    }

}

export interface CustomHttpOptions {
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    observe?: 'body';
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
}
