import { EventEmitter, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConstantsService } from '@core/services/constants.service';
import { AuthSession, LoginRequestModel } from '../auth.models';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {

    private static readonly tokenStorageKey: string = 'token';
    private static readonly sessionStorageKey: string = 'session';

    private readonly baseUrl: string;

    private tokenValue: string;
    private sessionValue: AuthSession;

    private authStateEventEmitter: EventEmitter<boolean> = new EventEmitter();

    constructor(private http: HttpClient,
                private constants: ConstantsService,
                private storage: Storage) {
        this.baseUrl = `${this.constants.apiUrl}Auth/`;
        this.loadSession();
    }

    public get authStateChanged(): EventEmitter<boolean> {
        return this.authStateEventEmitter;
    }

    public get authState(): boolean {
        return this.localAuthState;
    }

    public get authStateAsync(): Promise<boolean> {
        return new Promise(async (resolve) => {
            if (this.localAuthState) {
                resolve(true);
            } else {
                await this.loadSession();
                resolve(this.localAuthState);
            }
        });
    }

    public async hasRole(role: string): Promise<boolean> {
        const session = await this.getSession();

        if (!session.roles) {
            return false;
        }

        return session.roles.indexOf(role) !== -1;
    }

    public get localAuthState(): boolean {
        return !!this.tokenValue;
    }

    public async getOptions(needsAuth?: boolean): Promise<{ headers?: HttpHeaders }> {
        return {headers: await this.getHeaders(needsAuth)};
    }

    public async getHeaders(needsAuth?: boolean): Promise<HttpHeaders> {
        if (!needsAuth) {
            return new HttpHeaders();
        }
        const session = await this.getSession();

        if (!session) {
            return new HttpHeaders();
        }
        return new HttpHeaders().append('Authorization', `${session.tokenType} ${session.token}`);
    }

    public async login(requestModel: LoginRequestModel): Promise<any> {
        const url = `${this.baseUrl}Login`;
        const res = await this.http.post<AuthSession>(url, requestModel).toPromise();

        // const abc = await this.http.get<AuthSession>(url, {}).toPromise();

        await this.saveSession(res);
        return true;
    }

    public async logout(): Promise<void> {
        await this.saveSession();
    }

    public async getToken(): Promise<string> {
        if (!this.tokenValue) {
            await this.loadSession();
        }
        return this.tokenValue;
    }

    public async getSession(): Promise<AuthSession> {
        if (!this.sessionValue) {
            this.sessionValue = await this.storage.get(AuthService.sessionStorageKey) as AuthSession;
        }
        return this.sessionValue;
    }

    public async saveSession(authSession?: AuthSession): Promise<void> {
        if (authSession) {
            await this.storage.set(AuthService.tokenStorageKey, authSession.token);
            await this.storage.set(AuthService.sessionStorageKey, authSession);
        } else {
            await this.storage.remove(AuthService.tokenStorageKey);
            await this.storage.remove(AuthService.sessionStorageKey);
        }
        await this.loadSession();
    }

    private async loadSession(): Promise<void> {
        const initialStatus = !!this.tokenValue;
        this.tokenValue = await this.storage.get(AuthService.tokenStorageKey) as string;
        if (this.tokenValue) {
            this.sessionValue = await this.storage.get(AuthService.sessionStorageKey) as AuthSession;
        } else {
            this.sessionValue = null;
        }
        const differentStatus = initialStatus !== !!this.tokenValue;
        if (differentStatus) {
            this.authStateEventEmitter.emit(!!this.tokenValue);
        }
    }

}
