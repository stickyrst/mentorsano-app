import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConstantsService } from '@core/services/constants.service';
import { AuthService } from './auth.service';
import { AuthSession, ConfirmationRequestModel, ForgotPasswordModel } from '../auth.models';

@Injectable()
export class AccountService {
    private readonly baseUrl: string;

    constructor(private http: HttpClient,
                private constants: ConstantsService,
                private authService: AuthService) {
        this.baseUrl = `${this.constants.apiUrl}Auth/`;
    }

    public register(data: any): Promise<any> {
        const url = `${this.baseUrl}Register`;
        return this.http.post(url, data).toPromise();
    }

    public async forgotPassword(requestModel: ForgotPasswordModel): Promise<any> {
        const url = `${this.baseUrl}ForgotPassword`;
        return await this.http.post<any>(url, requestModel).toPromise();
    }

    public async resetPassword(requestModel: any): Promise<string> {
        const url = `${this.baseUrl}ResetPassword`;
        const response = await this.http.post<any>(url, requestModel).toPromise();
        if (response && response.session) {
            await this.authService.saveSession(response.session);
            return response.message;
        }
        return response;
    }

    public async changePassword(requestModel: any): Promise<any> {
        const url = `${this.baseUrl}ChangePassword`;
        const options = await this.authService.getOptions(true);
        return this.http.post(url, requestModel, options).toPromise();
    }

    public async updateAccount(requestModel: any): Promise<any> {
        const url = `${this.baseUrl}UpdateOwn`;
        const options = await this.authService.getOptions(true);
        return this.http.post(url, requestModel, options).toPromise();
    }

    public async confirmEmail(model: ConfirmationRequestModel): Promise<string> {
        const url = this.baseUrl + 'ConfirmEmail';
        const response = await this.http.post<any>(url, model).toPromise();
        if (response && response.session) {
            await this.authService.saveSession(response.session);
            return response.message;
        }
        return response;
    }

    public async getOwnAccount(): Promise<any> {
        const url = `${this.baseUrl}GetOwn`;
        const options = await this.authService.getOptions(true);

        try {
            return await this.http.get(url, options).toPromise();
        } catch {
            return {};
        }
    }

    public async loginGoogle(googleUser: any): Promise<any> {
        const url = `${this.baseUrl}LoginWithGoogle`;
        const options = await this.authService.getOptions(true);
        const response =  await this.http.post<{session: AuthSession}>(url, googleUser, options).toPromise();
        if (response && response.session) {
            await this.authService.saveSession(response.session);
            return true;
        }
        return response;
    }

    public async loginFacebook(data: { userID: string, accessToken: string}): Promise<any> {
        const url = `${this.baseUrl}LoginWithFacebook`;
        const options = await this.authService.getOptions(true);
        const response =  await this.http.post<{session: AuthSession}>(url, data, options).toPromise();
        if (response && response.session) {
            await this.authService.saveSession(response.session);
            return true;
        }
        return response;
    }
    public async loginApple(data: any): Promise<any> {
        const url = `${this.baseUrl}LoginWithApple`;
        const options = await this.authService.getOptions(true);
        const response =  await this.http.post<{session: AuthSession}>(url, data, options).toPromise();
        if (response && response.session) {
            await this.authService.saveSession(response.session);
            return true;
        }
        return response;
    }
 }
