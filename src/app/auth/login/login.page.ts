import { Component, NgZone, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { TranslationsService } from '@core/services/translations.service';
import { Task } from '@core/models/task';
import { ProfileService } from '@feats/profile/services/profile.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { AdmobService } from '@core/services/admob.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    constructor(
        private loading: CustomLoaderService,
        private auth: AuthService,
        private messages: DisplayMessageService,
        private navController: NavController,
        public trans: TranslationsService,
        private profileService: ProfileService,
        private zone: NgZone,
        private menuController: MenuController,
        private admobService: AdmobService,
    ) {
    }

    public model = {email: '', password: ''};

    public async ngOnInit() {
        await this.menuController.enable(false);
    }

    public async submit() {
        await this.loading.present();
        await this.messages.clear();
        try {
            await this.auth.login(this.model);
            await this.afterLoginHook();
        } catch (e) {
            console.log('error', e);
            await this.messages.showError(e);
        } finally {
            await this.loading.dismiss();
        }
    }

    private async afterLoginHook(): Task {
        const profile = await this.profileService.getOwn();
        await this.loading.dismiss();
        await this.menuController.enable(true);
        this.admobService.setPremium(profile.hasPremium === true);
        if (profile.bmi === 0 || profile.tags.length === 0) {
            await this.navController.navigateRoot('/be-honest');
        } else {
            await this.navController.navigateRoot('/');
        }
    }

    public async socialMediaLoggedIn($event): Promise<void> {
        if ($event !== true) {
            return;
        }
        await this.zone.run(async () => {
            await this.messages.clear();
            await this.afterLoginHook();
        });
    }
}
