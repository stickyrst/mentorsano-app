import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { FormlyModule } from '@ngx-formly/core';
import { CoreModule } from '@core/core.module';
import { ProfileModule } from '@feats/profile/profile.module';
import { SocialMediaLoginComponent } from '@auth/components/social-media-login/social-media-login.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LoginPageRoutingModule,
        ReactiveFormsModule,
        FormlyModule,
        CoreModule,
        ProfileModule.forRoot(),
    ],
    declarations: [
        LoginPage,
        SocialMediaLoginComponent,
    ],
})
export class LoginPageModule {
}
