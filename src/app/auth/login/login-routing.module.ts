import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginPage } from './login.page';
import { FormConfigResolver } from '@core/resolvers/form-config.resolver';

const routes: Routes = [
    {
        path: '',
        component: LoginPage,
        resolve: {formConfig: FormConfigResolver},
        data: {formSchemaName: 'LoginRequestModel'},
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LoginPageRoutingModule {
}
