import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { OneSignalService } from '@core/services/one-signal.service';
import { ConstantsService } from '@core/services/constants.service';
import { AuthService } from '@auth/services/auth.service';
import { GlobalsService } from '@core/services/globals.service';
import { TranslationsService } from '@core/services/translations.service';
import { CustomLoaderService } from '@core/services/custom-loader.service';
import { LocalNotificationsHelperService } from '@core/services/local-notifications-helper.service';
import { environment } from '../environments/environment';
import { BlockHwdBackButtonService } from '@core/services/block-hwd-back-button.service';
import { PremiumService } from '@feats/premium/services/premium.service';
import { DisplayMessageService } from '@core/services/display-message.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { NavigationEnd, Router } from '@angular/router';
import { AdmobService } from '@core/services/admob.service';
import { filter } from 'rxjs/operators';
import { ForceReloadAppService } from '@core/services/force-reload-app.service';
import { AppUpdateService } from '@core/services/app-update.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss'],
})
export class AppComponent {

    public platformLoading: boolean;

    public get platformLoadingValue(): boolean {
        return this.platformLoading;
    }

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private oneSignal: OneSignalService,
        public globals: GlobalsService,
        private constants: ConstantsService,
        private auth: AuthService,
        private transService: TranslationsService,
        private storage: Storage,
        private loading: CustomLoaderService,
        private localNotifsHelper: LocalNotificationsHelperService,
        private blockHwdBackButtonService: BlockHwdBackButtonService,
        private premiumService: PremiumService,
        private messages: DisplayMessageService,
        private ga: GoogleAnalytics,
        private router: Router,
        private admobService: AdmobService,
        public forceReloadApp: ForceReloadAppService,
        private appUpdate: AppUpdateService,
    ) {
        this.initializeApp();
        this.platformLoading = true;
    }

    public initializeApp() {

        this.log('initializeApp');
        this.loading.present().then();

        this.platform.ready().then(async () => {

            this.log('app ready');
            if (!await this.checkStorageVersion()) {
                this.log('checkStorageVersion is false');
                return;
            }
            this.log('doing app logic...');

            if (this.constants.hasCordova) {

                this.statusBar.overlaysWebView(true);
                (window as any).navigationbar.setUp(true); // use only on android

                this.splashScreen.hide();
                this.oneSignal.initialize();
                this.localNotifsHelper.initialize();
                this.premiumService.initialize();
                this.localNotifsHelper.setForMeals();

                this.configureGa();
                this.admobService.init();
            } else {
                console.log('Running in browser ...');
            }
            this.transService.loadSavedOrDefaultLanguage(true).then(async () => {
                setTimeout(async () => {
                    await this.loading.dismiss();
                    this.globals.appReady = true;
                    this.platformLoading = false;
                    this.appUpdate.triggerUpdateCheck();
                }, 500);
            }).catch(async e => {
                console.log('prins o eroare.');
                await this.messages.showError(e, true, true);
                window.location.href = '/';
            });
            this.blockHwdBackButtonService.initialize(this.platform);
        });
    }

    private async checkStorageVersion(): Promise<boolean> {
        this.log('checkStorageVersion');
        const currentStorageVersion = await this.storage.get(ConstantsService.storageVersionStorageKey);
        if (!currentStorageVersion) {
            this.log('checkStorageVersion no current version');
            await this.storage.set(ConstantsService.storageVersionStorageKey, ConstantsService.storageVersion);
            return true;
        }
        if (currentStorageVersion === ConstantsService.storageVersion) {
            this.log('checkStorageVersion correct current version');
            return true;
        }
        this.log('checkStorageVersion old, resetting app');
        await this.storage.clear();
        await this.storage.set(ConstantsService.storageVersionStorageKey, ConstantsService.storageVersion);
        if (window.location.href.indexOf('/app/') !== -1) {
            window.location.href = '/app/';
        } else {
            window.location.href = '/';
        }
        return false;
    }

    private configureGa(): void {
        this.ga.debugMode().then();
        this.ga.startTrackerWithId('UA-165943815-1', 2)
            .then(() => {
                this.ga.trackEvent('app', 'started').then();
            })
            .catch(e => console.log('Error starting GoogleAnalytics', e));
        this.router.events.pipe(
            filter((e): boolean => e instanceof NavigationEnd)
        ).subscribe((e: NavigationEnd) => {
            this.ga.trackView(e.url).then();
        });
    }

    private log(...args: any[]) {
        if (environment.debug) {
            console.log(...args);
        }
    }
}
