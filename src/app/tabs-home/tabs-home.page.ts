import { Component, NgZone, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { IonTabs, NavController } from '@ionic/angular';
import { AuthService } from '@auth/services/auth.service';
import { TranslationsService } from '@core/services/translations.service';
import { NavigationOptions } from '@ionic/angular/providers/nav-controller';

@Component({
    selector: 'app-tabs-home',
    templateUrl: './tabs-home.page.html',
    styleUrls: ['./tabs-home.page.scss'],
})
export class TabsHomePage {

    @ViewChild(IonTabs) public tabs: IonTabs;
    public selectedPath: string;

    public tabsConfig: TabConfigModel[] = [
        {
            icon: 'assets/icons/home.svg',
            text: 'Home',
            path: 'home',
        },
        {
            icon: 'assets/icons/notepad.svg',
            text: 'to-do',
            path: 'check',
            requireLogin: true,
        },
        {
            icon: 'assets/icons/news.svg',
            text: 'newsfeed',
            path: 'articles',
            options: {queryParams: {type: 'article'}},
            requireLogin: true,
        },
    ];

    public tabsWillChange($event) {
        this.zone.run(() => {
            this.selectedPath = $event.tab;
        });
    }

    public navigateToTab(config: TabConfigModel) {
        if (config.requireLogin) {
            if (!this.auth.localAuthState) {
                this.navController.navigateForward(['/login']).then();
                return;
            }
        }
        this.navController.navigateForward(['/home', config.path], config.options).then();
        // this.tabs.select(config.path).then();
        // this.navController.navigateForward(['/home', config.path], config.options).then();
    }

    constructor(
        private router: Router,
        private auth: AuthService,
        private navController: NavController,
        private zone: NgZone,
        public trans: TranslationsService,
    ) {
    }
}

interface TabConfigModel {
    icon: string;
    text: string;
    path: string;
    active?: boolean;
    options?: NavigationOptions;
    requireLogin?: boolean;
}
