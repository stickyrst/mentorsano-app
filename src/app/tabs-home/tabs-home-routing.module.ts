import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsHomePage } from './tabs-home.page';

const routes: Routes = [
    {
        path: '',
        component: TabsHomePage,
        children: [
            {
                path: '',
                redirectTo: 'home',
                pathMatch: 'full',
            },
            {
                path: 'home',
                loadChildren: () => import('../home/home.module').then(m => m.HomePageModule),
            },
            {
                path: 'check',
                loadChildren: () => import('../features/check-forms/check-forms.module').then(m => m.CheckFormsModule),
            },
            {
                path: 'articles',
                loadChildren: () => import('../features/articles/articles.module').then(m => m.ArticlesModule),
            },
        ],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class TabsHomePageRoutingModule {
}
